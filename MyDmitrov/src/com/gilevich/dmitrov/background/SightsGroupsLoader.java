package com.gilevich.dmitrov.background;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.MainActivity;
import com.gilevich.dmitrov.activities.SightsListActivity;

public class SightsGroupsLoader extends AsyncTask<Boolean, Integer, Boolean> {
	private Context context;
	private ProgressDialog mProgressDialog;
	private String[] modules_names;
	private int upd;

	public SightsGroupsLoader(Context context) {
		this.context = context;
		modules_names = context.getResources().getStringArray(R.array.modules);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
		publishProgress(0);
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		mProgressDialog.dismiss();
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		if (upd == 1 && MainActivity.work_type == 0) {
			Editor ed = sp.edit();
			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
					Locale.ENGLISH);
			Date date = new Date();
			ed.putString("LastUpdate", dateFormat.format(date));
			ed.commit();
			ed = null;
			dateFormat = null;
			date = null;
		}
		sp = null;
		SightsListActivity.setAdapter(context);
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		mProgressDialog
				.setMessage("�������� ������ ������ \""
						+ modules_names[values[0]]
						+ "\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
	}

	@SuppressLint("NewApi")
	@Override
	protected Boolean doInBackground(Boolean... params) {
		boolean isUpdating = params[0];
		SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
		Cursor c = db
				.query("sights_groups", null, null, null, null, null, null);
		if (MainActivity.work_type == 0 && c.moveToFirst() && !isUpdating) {
			clearArrayLists();
			int id_index = c.getColumnIndex("id");
			int name_index = c.getColumnIndex("name");
			int color_index = c.getColumnIndex("color");
			do {
				SightsListActivity.sights_groups_ids.add(c.getInt(id_index));
				SightsListActivity.sights_groups_names.add(c
						.getString(name_index));
				SightsListActivity.sights_groups_colors.add(c
						.getString(color_index));
			} while (c.moveToNext());
			c.close();
			c = db.query("sights", null, null, null, null, null, "group_id ASC");
			if (c.moveToFirst()) {
				id_index = c.getColumnIndex("id");
				name_index = c.getColumnIndex("name");
				int group_id_index = c.getColumnIndex("group_id");
				int description_index = c.getColumnIndex("description");
				int main_photo_index = c.getColumnIndex("main_photo");
				int phones_index = c.getColumnIndex("phones");
				int site_index = c.getColumnIndex("site");
				int address_index = c.getColumnIndex("address");
				int working_hours_index = c.getColumnIndex("working_hours");
				int photos_index = c.getColumnIndex("photos");
				int sounds_index = c.getColumnIndex("sound");
				int coordinates_index = c.getColumnIndex("coordinates");
				do {
					SightsListActivity.sights_ids.add(c.getInt(id_index));
					SightsListActivity.sights_names
							.add(c.getString(name_index));
					SightsListActivity.sights_group_ids.add(c
							.getInt(group_id_index));
					SightsListActivity.sights_descriptions.add(c
							.getString(description_index));
					SightsListActivity.sights_main_photos.add(c
							.getString(main_photo_index));
					SightsListActivity.sights_phones.add(c
							.getString(phones_index));
					SightsListActivity.sights_sites
							.add(c.getString(site_index));
					SightsListActivity.sights_addresses.add(c
							.getString(address_index));
					SightsListActivity.sights_working_hours.add(c
							.getString(working_hours_index));
					SightsListActivity.sights_photos.add(c
							.getString(photos_index));
					SightsListActivity.sights_sounds.add(c
							.getString(sounds_index));
					SightsListActivity.sights_coordinates.add(c
							.getString(coordinates_index));
				} while (c.moveToNext());
				fillSightsGroupsPositions();
			}
		} else {
			c.close();
			db.close();
			Updating u = new Updating(context);
			if (MainActivity.work_type == 1) {
				upd = u.updateSights();
			} else {
				u.updatePlaces();
				u.updateEvents();
				publishProgress(1);
				u.updateTaxi();
				publishProgress(2);
				u.updateDelivery();
				publishProgress(3);
				u.updateServicesGroups();
				u.updateServices();
				publishProgress(4);
				u.updateRestoraunts();
				publishProgress(5);
				upd = u.updateSights();
				try {
					GetRestorauntsList get = new GetRestorauntsList(context, 2);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
						get.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
								false).get();
					else
						get.execute(false).get();
					get = null;
				} catch (InterruptedException | ExecutionException e) {
				}
			}
			if (upd == 0) {
				SightsListActivity.sights_groups_ids
						.addAll(u.sights_groups_ids);
				SightsListActivity.sights_groups_names
						.addAll(u.sights_groups_names);
				SightsListActivity.sights_groups_colors
						.addAll(u.sights_groups_colors);
				SightsListActivity.sights_ids.addAll(u.sights_ids);
				SightsListActivity.sights_names.addAll(u.sights_names);
				SightsListActivity.sights_group_ids.addAll(u.sights_group_id);
				SightsListActivity.sights_main_photos
						.addAll(u.sights_main_photos);
				SightsListActivity.sights_descriptions
						.addAll(u.sights_descriptions);
				SightsListActivity.sights_photos.addAll(u.sights_photos);
				SightsListActivity.sights_phones.addAll(u.sights_phones);
				SightsListActivity.sights_sites.addAll(u.sights_sites);
				SightsListActivity.sights_addresses.addAll(u.sights_addresses);
				SightsListActivity.sights_working_hours
						.addAll(u.sights_working_hours);
				fillSightsGroupsPositions();
			} else if (upd == 2) {
				u = null;
				return false;
			}
			u = null;
		}
		c.close();
		db.close();
		return true;
	}

	private void fillSightsGroupsPositions() {
		SightsListActivity.sights_groups_starts.clear();
		SightsListActivity.sights_groups_ends.clear();
		for (int i = 0; i < SightsListActivity.sights_groups_ids.size(); i++) {
			int group = SightsListActivity.sights_groups_ids.get(i);
			SightsListActivity.sights_groups_starts
					.add(SightsListActivity.sights_group_ids.indexOf(group));
			int end = SightsListActivity.sights_group_ids.lastIndexOf(group);
			SightsListActivity.sights_groups_ends.add(end != -1 ? end + 1 : -1);
		}
	}

	private void clearArrayLists() {
		SightsListActivity.sights_groups_ids.clear();
		SightsListActivity.sights_groups_names.clear();
		SightsListActivity.sights_groups_colors.clear();
		SightsListActivity.sights_ids.clear();
		SightsListActivity.sights_names.clear();
		SightsListActivity.sights_descriptions.clear();
		SightsListActivity.sights_main_photos.clear();
		SightsListActivity.sights_photos.clear();
		SightsListActivity.sights_phones.clear();
		SightsListActivity.sights_sites.clear();
		SightsListActivity.sights_addresses.clear();
		SightsListActivity.sights_coordinates.clear();
		SightsListActivity.sights_sounds.clear();
		SightsListActivity.sights_working_hours.clear();
		SightsListActivity.sights_group_ids.clear();
	}
}
