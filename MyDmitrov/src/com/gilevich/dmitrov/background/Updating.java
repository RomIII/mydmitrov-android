package com.gilevich.dmitrov.background;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;

import com.gilevich.dmitrov.activities.AfishaPlacesChoose;
import com.gilevich.dmitrov.activities.MainActivity;
import com.gilevich.dmitrov.beans.SightBean;
import com.gilevich.dmitrov.utils.InternetUtils;

public class Updating {
	private DBHelper dbHelper;
	private SQLiteDatabase db;
	public ArrayList<Integer> places_id, event_places_id, event_id;
	public ArrayList<String> event_names, event_dates_from, event_dates_to,
			event_times, event_info, event_pic, event_type, event_price,
			places_names, place_description, place_address, place_tel,
			place_site;

	public ArrayList<Integer> delivery_ids, delivery_restoraunt_ids;
	public ArrayList<String> delivery_names, delivery_short_descriptions,
			delivery_long_descriptions, delivery_addresses, delivery_phones,
			delivery_sites, delivery_logos;

	public ArrayList<Integer> taxi_ids, taxi_isMain, taxi_operators;
	public ArrayList<String> taxi_names, taxi_phone, taxi_phones, taxi_logos;

	public ArrayList<Integer> services_id, services_groups_id,
			services_group_ids;
	public ArrayList<String> services_groups_names, services_groups_text,
			services_names, services_phones, services_addresses,
			services_sites, services_texts, services_logos, services_info,
			services_latitudes, services_lontitudes;

	public ArrayList<Integer> restoraunts_ids, restoraunts_min_amount;
	public ArrayList<String> restoraunts_names, restoraunts_phones,
			restoraunts_updates, restoraunts_user_updates, restoraunts_logos,
			restoraunts_delivery_times;

	public List<Integer> sights_groups_ids, sights_ids, sights_group_id;
	// groups_ids - id �����, group_ids - id ������ � ����������� �����
	public List<String> sights_groups_names, sights_groups_colors,
			sights_names, sights_descriptions, sights_photos,
			sights_main_photos, sights_phones, sights_sites, sights_sounds,
			sights_addresses, sights_working_hours, sights_coordinates;
	public List<SightBean> sights;

	// private final String SIGHTS_PHOTOS =
	// "http://mydmitrov.ru/gid/pic/%s.jpg";
	// private final String SIGHTS_LITTLE_PHOTOS =
	// "http://mydmitrov.ru/gid/pic/%s_lit.jpg";
	// private final String SIGHTS_SOUNDS = "http://mydmitrov.ru/gid/mp3/%s";

	private ContentValues cv;
	private Context context;
	private boolean loadImages, isPrevUpdWithImages;
	private final String NOT_AVAILABLE = "na";
	private String last_places = NOT_AVAILABLE, last_events = NOT_AVAILABLE,
			last_taxis = NOT_AVAILABLE, last_deliveries = NOT_AVAILABLE,
			last_services_groups = NOT_AVAILABLE,
			last_services = NOT_AVAILABLE, last_restoraunt = NOT_AVAILABLE,
			last_sights = NOT_AVAILABLE;
	private SharedPreferences sp;
	static String[] modules_names = { "�����", "�����", "��������",
			"����������", "����", "���������������������" };
	public static final String folder_path = Environment
			.getExternalStorageDirectory().getPath() + "/.MyDmitrov/";

	private final String SITE_PREFIX = "http://mydmitrov.com/json/android/";
	private final String MODULES_UPDATE_DATES = "http://mydmitrov.com/json/android/update_table.php";
	private final String ONLINE_PREFIX = "online_";
	private final String AFISHA_PLACES = "afisha_place.php";
	private final String AFISHA_EVENTS = "afisha_event.php";
	private final String DELIVERY = "delivery.php";
	private final String SERVICES_LEVEL_1 = "directory_level1.php";
	private final String SERVICES_LEVEL_2 = "directory_level2.php";
	private final String TAXI_LIST = "taxi_name.php";

	public Updating(Context context) {
		this.context = context;
		dbHelper = new DBHelper(context);
		db = dbHelper.getWritableDatabase();
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		isPrevUpdWithImages = sp.getBoolean("isPrevUpdWithImages", false);
		loadImages = sp.getBoolean("LoadImages", true);
		sp.edit().putBoolean("isPrevUpdWithImages", loadImages).commit();
		getLastModulesDataUpdates();
		File a = new File(folder_path);
		if (!a.exists()) {
			a.mkdir();
		}
		a = null;
		try {
			FileUtils.deleteDirectory(new File(Environment
					.getExternalStorageDirectory().getPath() + "/MyDmitrov/"));
		} catch (IOException e) {
		}
	}

	private void initAfishaEventsArrayLists() {
		event_id = new ArrayList<>();
		event_names = new ArrayList<>();
		event_times = new ArrayList<>();
		event_info = new ArrayList<>();
		event_pic = new ArrayList<>();
		event_type = new ArrayList<>();
		event_dates_from = new ArrayList<>();
		event_dates_to = new ArrayList<>();
		event_places_id = new ArrayList<>();
		event_price = new ArrayList<>();
	}

	private void initAfishaPlacesArrayLists() {
		places_names = new ArrayList<>();
		places_id = new ArrayList<>();
		place_description = new ArrayList<>();
		place_address = new ArrayList<>();
		place_tel = new ArrayList<>();
		place_site = new ArrayList<>();
	}

	private void initDeliveryArrayLists() {
		delivery_ids = new ArrayList<>();
		delivery_names = new ArrayList<>();
		delivery_short_descriptions = new ArrayList<>();
		delivery_long_descriptions = new ArrayList<>();
		delivery_addresses = new ArrayList<>();
		delivery_phones = new ArrayList<>();
		delivery_sites = new ArrayList<>();
		delivery_logos = new ArrayList<>();
		delivery_restoraunt_ids = new ArrayList<>();
	}

	private void initTaxiArrayLists() {
		taxi_names = new ArrayList<>();
		taxi_phone = new ArrayList<>();
		taxi_logos = new ArrayList<>();
		taxi_phones = new ArrayList<>();
		taxi_ids = new ArrayList<>();
		taxi_isMain = new ArrayList<>();
		taxi_operators = new ArrayList<>();
	}

	private void initServicesGroupsArrayLists() {
		services_groups_names = new ArrayList<>();
		services_groups_text = new ArrayList<>();
		// id-����� ��������� �����
		services_groups_id = new ArrayList<>();
	}

	private void initServicesArrayLists() {
		services_id = new ArrayList<>();
		services_names = new ArrayList<>();
		services_phones = new ArrayList<>();
		services_addresses = new ArrayList<>();
		services_sites = new ArrayList<>();
		services_texts = new ArrayList<>();
		services_info = new ArrayList<>();
		services_logos = new ArrayList<>();
		services_latitudes = new ArrayList<>();
		services_lontitudes = new ArrayList<>();
		// id-����� ���������� �����
		services_group_ids = new ArrayList<>();
	}

	private void initRestorauntsArrayLists() {
		restoraunts_logos = new ArrayList<>();
		restoraunts_delivery_times = new ArrayList<>();
		restoraunts_min_amount = new ArrayList<>();
		restoraunts_names = new ArrayList<>();
		restoraunts_user_updates = new ArrayList<>();
		restoraunts_updates = new ArrayList<>();
		restoraunts_ids = new ArrayList<>();
		restoraunts_phones = new ArrayList<>();
	}

	private void initSightArrayLists() {
		sights = new ArrayList<>();
		sights_groups_ids = new ArrayList<>();
		sights_groups_names = new ArrayList<>();
		sights_groups_colors = new ArrayList<>();
		sights_ids = new ArrayList<>();
		sights_names = new ArrayList<>();
		sights_main_photos = new ArrayList<>();
		sights_group_id = new ArrayList<>();
		sights_descriptions = new ArrayList<>();
		sights_photos = new ArrayList<>();
		sights_sounds = new ArrayList<>();
		sights_phones = new ArrayList<>();
		sights_sites = new ArrayList<>();
		sights_addresses = new ArrayList<>();
		sights_working_hours = new ArrayList<>();
		sights_coordinates = new ArrayList<>();
	}

	void getLastModulesDataUpdates() {
		try {
			URL url = new URL(MODULES_UPDATE_DATES);
			String data = IOUtils.toString(url);
			JSONArray modules_update = new JSONArray(data);
			data = null;
			last_events = modules_update.getJSONObject(0).getString("date");
			last_places = modules_update.getJSONObject(1).getString("date");
			last_taxis = modules_update.getJSONObject(2).getString("date");
			last_deliveries = modules_update.getJSONObject(3).getString("date");
			last_services_groups = modules_update.getJSONObject(4).getString(
					"date");
			last_services = modules_update.getJSONObject(5).getString("date");
			last_restoraunt = modules_update.getJSONObject(6).getString("date");
			last_sights = modules_update.getJSONObject(8).getString("date");
			sp.edit()
					.putString("news_update",
							modules_update.getJSONObject(9).getString("date"))
					.commit();
		} catch (IOException | JSONException e) {
		}
	}

	public static String getFileExtension(String filePath) {
		return "." + FilenameUtils.getExtension(filePath);
	}

	public static JSONArray getJSONFromURL(String URL) {
		try {
			URL url = new URL(URL);
			String data = IOUtils.toString(url);
			JSONArray array = new JSONArray(data);
			data = null;
			return array;
		} catch (IOException | JSONException e) {
			return null;
		}
	}

	public int updatePlaces() {
		if (MainActivity.work_type == 0
				&& sp.getString("placesDate", "n/a").contains(last_places)) {
			if (!(!isPrevUpdWithImages && loadImages))
				return 1;
		} else if (last_places.contains(NOT_AVAILABLE)) {
			return 2;
		}
		JSONArray places = getJSONFromURL(SITE_PREFIX
				+ (MainActivity.work_type == 1 ? ONLINE_PREFIX : "")
				+ AFISHA_PLACES);
		if (places != null) {
			initAfishaPlacesArrayLists();
			db.delete("afisha_places", null, null);
			for (int i = 0; i < places.length(); i++) {
				try {
					JSONObject place = places.getJSONObject(i);
					places_id.add(place.getInt("id"));
					places_names.add(place.getString("name"));
					place_site.add(place.getString("site"));
					place_description.add(place.getString("text"));
					place_address.add(place.getString("adr"));
					place_tel.add(place.getString("tel"));
					if (MainActivity.work_type == 0) {
						cv = new ContentValues();
						cv.put("place_id", places_id.get(i));
						cv.put("place_name", places_names.get(i));
						cv.put("place_site", place_site.get(i));
						cv.put("place_description", place_description.get(i));
						cv.put("place_address", place_address.get(i));
						cv.put("place_tel", place_tel.get(i));
						db.insert("afisha_places", null, cv);
						cv = null;
					}
					place = null;
				} catch (JSONException e) {
				}
				if (MainActivity.work_type == 0)
					sp.edit().putString("placesDate", last_places).commit();
				else
					sp.edit().remove("placesDate").commit();
			}
		} else
			return 2;
		places = null;
		return 0;
	}

	public int updateEvents(int placeId) {
		if (MainActivity.work_type == 0
				&& sp.getString("eventsDate", "n/a").contains(last_events)) {
			if (!(!isPrevUpdWithImages && loadImages))
				return 1;
		} else if (last_events.contains(NOT_AVAILABLE)) {
			return 2;
		}
		String url = AFISHA_EVENTS;
		if (placeId != -1)
			url += "?place_id=" + AfishaPlacesChoose.places_id.get(placeId);
		JSONArray events = getJSONFromURL(SITE_PREFIX
				+ (MainActivity.work_type == 1 ? ONLINE_PREFIX : "") + url);
		url = null;
		if (events != null) {
			initAfishaEventsArrayLists();
			db.delete("afisha_events", null, null);
			for (int i = 0; i < events.length(); i++) {
				try {
					JSONObject event = events.getJSONObject(i);
					event_id.add(event.getInt("id"));
					event_type.add(event.getString("tip"));
					event_times.add(event.getString("start"));
					event_price.add(event.getString("money"));
					event_names.add(event.getString("littext"));
					event_places_id.add(event.getInt("mid"));
					event_dates_from.add(event.getString("date1"));
					if (event.getString("date2").length() > 0)
						event_dates_to.add(event.getString("date2"));
					else
						event_dates_to.add(event.getString("date1"));

					String logo_url = event.getString("logo"), logo_path = "";
					if (logo_url.length() > 0) {
						logo_path = folder_path + "event_" + event.getInt("id")
								+ getFileExtension(logo_url);
						if (!new File(logo_path).exists())
							try {
								FileUtils.copyURLToFile(new URL(logo_url),
										new File(logo_path));
							} catch (IOException e1) {
								logo_path = null;
							}
					} else {
						logo_path = null;
					}
					event_pic.add(logo_path);
					logo_url = null;
					logo_path = null;

					if (MainActivity.work_type == 0) {
						event_info.add(event.getString("a_text"));

						cv = new ContentValues();
						cv.put("event_type", event_type.get(i));
						cv.put("event_time", event_times.get(i));
						cv.put("event_price", event_price.get(i));
						cv.put("event_name", event_names.get(i));
						cv.put("place_id", event_places_id.get(i));
						cv.put("date_from", event_dates_from.get(i));
						cv.put("date_to", event_dates_to.get(i));
						cv.put("event_info", event_info.get(i));
						cv.put("event_pic", event_pic.get(i));
						db.insert("afisha_events", null, cv);
						cv = null;
					}

					event = null;
				} catch (JSONException e) {
				}
			}
			if (MainActivity.work_type == 0)
				sp.edit().putString("eventsDate", last_events).commit();
			else
				sp.edit().remove("eventsDate").commit();
		} else
			return 2;
		events = null;
		return 0;
	}

	public int updateEvents() {
		return updateEvents(-1);
	}

	public int updateTaxi() {
		if (MainActivity.work_type == 0
				&& sp.getString("taxiDate", "n/a").contains(last_taxis)) {
			if (!(!isPrevUpdWithImages && loadImages))
				return 1;
		} else if (last_taxis.contains(NOT_AVAILABLE)) {
			return 2;
		}
		JSONArray taxis = getJSONFromURL(SITE_PREFIX
				+ (MainActivity.work_type == 1 ? ONLINE_PREFIX : "")
				+ TAXI_LIST);
		if (taxis != null) {
			initTaxiArrayLists();
			ArrayList<String> main_numbers = new ArrayList<>();
			Cursor c = db
					.query("taxi_name", null, null, null, null, null, null);
			if (c.moveToFirst()) {
				int telIndex = c.getColumnIndex("tel");
				do {
					main_numbers.add(c.getString(telIndex));
				} while (c.moveToNext());
			}
			c.close();
			db.delete("taxi_name", null, null);
			db.delete("taxi_tel", null, null);
			for (int i = 0; i < taxis.length(); i++) {
				try {
					if (MainActivity.work_type == 0)
						cv = new ContentValues();
					int start_position = taxi_phones.size();
					JSONObject taxi = taxis.getJSONObject(i);

					String name = taxi.getString("name");
					taxi_names.add(name);

					String logo_url = taxi.getString("logo");
					String logo_path = "";

					if (logo_url.length() > 0) {
						logo_path = folder_path + "taxi_" + taxi.getInt("id")
								+ logo_url.substring(logo_url.lastIndexOf("."));
						if (!new File(logo_path).exists())
							try {
								FileUtils.copyURLToFile(new URL(logo_url),
										new File(logo_path));
							} catch (IOException e) {
								logo_path = null;
							}
					} else {
						logo_path = null;
					}
					taxi_logos.add(logo_path);
					logo_path = null;
					logo_url = null;

					JSONArray tels = taxi.getJSONArray("tels");
					boolean isMainPhoneSetted = false;
					for (int j = 0; j < tels.length(); j++) {
						JSONObject phone = tels.getJSONObject(j);
						String phone_number = phone.getString("phone");
						taxi_phones.add(phone_number);

						taxi_operators.add(Integer.parseInt(phone
								.getString("operator")));

						if (!isMainPhoneSetted) {
							if (main_numbers.indexOf(phone_number) >= 0) {
								taxi_isMain.add(1);
								taxi_phone.add(phone_number);
								if (MainActivity.work_type == 0)
									cv.put("tel", phone_number);
								isMainPhoneSetted = true;
							} else
								taxi_isMain.add(0);
						} else
							taxi_isMain.add(0);

						taxi_ids.add(taxi_names.size() - 1);

						if (MainActivity.work_type == 0) {
							ContentValues phones = new ContentValues();
							phones.put("taxi_id", taxi_names.size() - 1);
							phones.put("nomer_tel", phone_number);
							phones.put("operator", taxi_operators
									.get(taxi_operators.size() - 1));
							phones.put("main",
									taxi_isMain.get(taxi_isMain.size() - 1));
							db.insert("taxi_tel", null, phones);
							phones = null;
						}
						phone_number = null;
						phone = null;
					}

					if (!isMainPhoneSetted) {
						String tel = taxi_phones.get(start_position);
						taxi_isMain.set(start_position, 1);
						taxi_phone.add(tel);
						if (MainActivity.work_type == 0) {
							cv.put("tel", tel);

							ContentValues phone = new ContentValues();
							phone.put("main", 1);
							db.update("taxi_tel", phone, "nomer_tel=" + tel,
									null);
							phone = null;
						}
						tel = null;
					}

					if (MainActivity.work_type == 0) {
						cv.put("name", name);
						cv.put("logo", taxi_logos.get(i));
						db.insert("taxi_name", null, cv);
						cv = null;
					}
					name = null;
					logo_path = null;
					tels = null;
					taxi = null;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			main_numbers = null;
			if (MainActivity.work_type == 0)
				sp.edit().putString("taxiDate", last_taxis).commit();
			else
				sp.edit().remove("taxiDate").commit();
		} else
			return 2;
		taxis = null;
		return 0;
	}

	public int updateDelivery() {
		if (MainActivity.work_type == 0
				&& sp.getString("deliveryDate", "n/a")
						.contains(last_deliveries)) {
			if (!(!isPrevUpdWithImages && loadImages))
				return 1;
		} else if (last_deliveries.contains(NOT_AVAILABLE)) {
			return 2;
		}
		JSONArray services = getJSONFromURL(SITE_PREFIX
				+ (MainActivity.work_type == 1 ? ONLINE_PREFIX : "") + DELIVERY);
		if (services != null) {
			initDeliveryArrayLists();
			db.delete("delivery_firms", null, null);
			for (int i = 0; i < services.length(); i++) {
				try {
					JSONObject service = services.getJSONObject(i);
					delivery_ids.add(service.getInt("id"));
					delivery_names.add(service.getString("name"));
					String logo_url = service.getString("logo");
					String logo_path = null;
					if (logo_url.length() > 0) {
						logo_path = folder_path + "delivery_"
								+ service.getInt("id")
								+ logo_url.substring(logo_url.lastIndexOf("."));
						if (!new File(logo_path).exists())
							try {
								FileUtils.copyURLToFile(new URL(logo_url),
										new File(logo_path));
							} catch (IOException e) {
								logo_path = null;
							}
					} else {
						logo_path = null;
					}
					delivery_logos.add(logo_path);
					logo_path = null;
					logo_url = null;

					delivery_phones.add(service.getString("tel"));

					delivery_addresses.add(service.getString("adr"));

					delivery_short_descriptions.add(service
							.getString("text200"));

					delivery_long_descriptions.add(service
							.getString("text1500"));

					delivery_sites.add(service.getString("site"));

					delivery_restoraunt_ids.add(service.getInt("menu"));

					if (MainActivity.work_type == 0) {
						cv = new ContentValues();
						cv.put("id", delivery_ids.get(i));
						cv.put("delivery_name", delivery_names.get(i));
						cv.put("delivery_logo", delivery_logos.get(i));
						cv.put("delivery_phone", delivery_phones.get(i));
						cv.put("delivery_address", delivery_addresses.get(i));
						cv.put("delivery_short_description",
								delivery_short_descriptions.get(i));
						cv.put("delivery_long_description",
								delivery_long_descriptions.get(i));
						cv.put("delivery_site", delivery_sites.get(i));
						cv.put("restoraunt_id", delivery_restoraunt_ids.get(i));
						db.insert("delivery_firms", null, cv);
						cv = null;
					}
					service = null;
				} catch (JSONException e) {
				}
			}
			if (MainActivity.work_type == 0)
				sp.edit().putString("deliveryDate", last_deliveries).commit();
			else
				sp.edit().remove("deliveryDate").commit();
		} else
			return 2;
		services = null;
		return 0;
	}

	public int updateServicesGroups() {
		if (MainActivity.work_type == 0
				&& sp.getString("services_groupsDate", "n/a").contains(
						last_services_groups)) {
			if (!(!isPrevUpdWithImages && loadImages))
				return 1;
		} else if (last_services_groups.contains(NOT_AVAILABLE)) {
			return 2;
		}
		JSONArray groups = getJSONFromURL(SITE_PREFIX + SERVICES_LEVEL_1);
		if (groups != null) {
			initServicesGroupsArrayLists();
			db.delete("services_groups", null, null);
			for (int i = 0; i < groups.length(); i++) {
				try {
					JSONObject group = groups.getJSONObject(i);

					services_groups_id.add(group.getInt("id"));

					String name = group.getString("name");
					services_groups_names.add(name);
					String description = group.getString("text");

					services_groups_text.add(description);

					if (MainActivity.work_type == 0) {
						cv = new ContentValues();
						cv.put("group_id", services_groups_id.get(i));
						cv.put("group_name", name);
						cv.put("group_description", description);
						db.insert("services_groups", null, cv);
						cv = null;
					}
					group = null;
					name = null;
					description = null;
				} catch (JSONException e) {
				}
			}
			if (MainActivity.work_type == 0)
				sp.edit()
						.putString("services_groupsDate", last_services_groups)
						.commit();
			else
				sp.edit().remove("services_groupsDate").commit();
		} else
			return 2;
		groups = null;
		return 0;
	}

	public int updateServices(int levelId) {
		if (MainActivity.work_type == 0
				&& sp.getString("servicesDate", "n/a").contains(last_services)) {
			if (!(!isPrevUpdWithImages && loadImages))
				return 1;
		} else if (last_services.contains(NOT_AVAILABLE)) {
			return 2;
		}
		String url = SERVICES_LEVEL_2;
		if (levelId != -1)
			url += "?level2_id=" + levelId;
		JSONArray services = getJSONFromURL(SITE_PREFIX
				+ (MainActivity.work_type == 1 ? ONLINE_PREFIX : "") + url);
		url = null;
		if (services != null) {
			initServicesArrayLists();
			db.delete("services", null, null);
			for (int i = 0; i < services.length(); i++) {
				try {
					JSONObject service = services.getJSONObject(i);
					if (MainActivity.work_type == 1)
						services_id.add(service.getInt("id"));
					services_group_ids.add(levelId == -1 ? service
							.getInt("sub_id") : levelId);

					services_names.add(service.getString("name"));

					String logo_url = service.getString("logo");
					String logo_path = null;
					if (logo_url.length() > 0) {
						logo_path = folder_path + "services"
								+ service.getInt("id")
								+ logo_url.substring(logo_url.lastIndexOf("."));
						if (!new File(logo_path).exists())
							FileUtils.copyURLToFile(new URL(logo_url),
									new File(logo_path));
					} else {
						logo_path = null;
					}
					services_logos.add(logo_path);
					logo_path = null;
					logo_url = null;
					services_info.add(service.getString("info"));
					services_phones.add(service.getString("tels"));

					if (MainActivity.work_type == 0) {

						services_addresses.add(service.getString("adr"));

						services_sites.add(service.getString("site"));

						services_texts.add(service.getString("text"));

						String lon = service.getString("lon");
						if (lon.equals("0"))
							lon = null;
						services_lontitudes.add(lon);
						lon = null;

						String lat = service.getString("lat");
						if (lat.equals("0"))
							lat = null;
						services_latitudes.add(lat);
						lat = null;

						cv = new ContentValues();
						cv.put("group_id", services_group_ids.get(i));
						cv.put("service_name", services_names.get(i));
						cv.put("service_logo", services_logos.get(i));
						cv.put("service_info", services_info.get(i));
						cv.put("service_phone", services_phones.get(i));
						cv.put("service_address", services_addresses.get(i));
						cv.put("service_site", services_sites.get(i));
						cv.put("service_text", services_texts.get(i));
						cv.put("service_lontitude", services_lontitudes.get(i));
						cv.put("service_latitude", services_latitudes.get(i));
						db.insert("services", null, cv);
					}
					service = null;
				} catch (JSONException | IOException e) {
					e.printStackTrace();
				}
			}
			if (MainActivity.work_type == 0)
				sp.edit().putString("servicesDate", last_services).commit();
			else
				sp.edit().remove("servicesDate").commit();
		} else
			return 2;
		return 0;
	}

	public int updateServices() {
		return updateServices(-1);
	}

	void setLastUpdateDate() {
		if (MainActivity.work_type == 0)
			sp.edit()
					.putString(
							"LastUpdate",
							new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
									.format(new Date())).commit();
	}

	public int updateRestoraunts() {
		if (MainActivity.work_type == 0
				&& sp.getString("restorauntsDate", "n/a").contains(
						last_restoraunt)) {
			if (!(!isPrevUpdWithImages && loadImages))
				return 1;
		} else if (last_restoraunt.contains(NOT_AVAILABLE)) {
			return 2;
		}
		try {
			Cursor c = db.query("restoraunts", new String[] { "id",
					"user_update" }, null, null, null, null, null);
			SparseArray<String> users_update = new SparseArray<String>();
			if (c.moveToFirst()) {
				int idIndex = c.getColumnIndex("id"), updateIndex = c
						.getColumnIndex("user_update");
				do {
					users_update.append(c.getInt(idIndex),
							c.getString(updateIndex));
				} while (c.moveToNext());
			}
			URL url = new URL("http://mydmitrov.com/json/android/menu_restoraunt.php");
			String data = IOUtils.toString(url);
			initRestorauntsArrayLists();
			JSONArray restoraunts = new JSONArray(data);
			for (int i = 0; i < restoraunts.length(); i++) {
				JSONObject restoraunt = restoraunts.getJSONObject(i);
				restoraunts_ids
						.add(Integer.valueOf(restoraunt.getString("id")));
				restoraunts_names.add(restoraunt.getString("name"));
				String path = restoraunt.getString("pic"), local_path = null;
				if (path.length() > 0) {
					InternetUtils.download(path,
							"r" + restoraunt.getString("id") + ".png");
					local_path = folder_path + "r" + restoraunt.getString("id")
							+ ".png";
				}
				restoraunts_phones.add("");
				if (restoraunt.has("tel")) {
					Log.d("i1", "tel = " + restoraunt.getString("tel"));
					restoraunts_phones.set(restoraunts_phones.size() - 1,
							restoraunt.getString("tel"));
				}
				restoraunts_logos.add(local_path);
				path = null;
				local_path = null;
				restoraunts_min_amount.add(Integer.valueOf(restoraunt
						.getString("min")));
				restoraunts_delivery_times.add(restoraunt.getString("time"));
				restoraunts_updates.add(restoraunt.getString("date"));
				restoraunts_user_updates.add(users_update.get(
						restoraunts_ids.get(i), "2014-10-01 00:00:00"));
				restoraunt = null;
			}
			users_update = null;
			restoraunts = null;
			data = null;
			db.delete("restoraunts", null, null);
			if (MainActivity.work_type == 0)
				for (int i = 0; i < restoraunts_ids.size(); i++) {
					ContentValues cv = new ContentValues();
					cv.put("id", restoraunts_ids.get(i));
					cv.put("name", restoraunts_names.get(i));
					cv.put("phone", restoraunts_phones.get(i));
					cv.put("updated", restoraunts_updates.get(i));
					cv.put("user_update", restoraunts_user_updates.get(i));
					cv.put("logo", restoraunts_logos.get(i));
					cv.put("minimal_amount", restoraunts_min_amount.get(i));
					cv.put("delivery_time", restoraunts_delivery_times.get(i));
					db.insert("restoraunts", null, cv);
					cv = null;
				}
			if (MainActivity.work_type == 0) {
				sp.edit().putString("restorauntsDate", last_restoraunt)
						.commit();
				setLastUpdateDate();
			} else
				sp.edit().remove("restorauntsDate").commit();
		} catch (IOException | JSONException e) {
		}
		if (dbHelper != null) {
			dbHelper.close();
		}
		if (db != null) {
			db.close();
		}
		return 0;
	}

	public ArrayList<ArrayList<String>> getRestorauntCatalog(int restorauntID) {
		ArrayList<ArrayList<String>> result = null;
		try {
			URL url = new URL(
					"http://mydmitrov.com/json/android/menu_catalog.php?restoraunt_id="
							+ restorauntID);
			String data = IOUtils.toString(url);
			result = new ArrayList<ArrayList<String>>();
			ArrayList<String> ids = new ArrayList<>(), names = new ArrayList<>();
			JSONArray sections = new JSONArray(data);
			for (int i = 1; i < sections.length(); i++) {
				JSONObject section = sections.getJSONObject(i);
				ids.add(section.getInt("id") + "");
				names.add(section.getString("name"));
				section = null;
			}
			result.add(ids);
			result.add(names);
			names = null;
			ids = null;
			sections = null;
			data = null;
		} catch (IOException | JSONException e) {
		}
		return result;
	}

	public ArrayList<ArrayList<String>> getRestorauntDishes(int restorauntID,
			int catalogId) {
		ArrayList<ArrayList<String>> result = null;
		try {
			URL url = new URL(
					"http://mydmitrov.com/json/android/menu_dish.php?restoraunt_id="
							+ restorauntID + "&catalog_id=" + catalogId);
			String data = IOUtils.toString(url);
			result = new ArrayList<ArrayList<String>>();
			ArrayList<String> ids = new ArrayList<>();
			ArrayList<String> names = new ArrayList<>();
			ArrayList<String> prices = new ArrayList<>();
			ArrayList<String> descriptions = new ArrayList<>();
			ArrayList<String> images = new ArrayList<>();
			ArrayList<String> portions = new ArrayList<>();
			JSONArray dishes = new JSONArray(data);
			for (int i = 0; i < dishes.length(); i++) {
				JSONObject dish = dishes.getJSONObject(i);
				ids.add(dish.getInt("id") + "");
				names.add(dish.getString("name"));
				prices.add(dish.getInt("price") + "");
				descriptions.add(dish.getString("text"));
				portions.add(dish.getString("portion"));
				String image = dish.getString("pic"), file_path = null;
				if (image.length() > 0) {
					String file_url = dish.getString("pic");
					file_path = "dish_" + dish.getInt("id") + "."
							+ file_url.substring(file_url.lastIndexOf(".") + 1);
					file_url = null;
					if (InternetUtils
							.download(dish.getString("pic"), file_path))
						file_path = folder_path + file_path;
					else
						file_path = null;
				}
				images.add(file_path);
				image = null;
				dish = null;
			}
			result.add(ids);
			result.add(names);
			result.add(prices);
			result.add(descriptions);
			result.add(images);
			result.add(portions);
			portions = null;
			images = null;
			descriptions = null;
			prices = null;
			names = null;
			ids = null;
			dishes = null;
			data = null;
		} catch (IOException | JSONException e) {
		}
		return result;
	}

	public int updateRestoraunt(int restorauntID) {
		try {
			dbHelper = new DBHelper(context);
			db = dbHelper.getWritableDatabase();
			URL url = new URL(
					"http://mydmitrov.com/json/android/menu_catalog.php?restoraunt_id="
							+ restorauntID);
			String data = IOUtils.toString(url);
			JSONArray sections = new JSONArray(data);
			String last_update = sections.getJSONObject(0).getString("date");
			db.delete("sections", "restoraunt_id=" + restorauntID, null);
			if (MainActivity.work_type == 0)
				for (int i = 1; i < sections.length(); i++) {
					JSONObject section = sections.getJSONObject(i);
					cv = new ContentValues();
					cv.put("id", Integer.parseInt(section.getString("id")));
					cv.put("name", section.getString("name"));
					cv.put("restoraunt_id", restorauntID);
					db.insert("sections", null, cv);
					cv = null;
					section = null;
				}
			sections = null;
			data = null;
			url = new URL(
					"http://mydmitrov.com/json/android/menu_dish.php?restoraunt_id="
							+ restorauntID);
			data = IOUtils.toString(url);
			JSONArray dishes = new JSONArray(data);
			data = null;
			db.delete("dishes", "restoraunt_id=" + restorauntID, null);
			for (int i = 0; i < dishes.length(); i++) {
				JSONObject dish = dishes.getJSONObject(i);
				cv = new ContentValues();
				cv.put("id", dish.getInt("id"));
				cv.put("name", dish.getString("name"));
				cv.put("description", dish.getString("text"));
				cv.put("price", Integer.parseInt(dish.getString("price")));
				if (dish.getString("pic").length() > 0) {
					String file_url = dish.getString("pic"), file_path = "dish_"
							+ dish.getInt("id")
							+ "."
							+ file_url.substring(file_url.lastIndexOf(".") + 1);
					file_url = null;
					if (InternetUtils
							.download(dish.getString("pic"), file_path))
						cv.put("picture", folder_path + file_path);
				}
				cv.put("portion", dish.getString("portion"));
				cv.put("catalog_id",
						Integer.parseInt(dish.getString("catalog_id")));
				cv.put("restoraunt_id", restorauntID);
				if (MainActivity.work_type == 0)
					db.insert("dishes", null, cv);
				cv = null;
				dish = null;
			}
			if (MainActivity.work_type == 0) {
				cv = new ContentValues();
				cv.put("user_update", last_update);
				cv.put("updated", last_update);
				db.update("restoraunts", cv, "id=" + restorauntID, null);
				cv = null;
			}
			last_update = null;
			if (dbHelper != null) {
				dbHelper.close();
			}
			if (db != null) {
				db.close();
			}
		} catch (IOException | JSONException e) {
			return 1;
		}
		return 0;
	}

	public int updateSights() {
		if (MainActivity.work_type == 0
				&& sp.getString("sightsDate", "n/a").contains(last_sights)) {
			if (!(!isPrevUpdWithImages && loadImages))
				return 1;
		} else if (last_sights.contains(NOT_AVAILABLE)) {
			return 2;
		}
		initSightArrayLists();
		// try {
		// String data = IOUtils.toString(new URL(
		// "http://mydmitrov.ru/gid/json/android/gid_type.php"));
		// JSONArray groups = new JSONArray(data);
		// data = null;
		// for (int i = 0; i < groups.length(); i++) {
		// JSONObject group = groups.getJSONObject(i);
		// sights_groups_ids.add(group.getInt("id"));
		// sights_groups_names.add(group.getString("name"));
		// sights_groups_colors.add(group.getString("color"));
		// group = null;
		// }
		// groups = null;
		// data = IOUtils
		// .toString(new URL(
		// MainActivity.work_type == 0 ?
		// "http://mydmitrov.ru/gid/json/android/gid_place.php"
		// : "http://mydmitrov.ru/gid/json/android/online_gid_one.php"));
		// JSONArray places = new JSONArray(data);
		// for (int i = 0; i < places.length(); i++) {
		// JSONObject place = places.getJSONObject(i);
		// sights_ids.add(place.getInt("id"));
		// sights_names.add(place.getString("name"));
		// sights_group_id.add(place.getInt("type_id"));
		//
		// String main_photo = place.getString("f_main");
		// if (main_photo.length() > 0) {
		// if (InternetUtils.download(
		// String.format(SIGHTS_LITTLE_PHOTOS, main_photo),
		// "sight_" + main_photo + "_lit.jpg")) {
		// sights_main_photos.add(folder_path + "sight_"
		// + main_photo + "_lit.jpg");
		// }
		// } else
		// sights_main_photos.add(null);
		// main_photo = null;
		//
		// if (MainActivity.work_type == 0) {
		// sights_descriptions.add(place.getString("text"));
		// sights_phones.add(place.getString("tel_arr"));
		// sights_sites.add(place.getString("site_arr"));
		// sights_addresses.add(place.getString("address"));
		// sights_working_hours.add(place.getString("open"));
		// sights_coordinates.add(place.getString("coordinates"));
		//
		// String photos = place.getString("f_arr");
		// if (photos.length() > 0) {
		// String[] photos_array = photos.split(",");
		// photos = "";
		// for (int j = 0; j < photos_array.length; j++) {
		// if (InternetUtils.download(String.format(
		// SIGHTS_PHOTOS, photos_array[j]), "sight_"
		// + photos_array[j] + ".jpg")) {
		// if (photos.length() > 0)
		// photos += ",";
		// photos += (folder_path + "sight_"
		// + photos_array[j] + ".jpg");
		// }
		// }
		// if (photos.length() > 0) {
		// sights_photos.add(photos);
		// } else {
		// sights_photos.add(null);
		// }
		// } else {
		// sights_photos.add(null);
		// }
		// photos = null;
		//
		// String sound = place.getString("mp3_file");
		// if (sound.length() > 0) {
		// if (MainActivity.work_type == 0) {
		// if (InternetUtils.download(
		// String.format(SIGHTS_SOUNDS, sound), sound)) {
		// sights_sounds.add(folder_path + sound);
		// }
		// } else
		// sights_sounds.add(sound);
		// } else {
		// sights_sounds.add(null);
		// }
		// sound = null;
		//
		// place = null;
		// } else {
		// sights_descriptions.add(null);
		// sights_phones.add(null);
		// sights_sites.add(null);
		// sights_addresses.add(null);
		// sights_working_hours.add(null);
		// sights_coordinates.add(null);
		// sights_photos.add(null);
		// sights_sounds.add(null);
		// }
		// }
		// if (MainActivity.work_type == 0) {
		// SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
		// db.delete("sights_groups", null, null);
		// db.delete("sights", null, null);
		// for (int i = 0; i < sights_groups_ids.size(); i++) {
		// cv = new ContentValues();
		// cv.put("id", sights_groups_ids.get(i));
		// cv.put("name", sights_groups_names.get(i));
		// cv.put("color", sights_groups_colors.get(i));
		// db.insert("sights_groups", null, cv);
		// }
		// for (int i = 0; i < sights_ids.size(); i++) {
		// cv = new ContentValues();
		// cv.put("id", sights_ids.get(i));
		// cv.put("name", sights_names.get(i));
		// cv.put("group_id", sights_group_id.get(i));
		// cv.put("description", sights_descriptions.get(i));
		// cv.put("photos", sights_photos.get(i));
		// cv.put("main_photo", sights_main_photos.get(i));
		// cv.put("sound", sights_sounds.get(i));
		// cv.put("phones", sights_phones.get(i));
		// cv.put("site", sights_sites.get(i));
		// cv.put("address", sights_addresses.get(i));
		// cv.put("working_hours", sights_working_hours.get(i));
		// cv.put("coordinates", sights_coordinates.get(i));
		// db.insert("sights", null, cv);
		// }
		// db.close();
		// db = null;
		// sp.edit().putString("sightsDate", last_sights).commit();
		// } else {
		// sp.edit().remove("sightsDate").commit();
		// }
		// places = null;
		// data = null;
		// } catch (IOException | JSONException e) {
		// return 1;
		// }
		return 0;
	}

	/**
	 * A method for getting the full sight info.
	 * 
	 * @param sightId
	 *            ID of the sight
	 * @return a {@link String} array, that contains sight data, or null if
	 *         something wrong. Returned array has 6 elements: description,
	 *         photos array, phones array, sites arrays, address, and working
	 *         hours
	 */
	public String[] getSight(int sightId) {
		String[] sightData = null;
		try {
			String data = IOUtils.toString(new URL(
					"http://mydmitrov.ru/gid/json/android/online_gid_two.php?place_id="
							+ sightId));
			JSONObject sight = new JSONArray(data).getJSONObject(0);
			sightData = new String[6];
			sightData[0] = sight.getString("text");
			sightData[1] = sight.getString("f_arr");
			sightData[2] = sight.getString("tel_arr");
			sightData[3] = sight.getString("site_arr");
			sightData[4] = sight.getString("address");
			sightData[5] = sight.getString("open");
			sight = null;
			data = null;
		} catch (IOException | JSONException e) {
			return null;
		}
		return sightData;
	}
}