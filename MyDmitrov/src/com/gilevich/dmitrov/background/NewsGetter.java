package com.gilevich.dmitrov.background;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import com.gilevich.dmitrov.activities.NewsActivity;
import com.gilevich.dmitrov.beans.NewsBean;
import com.gilevich.dmitrov.utils.InternetUtils;

public class NewsGetter extends AsyncTask<Void, Void, List<NewsBean>> {
	private ProgressDialog mProgressDialog;
	private Context context;

	public NewsGetter(Context context) {
		this.context = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog
				.setMessage("�������� ��������. ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}

	@Override
	protected List<NewsBean> doInBackground(Void... params) {
		List<NewsBean> result = new ArrayList<NewsBean>();
		try {
			String data = IOUtils
					.toString(new URL(
							"http://www.mydmitrov.com/json/android/online_video_smt.php"));
			JSONArray newsArray = new JSONArray(data);
			for (int i = 0; i < newsArray.length(); i++) {
				JSONObject news = newsArray.getJSONObject(i);
				String image = news.getString("img");
				int id = news.getInt("id");
				if (InternetUtils.download(image, "news_" + id)) {
					image = Updating.folder_path + "news_" + id;
				} else {
					image = null;
				}
				NewsBean newsBean = new NewsBean(id, news.getString("title"),
						news.getString("text"), news.getString("url"),
						news.getString("date"), image);
				result.add(newsBean);
				image = null;
				newsBean = null;
				news = null;
			}
		} catch (IOException | JSONException e) {
			result.clear();
			result = null;
		}
		return result;
	}

	@Override
	protected void onPostExecute(List<NewsBean> result) {
		super.onPostExecute(result);
		mProgressDialog.dismiss();
		if (result != null) {
			NewsActivity.list = result;
			NewsActivity.setAdapter(context);
		} else {
			new AlertDialog.Builder(context)
					.setMessage("����������� ���������� � ����������")
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									((Activity) context).finish();
								}
							}).show();
		}
	}
}