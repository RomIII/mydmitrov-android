package com.gilevich.dmitrov.background;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.AfishaPlacesChoose;
import com.gilevich.dmitrov.activities.DeliveryMain;
import com.gilevich.dmitrov.activities.RestorauntsList;
import com.gilevich.dmitrov.activities.ServicesGroups;
import com.gilevich.dmitrov.activities.TaxiMain;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class Widget extends AppWidgetProvider {
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
				R.layout.widget);
		remoteViews.setOnClickPendingIntent(R.id.imageView1, PendingIntent
				.getActivity(context, 0, new Intent(context,
						AfishaPlacesChoose.class), 0));
		remoteViews.setOnClickPendingIntent(R.id.imageView2,
				PendingIntent.getActivity(context, 0, new Intent(context,
						TaxiMain.class), 0));
		remoteViews.setOnClickPendingIntent(R.id.imageView3, PendingIntent
				.getActivity(context, 0,
						new Intent(context, DeliveryMain.class), 0));
		remoteViews.setOnClickPendingIntent(R.id.imageView4, PendingIntent
				.getActivity(context, 0, new Intent(context,
						ServicesGroups.class), 0));
		remoteViews.setOnClickPendingIntent(R.id.imageView5, PendingIntent
				.getActivity(context, 0, new Intent(context,
						RestorauntsList.class), 0));
		appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
	}

}
