package com.gilevich.dmitrov.background;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import com.gilevich.dmitrov.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

public class NotificationsService extends Service {
	NotificationManager nm;

	@Override
	public IBinder onBind(Intent arg0) {
		
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				try {
					URL query = new URL(
							"http://mydmitrov.com/json/notifications.json");
					BufferedReader in = new BufferedReader(
							new InputStreamReader(query.openStream()));
					String inputLine, whole_text = "";
					while ((inputLine = in.readLine()) != null) {
						whole_text += inputLine;
						inputLine = null;
					}
					in.close();
					JSONObject jobj = new JSONObject(whole_text);
					int message_id = Integer.parseInt(jobj.get("message_id")
							.toString());
					if (message_id > PreferenceManager
							.getDefaultSharedPreferences(
									NotificationsService.this).getInt(
									"message_id", 0)) {
						String url = "";
						Intent intent = null;
						if (jobj.has("url")) {
							url = jobj.getString("url");
							intent = new Intent(Intent.ACTION_VIEW);
							intent.setData(Uri.parse(url));
						}
						NotificationCompat.Builder nb = new NotificationCompat.Builder(
								getApplicationContext())
								.setSmallIcon(R.drawable.ic_launcher)
								.setAutoCancel(true)
								.setTicker(jobj.getString("message"))
								.setContentText(jobj.getString("message"))
								.setContentIntent(
										PendingIntent
												.getActivity(
														getApplicationContext(),
														0,
														intent,
														PendingIntent.FLAG_CANCEL_CURRENT))
								.setWhen(System.currentTimeMillis())
								.setContentTitle("MyDmitrov")
								.setDefaults(Notification.DEFAULT_SOUND);
						((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
								.notify(001, nb.build());
						PreferenceManager
								.getDefaultSharedPreferences(
										NotificationsService.this).edit()
								.putInt("message_id", message_id).commit();
						url = null;
					}
					jobj = null;
				} catch (IOException e) {
				} catch (JSONException e) {
				}
			}
		}, 0, 14400000);
		return super.onStartCommand(intent, flags, startId);
	}
}
