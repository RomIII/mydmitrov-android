package com.gilevich.dmitrov.background;

import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

public class NewsAvailability extends AsyncTask<Void, Void, Boolean> {

	@Override
	protected Boolean doInBackground(Void... params) {
		try {
			String dates = IOUtils.toString(new URL(
					"http://www.mydmitrov.com/json/android/update_table.php"));
			JSONArray datesArray = new JSONArray(dates);
			JSONObject newsObject = datesArray.getJSONObject(9);
			return !newsObject.getString("date").equals("0000-00-00 00:00:00");
		} catch (IOException | JSONException e) {
			return false;
		}
	}

}