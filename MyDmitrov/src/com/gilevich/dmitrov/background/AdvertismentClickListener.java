package com.gilevich.dmitrov.background;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;

public class AdvertismentClickListener implements OnClickListener {
	Activity context;

	public AdvertismentClickListener(Context context) {
		this.context = (Activity) context;
	}

	@Override
	public void onClick(View v) {
		if (Advertisment.links.get(Advertisment.current_banner).length() != 0) {
			Analytics.sendSite(
					Advertisment.links.get(Advertisment.current_banner), true);
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
					.parse(Advertisment.links.get(Advertisment.current_banner))));
		} else if (Advertisment.phones.get(Advertisment.current_banner)
				.length() != 0) {
			Analytics.sendCall(
					Advertisment.phones.get(Advertisment.current_banner), true);
			Intent callIntent = new Intent(Intent.ACTION_DIAL);
			callIntent.setData(Uri.parse("tel:"
					+ Advertisment.phones.get(Advertisment.current_banner)));
			context.startActivity(callIntent);
		}
	}

}
