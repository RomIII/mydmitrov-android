package com.gilevich.dmitrov.background;

import java.util.ArrayList;

import com.gilevich.dmitrov.activities.RestorauntsList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseArray;

public class Cart {
	public static int currentRestoraunt;
	public static SparseArray<Integer> dishes;
	public static ArrayList<Integer> dishes_prices;
	public static SharedPreferences sp;
	public static String restorauntName = "", restorauntPhone = "";
	public static boolean cartCreated = false;
	public static Context context;

	public Cart(Context context) {
		Cart.context = context;
		dishes = new SparseArray<>();
		dishes_prices = new ArrayList<>();
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		currentRestoraunt = sp.getInt("RestorauntID", -1);
		if (currentRestoraunt != -1) {
			Log.d("i1",
					"position = "
							+ RestorauntsList.id.indexOf(currentRestoraunt));
			restorauntName = RestorauntsList.names.get(RestorauntsList.id
					.indexOf(currentRestoraunt));
			restorauntPhone = RestorauntsList.phones.get(RestorauntsList.id
					.indexOf(currentRestoraunt));
			Log.d("i1", "phone = " + restorauntPhone);
			String data = sp.getString("Dishes", "empty");
			if (!data.equals("empty")) {
				String[] dishes = data.split(";");
				for (int i = 0; i < dishes.length; i++) {
					String[] dish = dishes[i].split(",");
					Cart.dishes.append(Integer.parseInt(dish[0]),
							Integer.parseInt(dish[1]));
					dishes_prices.add(0);
					dish = null;
				}
				dishes = null;
			}
			data = null;
		}
		SQLiteDatabase db = new DBHelper(context).getReadableDatabase();
		Cursor c = db.query("dishes", new String[] { "id", "price" },
				"restoraunt_id=" + currentRestoraunt, null, null, null, null);
		if (c.moveToFirst()) {
			int idIndex = c.getColumnIndex("id"), priceIndex = c
					.getColumnIndex("price");
			do {
				int id = c.getInt(idIndex), position = dishes.indexOfKey(id);
				if (position >= 0) {
					dishes_prices.set(position, c.getInt(priceIndex));
				}
			} while (c.moveToNext());
		}
		c.close();
		db.close();
		cartCreated = true;
	}

	public static int modifyDish(int dishID, int action, int restorauntID) {
		if (Cart.currentRestoraunt == -1) {
			Cart.currentRestoraunt = restorauntID;
			restorauntName = RestorauntsList.names.get(RestorauntsList.id
					.indexOf(currentRestoraunt));
			restorauntPhone = RestorauntsList.phones.get(RestorauntsList.id
					.indexOf(currentRestoraunt));
		}
		if (Cart.currentRestoraunt == restorauntID) {
			SQLiteDatabase db = new DBHelper(context).getReadableDatabase();
			int count = Cart.dishes.get(dishID, 0);
			count += action;
			if (count == 0) {
				Cart.dishes_prices.remove(Cart.dishes.indexOfKey(dishID));
				Cart.dishes.remove(dishID);
				db.delete("dishes", "id=" + dishID, null);
			} else {
				Cart.dishes.put(dishID, count);
				if (action == 1 && count == 1) {
					Cursor c = db.query("dishes", new String[] { "price" },
							"id=" + dishID, null, null, null, null);
					c.moveToFirst();
					Log.d("i1", "index = " + Cart.dishes.indexOfKey(dishID));
					Cart.dishes_prices.add(Cart.dishes.indexOfKey(dishID),
							c.getInt(c.getColumnIndex("price")));
					c.close();
				}
			}
			String data = "";
			for (int i = 0; i < dishes.size(); i++) {
				data += dishes.keyAt(i) + "," + dishes.valueAt(i) + ";";
			}
			Editor ed = sp.edit();
			if (Cart.dishes.size() == 0) {
				Cart.currentRestoraunt = -1;
				Cart.restorauntName = "";
				ed.remove("Dishes");
				ed.remove("RestorauntID");
			} else {
				ed.putString("Dishes", data);
				ed.putInt("RestorauntID", restorauntID);
			}
			ed.commit();
			ed = null;
			data = null;
			db.close();
			return count;
		} else {
			return -1;
		}
	}

	public static void clearCart() {
		dishes.clear();
		dishes_prices.clear();
		Editor ed = sp.edit();
		Cart.currentRestoraunt = -1;
		Cart.restorauntName = "";
		ed.remove("Dishes");
		ed.remove("RestorauntID");
		ed.commit();
		ed = null;
	}

	public static int getCartAmount() {
		int amount = 0;
		for (int i = 0; i < dishes.size(); i++) {
			amount += Cart.dishes_prices.get(i) * dishes.valueAt(i);
		}
		return amount;
	}
}
