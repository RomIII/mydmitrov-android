package com.gilevich.dmitrov.background;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import com.gilevich.dmitrov.activities.MainActivity;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class AutoBaseUpdateService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		Timer t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				
				if (MainActivity.work_type == 0) {
					SharedPreferences sp = PreferenceManager
							.getDefaultSharedPreferences(getApplicationContext());
					if (sp.getBoolean("AutoUpdate", false)) {
						Calendar c = Calendar.getInstance();
						int hours = c.get(Calendar.HOUR_OF_DAY);
						int minutes = c.get(Calendar.MINUTE);
						int h = sp.getInt("Hours", -1);
						int m = sp.getInt("Minutes", -1);
						if (h == hours && m == minutes) {
							Updating u = new Updating(getApplicationContext());
							if (u.updatePlaces() < 2 && u.updateEvents() < 2
									&& u.updateTaxi() < 2
									&& u.updateDelivery() < 2
									&& u.updateServicesGroups() < 2
									&& u.updateServices() < 2) {
								DateFormat dateFormat = new SimpleDateFormat(
										"dd.MM.yyyy", Locale.ENGLISH);
								Date date = new Date();
								sp.edit()
										.putString("LastUpdate",
												dateFormat.format(date))
										.commit();
							}
						}
					}
				}
			}
		}, 0, 60000);
		return super.onStartCommand(intent, flags, startId);
	}
}
