package com.gilevich.dmitrov.background;

import android.app.Application;

import com.gilevich.dmitrov.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.parse.Parse;
import com.parse.ParseInstallation;

public class Analytics extends Application {
	public static GoogleAnalytics analytics;
	public static Tracker tracker;
	public static final String CATEGORY_NAME = "android_event";
	public static final String ADS_CATEGORY_NAME = "android_banner";
	public static final String ACTION_CALL = "android_Call";
	public static final String ACTION_SITE = "android_Site";

	@Override
	public void onCreate() {
		super.onCreate();
		analytics = GoogleAnalytics.getInstance(this);
		analytics.setLocalDispatchPeriod(1800);

		tracker = analytics.newTracker(R.xml.global_tracker);
		Parse.initialize(this, "rkWoJ6gE8Ku9wxi9rxL8pW0JPmoBbPdZHZN543yq",
				"K5VDaYzlYBirjIUBiinOM8ok7Bi0B8lw12QDbHt5");
		ParseInstallation.getCurrentInstallation().saveInBackground();
	}

	public static void sendCall(String phone, boolean isAds) {
		Analytics.tracker.send(new HitBuilders.EventBuilder()
				.setCategory(
						isAds ? Analytics.ADS_CATEGORY_NAME
								: Analytics.CATEGORY_NAME)
				.setAction(Analytics.ACTION_CALL).setLabel(phone).build());
	}

	public static void sendSite(String site, boolean isAds) {
		Analytics.tracker.send(new HitBuilders.EventBuilder()
				.setCategory(
						isAds ? Analytics.ADS_CATEGORY_NAME
								: Analytics.CATEGORY_NAME)
				.setAction(Analytics.ACTION_SITE).setLabel(site).build());
	}
}