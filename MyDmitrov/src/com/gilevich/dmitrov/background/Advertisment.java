package com.gilevich.dmitrov.background;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gilevich.dmitrov.utils.ImageUtils;
import com.gilevich.dmitrov.utils.InternetUtils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;

public class Advertisment implements OnClickListener {
	static public ArrayList<String> links, phones;
	static public ArrayList<Bitmap> images;
	static Context context;
	static final Random r = new Random();
	private final String BANNERS_LIST_URL = "http://mydmitrov.com/banner/android.php";
	static int current_banner;

	public Advertisment(Context context) {
		links = new ArrayList<>();
		phones = new ArrayList<>();
		images = new ArrayList<>();
		new ShowAds().execute();
		Advertisment.context = context;
	}

	class ShowAds extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				links.clear();
				phones.clear();
				images.clear();
				int width = ImageUtils.getWidth(context);
				String data = IOUtils.toString(new URL(BANNERS_LIST_URL));
				JSONArray jArray = new JSONArray(data);
				data = null;
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject banner = jArray.getJSONObject(i);
					String url_param = "pic";
					if (width > 320 && width <= 640)
						url_param += "2";
					else if (width > 640)
						url_param += "3";

					String url = banner.getString(url_param);
					String local_file = Updating.folder_path
							+ url.substring(url.lastIndexOf("/") + 1);
					if (InternetUtils.download(url,
							url.substring(url.lastIndexOf("/") + 1))) {
						images.add(BitmapFactory.decodeFile(local_file));
						String site = banner.getString("url");
						if (!site.startsWith("http://"))
							site = "http://" + site;
						links.add(site);
						site = null;
						phones.add(banner.getString("tel"));
					}
					local_file = null;
					url = null;
					url_param = null;
					banner = null;
				}
				jArray = null;
				return images.size() > 0;
			} catch (IOException | JSONException e) {
				links.clear();
				phones.clear();
				images.clear();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {

			super.onPostExecute(result);
			if (result) {
				new Timer().schedule(new TimerTask() {

					@Override
					public void run() {
						if (links.size() > 0)
							sendBroadcast(context);

					}
				}, 0, 60000);
			}
		}
	}

	public static void sendBroadcast(Context context) {
		if (links != null) {
			if (links.size() > 0) {
				Intent intent = new Intent("NewAdvertismentImage");
				current_banner = r.nextInt(links.size());
				intent.putExtra("AdvertismentItemNumber", current_banner);
				context.sendBroadcast(intent);
			}
		} else
			new Advertisment(context);
	}

	@Override
	public void onClick(View v) {

	}
}
