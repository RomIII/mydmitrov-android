package com.gilevich.dmitrov.background;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.SightActivity;
import com.gilevich.dmitrov.activities.SightsListActivity;
import com.google.android.gms.internal.po;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

public class SightLoading extends AsyncTask<Integer, Void, Boolean> {
	private Context context;
	private int position;

	public SightLoading(Context context) {
		this.context = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Boolean doInBackground(Integer... params) {
		Updating u = new Updating(context);
		if (params.length > 0) {
			position = params[0];
			String[] sightInfo = u.getSight(params[0]);
			if (sightInfo != null) {
				int position = SightsListActivity.sights_ids.indexOf(params[0]);
				SightsListActivity.sights_descriptions.set(position,
						sightInfo[0]);
				SightsListActivity.sights_photos.set(position, sightInfo[1]);
				SightsListActivity.sights_phones.set(position, sightInfo[2]);
				SightsListActivity.sights_sites.set(position, sightInfo[3]);
				SightsListActivity.sights_addresses.set(position, sightInfo[4]);
				SightsListActivity.sights_working_hours.set(position,
						sightInfo[5]);
				return true;
			}
		}
		return false;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if (result) {
			context.startActivity(new Intent(context, SightActivity.class)
					.putExtra("position", position));
		} else {
			new AlertDialog.Builder(context)
					.setMessage("Невозможно загрузить данные.")
					.setPositiveButton("OK", null).show();
		}
	}
}
