package com.gilevich.dmitrov.background;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class GetRestorauntMenu extends AsyncTask<Integer, Void, Boolean> {
	ProgressDialog mProgressDialog;
	Context context;

	public GetRestorauntMenu(Context context) {
		this.context = context;
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog
				.setMessage("�������� ������ ���������. ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}

	@Override
	protected Boolean doInBackground(Integer... params) {

		Updating u = new Updating(context);
		boolean result = u.updateRestoraunt(params[0]) == 0;
		u = null;
		return result;
	}

	@Override
	protected void onPostExecute(Boolean result) {

		super.onPostExecute(result);
		mProgressDialog.dismiss();
		if (result) {
			new AlertDialog.Builder(context).setMessage("�������� ���������!")
					.setPositiveButton("OK", null).show();
			new GetRestorauntsList(context, 1).execute(false);
		} else {
			new AlertDialog.Builder(context)
					.setMessage(
							"���������� ��������� ������. �������� ���������� � ����������.")
					.show();
		}
	}
}
