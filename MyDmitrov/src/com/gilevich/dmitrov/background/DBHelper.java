package com.gilevich.dmitrov.background;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	public DBHelper(Context context) {
		super(context, "DB", null, 22);
	}

	private void createTaxiTables(SQLiteDatabase arg0) {
		arg0.execSQL("create table taxi_name ("
				+ "id integer primary key autoincrement," + "name text,"
				+ "tel text," + "logo text" + ");");
		arg0.execSQL("create table taxi_tel ("
				+ "id integer primary key autoincrement," + "nomer_tel text,"
				+ "taxi_id integer," + "main integer," + "operator integer"
				+ ");");
	}

	private void createAfishaTables(SQLiteDatabase arg0) {
		arg0.execSQL("create table afisha_places (" + "id integer primary key,"
				+ "place_name text," + "place_id integer,"
				+ "place_description text," + "place_address text,"
				+ "place_tel text," + "place_logo text," + "place_site text"
				+ ");");
		arg0.execSQL("create table afisha_events ("
				+ "id integer primary key autoincrement," + "event_name text,"
				+ "place_id integer," + "date_from text," + "date_to text,"
				+ "event_time text," + "event_info text," + "event_pic text,"
				+ "event_type text," + "event_price text" + ");");
	}

	private void createDeliveryTables(SQLiteDatabase arg0) {
		arg0.execSQL("create table delivery_firms (" + "id integer ,"
				+ "delivery_name text," + "delivery_short_description text,"
				+ "delivery_long_description text," + "delivery_address text,"
				+ "delivery_phone text," + "delivery_site text,"
				+ "delivery_logo text," + "restoraunt_id int" + ");");
	}

	private void createServicesTables(SQLiteDatabase arg0) {
		arg0.execSQL("create table services_groups (" + "id integer,"
				+ "group_name text," + "group_id integer,"
				+ "group_description text" + ");");
		arg0.execSQL("create table services ("
				+ "id integer primary key autoincrement,"
				+ "service_name text," + "group_id integer,"
				+ "service_text text," + "service_phone text,"
				+ "service_address text," + "service_info text,"
				+ "service_latitude text," + "service_lontitude text,"
				+ "service_site text," + "service_logo text" + ");");
	}

	private void createRestrauntsTables(SQLiteDatabase arg0) {
		arg0.execSQL("create table restoraunts (" + "id integer,"
				+ "name text," + "phone text," + "updated text,"
				+ "user_update text," + "logo text,"
				+ "minimal_amount integer," + "delivery_time text" + ");");
		arg0.execSQL("create table sections (" + "id integer," + "name text,"
				+ "restoraunt_id integer" + ");");
		arg0.execSQL("create table dishes (" + "id integer," + "name text,"
				+ "description text," + "price integer," + "picture text,"
				+ "portion text," + "catalog_id integer,"
				+ "restoraunt_id integer" + ");");
	}

	private void createSightsTables(SQLiteDatabase arg0) {
		// arg0.execSQL("create table sights_groups (" + "id integer,"
		// + "name text," + "color text" + ");");
		// arg0.execSQL("create table sights (" + "id integer," + "name text,"
		// + "group_id integer," + "description text," + "photos text,"
		// + "main_photo text," + "sound text," + "phones text,"
		// + "site text," + "address text, working_hours text,"
		// + "coordinates text" + ");");
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		createTaxiTables(arg0);
		createAfishaTables(arg0);
		createDeliveryTables(arg0);
		createServicesTables(arg0);
		createRestrauntsTables(arg0);
		createSightsTables(arg0);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		arg0.execSQL("drop table if exists taxi_name");
		arg0.execSQL("drop table if exists taxi_tel");
		arg0.execSQL("drop table if exists afisha_places");
		arg0.execSQL("drop table if exists afisha_events");
		arg0.execSQL("drop table if exists delivery_firms");
		arg0.execSQL("drop table if exists services_groups");
		arg0.execSQL("drop table if exists services");
		arg0.execSQL("drop table if exists restoraunts");
		arg0.execSQL("drop table if exists sections");
		arg0.execSQL("drop table if exists dishes");
		arg0.execSQL("drop table if exists sights_groups");
		arg0.execSQL("drop table if exists sights");
		onCreate(arg0);
	}

}
