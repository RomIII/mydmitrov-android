package com.gilevich.dmitrov.background;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.ListView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.MainActivity;
import com.gilevich.dmitrov.activities.RestorauntsList;
import com.gilevich.dmitrov.adapters.RestorauntsListAdapter;
import com.gilevich.dmitrov.utils.DateUtils;

public class GetRestorauntsList extends AsyncTask<Boolean, Integer, Boolean> {
	boolean isUpdating;
	ProgressDialog mProgressDialog;
	Context context;
	int calledFrom;

	public GetRestorauntsList(Context context, int calledFrom) {

		this.context = context;
		this.calledFrom = calledFrom;
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		if (!RestorauntsList.isRestorauntUpdating
				&& (calledFrom == 1 || calledFrom == 3)) {
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog
					.setMessage("�������� ������ ������ \"����\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}
	}

	@Override
	protected Boolean doInBackground(Boolean... params) {

		isUpdating = params[0];
		DBHelper dbh = new DBHelper(context);
		SQLiteDatabase db = dbh.getReadableDatabase();
		Cursor c = db.query("restoraunts", null, null, null, null, null, null);
		if (MainActivity.work_type == 0 && c.moveToFirst() && !isUpdating) {
			RestorauntsList.initializeArrayLists();
			int idIndex = c.getColumnIndex("id"), nameIndex = c
					.getColumnIndex("name"), phoneIndex = c
					.getColumnIndex("phone"), updatedIndex = c
					.getColumnIndex("updated"), userUpdateIndex = c
					.getColumnIndex("user_update"), logoIndex = c
					.getColumnIndex("logo"), minimalAmountIndex = c
					.getColumnIndex("minimal_amount"), deliveryTimeIndex = c
					.getColumnIndex("delivery_time");
			do {
				RestorauntsList.id.add(c.getInt(idIndex));
				RestorauntsList.names.add(c.getString(nameIndex));
				RestorauntsList.phones.add(c.getString(phoneIndex));
				RestorauntsList.user_update.add(c.getString(userUpdateIndex));
				RestorauntsList.updated.add(c.getString(updatedIndex));
				RestorauntsList.logo.add(c.getString(logoIndex));
				RestorauntsList.minimal_amount
						.add(c.getInt(minimalAmountIndex));
				RestorauntsList.delivery_time.add(c
						.getString(deliveryTimeIndex));
			} while (c.moveToNext());
			db.close();
			dbh.close();
		} else {
			c.close();
			if (dbh != null) {
				dbh.close();
			}
			if (db != null) {
				db.close();
			}
			Updating u = new Updating(context);
			int upd;
			if (calledFrom != 1 || MainActivity.work_type == 1) {
				publishProgress(4);
				upd = u.updateRestoraunts();
			} else {
				publishProgress(0);
				u.updatePlaces();
				u.updateEvents();
				publishProgress(1);
				u.updateTaxi();
				publishProgress(2);
				u.updateDelivery();
				publishProgress(3);
				u.updateServicesGroups();
				u.updateServices();
				publishProgress(4);
				upd = u.updateRestoraunts();
				publishProgress(5);
				u.updateSights();
			}
			if (upd == 0) {
				RestorauntsList.id = u.restoraunts_ids;
				RestorauntsList.names = u.restoraunts_names;
				RestorauntsList.user_update = u.restoraunts_user_updates;
				RestorauntsList.updated = u.restoraunts_updates;
				RestorauntsList.logo = u.restoraunts_logos;
				RestorauntsList.minimal_amount = u.restoraunts_min_amount;
				RestorauntsList.delivery_time = u.restoraunts_delivery_times;
				RestorauntsList.phones = u.restoraunts_phones;
				u = null;
			} else if (upd == 2) {
				u = null;
				return false;
			}
		}
		return true;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {

		super.onProgressUpdate(values);
		if (mProgressDialog != null)
			mProgressDialog
					.setMessage("�������� ������ ������ \""
							+ Updating.modules_names[values[0]]
							+ "\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
	}

	@Override
	protected void onPostExecute(Boolean result) {

		super.onPostExecute(result);
		if (!Cart.cartCreated)
			new Cart(context);
		if (mProgressDialog != null)
			mProgressDialog.dismiss();
		if (calledFrom == 1) {
			if (result) {
				((ListView) ((Activity) context).findViewById(R.id.listView1))
						.setAdapter(new RestorauntsListAdapter(context));
				SharedPreferences sp = PreferenceManager
						.getDefaultSharedPreferences(context);
				if (MainActivity.work_type == 0) {
					Editor ed = sp.edit();
					DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
							Locale.ENGLISH);
					Date date = new Date();
					ed.putString("LastUpdate", dateFormat.format(date));
					ed.commit();
					RestorauntsList.t2.setText("������ ��������� "
							+ DateUtils.getDateText(sp.getString("LastUpdate",
									"")));
				}
				if (isUpdating) {
					if (!RestorauntsList.isRestorauntUpdating)
						new AlertDialog.Builder(context)
								.setMessage(
										"�������� ������ ��������� �������.")
								.setPositiveButton("��", null).show();
				}
				RestorauntsList.setCartButtonText();
				sp = null;
			} else {
				if (!RestorauntsList.isRestorauntUpdating)
					new AlertDialog.Builder(context)
							.setTitle("���������� ��������� ������.")
							.setMessage("��������� ���������� � ����������.")
							.setPositiveButton("��", null).show();
			}
		}
	}
}
