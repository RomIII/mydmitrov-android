package com.gilevich.dmitrov.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;

import com.gilevich.dmitrov.background.Updating;

public class InternetUtils {

	/**
	 * A method to download file from Internet to device. Saves the file to
	 * application folder.
	 * 
	 * @param path
	 *            URL of the file
	 * @param name
	 *            Name for file, which will be saved on a device
	 * @return <b>true</b> if file was downloaded successfully, or <b>false</b>
	 *         if not
	 */
	public static boolean download(String path, String name) {
		try {
			URL url = new URL(path);
			File f = new File(Updating.folder_path + name);
			if (!f.exists())
				FileUtils.copyURLToFile(url, new File(Updating.folder_path
						+ name));
			f = null;
			url = null;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

}