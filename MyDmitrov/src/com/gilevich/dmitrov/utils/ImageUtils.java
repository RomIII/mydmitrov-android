package com.gilevich.dmitrov.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class ImageUtils {

	/**
	 * 
	 * @param context
	 *            Context of activity, calling this method
	 * @return Image size for loading pictures
	 */
	public static int getSize(Context context) {
		int size = 140;
		switch (context.getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			size = 35;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			size = 50;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			size = 70;
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			size = 100;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			size = 140;
		}
		return size;
	}

	/**
	 * 
	 * @param context
	 *            Context of activity, calling this method
	 * @return Width of device screen
	 */
	public static int getWidth(Context context) {
		return context.getResources().getDisplayMetrics().widthPixels;
	}

	/**
	 * 
	 * @param context
	 *            Context of activity, calling this method
	 * @return Height of device screen
	 */
	public static int getHeight(Context context) {
		return context.getResources().getDisplayMetrics().heightPixels;
	}

	/**
	 * This method convert value in dp to pixel value
	 * 
	 * @param dp
	 *            Source value
	 * @param context
	 *            Context of activity, calling this method
	 * @return Pixel value
	 */
	public static int dpToPx(int dp, Context context) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				context.getResources().getDisplayMetrics());
	}
}
