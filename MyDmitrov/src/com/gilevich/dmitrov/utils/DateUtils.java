package com.gilevich.dmitrov.utils;

import com.gilevich.dmitrov.activities.MainActivity;

public class DateUtils {
	private static String[] months = { "������", "�������", "�����", "������",
			"���", "����", "����", "�������", "��������", "�������", "������",
			"�������" };

	/**
	 * 
	 * @param date
	 *            A {@link String} date parameter in 01.12.2015 format
	 * @return String in 01 December 2015 format, or empty string if current
	 *         work type is on-line
	 */
	public static String getDateText(String date) {
		return MainActivity.work_type == 0 ? date.substring(0, 2) + " "
				+ months[Integer.parseInt(date.substring(3, 5)) - 1] + " "
				+ date.substring(6) : "";
	}
}
