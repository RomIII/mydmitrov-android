package com.gilevich.dmitrov.beans;

/**
 * A class, defined a Sight model.
 * */
public class SightBean {

	private int id;
	private int groupId;
	private String name;
	private String description;
	private String[] photos;
	private String mainPhoto;
	private String sound;
	private String[] phones;
	private String[] sites;
	private String address;
	private String workingHours;
	private String coordinates;

	/**
	 * Default constructor, which creates a Sight with id, group id, name, and
	 * main photo.
	 * 
	 * @param id
	 *            ID of the sight
	 * @param groupId
	 *            ID of the sight's group
	 * @param name
	 *            Name of the sight
	 * @param mainPhoto
	 *            Path to main photo of the sight
	 */
	public SightBean(int id, int groupId, String name, String mainPhoto) {
		this.id = id;
		this.groupId = groupId;
		this.name = name;
		this.mainPhoto = mainPhoto;
	}

	/**
	 * @return ID of the sight
	 */
	public int getId() {
		return id;
	}

	public int getGroupId() {
		return groupId;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String[] getPhotos() {
		return photos;
	}

	public void setPhotos(String[] photos) {
		this.photos = photos;
	}

	public String getMainPhoto() {
		return mainPhoto;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

	public String[] getPhones() {
		return phones;
	}

	public void setPhones(String[] phones) {
		this.phones = phones;
	}

	public String[] getSites() {
		return sites;
	}

	public void setSites(String[] sites) {
		this.sites = sites;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

}