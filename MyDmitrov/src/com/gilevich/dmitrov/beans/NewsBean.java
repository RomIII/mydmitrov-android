package com.gilevich.dmitrov.beans;

public class NewsBean {
	private int id;
	private String title;
	private String text;
	private String url;
	private String date;
	private String image;

	/**
	 * Creates a new NewsBean object, and set to it given values
	 * 
	 * @param id
	 *            News id
	 * @param title
	 *            News title
	 * @param text
	 *            News text
	 * @param url
	 *            News URL
	 * @param date
	 *            News date
	 * @param image
	 *            News image path
	 */
	public NewsBean(int id, String title, String text, String url, String date,
			String image) {
		this.id = id;
		this.title = title;
		this.text = text;
		this.url = url;
		this.date = date;
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getText() {
		return text;
	}

	public String getUrl() {
		return url;
	}

	public String getDate() {
		return date;
	}

	public String getImage() {
		return image;
	}

}