package com.gilevich.dmitrov.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.adapters.SightsGroupAdapter;
import com.gilevich.dmitrov.background.SightLoading;
import com.gilevich.dmitrov.background.SightsGroupsLoader;
import com.gilevich.dmitrov.utils.DateUtils;

public class SightsListActivity extends ActionBarActivity implements
		OnClickListener, OnChildClickListener,
		android.content.DialogInterface.OnClickListener {

	static public List<Integer> sights_groups_ids, sights_ids,
			sights_group_ids, sights_groups_starts, sights_groups_ends;
	static public List<String> sights_groups_names, sights_names,
			sights_groups_colors, sights_descriptions, sights_main_photos,
			sights_photos, sights_phones, sights_sites, sights_addresses,
			sights_working_hours, sights_coordinates, sights_sounds;
	static public ExpandableListView groups_list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sights_activity);
		sights_groups_ids = new ArrayList<>();
		sights_groups_names = new ArrayList<>();
		sights_groups_colors = new ArrayList<>();
		sights_groups_starts = new ArrayList<>();
		sights_groups_ends = new ArrayList<>();
		sights_ids = new ArrayList<>();
		sights_group_ids = new ArrayList<>();
		sights_names = new ArrayList<>();
		sights_descriptions = new ArrayList<>();
		sights_main_photos = new ArrayList<>();
		sights_photos = new ArrayList<>();
		sights_phones = new ArrayList<>();
		sights_sites = new ArrayList<>();
		sights_addresses = new ArrayList<>();
		sights_working_hours = new ArrayList<>();
		sights_coordinates = new ArrayList<>();
		sights_sounds = new ArrayList<>();
		groups_list = (ExpandableListView) findViewById(R.id.sights_groups_listview);
		groups_list.setOnChildClickListener(this);
		findViewById(R.id.button_logo).setOnClickListener(this);
		findViewById(R.id.button_refresh).setOnClickListener(this);
		setSettingButtonClickListener();
		if (MainActivity.work_type == -1)
			MainActivity.work_type = PreferenceManager
					.getDefaultSharedPreferences(this).getInt("work_type", 0);
		new SightsGroupsLoader(this).execute(false);
	}

	@Override
	protected void onStart() {
		super.onStart();
		findViewById(R.id.last_update).setVisibility(
				MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE
						: View.INVISIBLE);
		setSettingButtonClickListener();
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(SightsListActivity.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												SightsListActivity.this,
												MainActivity.work_type == 0 ? 1
														: 0),
										SightsListActivity.this).show();
					}
				});
	}

	static public void setAdapter(Context context) {
		groups_list.setAdapter(new SightsGroupAdapter(context));
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		((TextView) ((Activity) context).findViewById(R.id.last_update))
				.setText("������ ��������� "
						+ DateUtils.getDateText(sp.getString("LastUpdate", "")));
		sp = null;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_logo:
			finish();
			break;
		case R.id.button_refresh:
			new SightsGroupsLoader(this).execute(true);
			break;
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		if (MainActivity.work_type == 0) {
			startActivity(new Intent(this, SightActivity.class).putExtra(
					"position", (int) v.getTag()));
		} else {
			new SightLoading(this).execute((int) v.getTag());
		}
		return true;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			if (MainActivity.work_type == 1)
				startActivity(new Intent(this, Settings.class));
			else {
				new SightsGroupsLoader(this).execute(true);
			}
			break;
		case 2:
			startActivity(new Intent(this, Settings.class));
		}
	}
}
