package com.gilevich.dmitrov.activities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.Analytics;
import com.gilevich.dmitrov.background.Updating;
import com.gilevich.dmitrov.utils.ImageUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class ServiceInfo extends Activity implements OnClickListener,
		android.content.DialogInterface.OnClickListener {
	int id, d = 0;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.service_info);
		id = getIntent().getIntExtra("ID", -1);
		int size = ImageUtils.getSize(this);
		LayoutParams lp = new LayoutParams(size, LayoutParams.WRAP_CONTENT);
		lp.setMargins(5, 5, 5, 5);
		String logo = ServicesList.services_logos.get(id);
		if (logo != null) {
			String t = ServicesList.services_logos.get(id);
			if (t.lastIndexOf("g") == t.length() - 1) {
				((ImageView) findViewById(R.id.imageView1)).setLayoutParams(lp);
			} else {
				t = t.substring(0, t.lastIndexOf("g") + 1);
			}
			((ImageView) findViewById(R.id.imageView1))
					.setImageDrawable(Drawable.createFromPath(t));
			logo = null;
		} else
			((ImageView) findViewById(R.id.imageView1))
					.setVisibility(View.GONE);
		((TextView) findViewById(R.id.textView2))
				.setText(ServicesList.services_names.get(id));
		String phone = ServicesList.services_phones.get(id);
		if (phone.length() > 0) {
			String[] phones = phone.split(",");
			LayoutInflater lInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			LinearLayout phones_layout = (LinearLayout) findViewById(R.id.linearLayout1);
			for (int i = 0; i < phones.length; i++) {
				View layout = lInflater.inflate(R.layout.service_phone,
						phones_layout, false);
				((TextView) layout.findViewById(R.id.textView4))
						.setText(phones[i]);
				((TextView) layout.findViewById(R.id.textView4))
						.setOnClickListener(this);
				phones_layout.addView(layout, 2 + i);
				layout = null;
			}
			phones_layout = null;
			lInflater = null;
		}
		if (MainActivity.work_type == 0) {
			if (ServicesList.services_addresses.get(id).length() > 0) {
				((TextView) findViewById(R.id.textView5))
						.setText(ServicesList.services_addresses.get(id));
				((LinearLayout) findViewById(R.id.addressLayout1))
						.setVisibility(View.VISIBLE);
			}
			if (ServicesList.services_sites.get(id).length() > 0) {
				((TextView) findViewById(R.id.textView6))
						.setText(ServicesList.services_sites.get(id));
				((TextView) findViewById(R.id.textView6))
						.setOnClickListener(this);
				((LinearLayout) findViewById(R.id.siteLayout1))
						.setVisibility(View.VISIBLE);
			}
			if (ServicesList.services_texts.get(id).length() > 0) {
				((TextView) findViewById(R.id.textView7))
						.setText(ServicesList.services_texts.get(id));
				((TextView) findViewById(R.id.textView7))
						.setVisibility(View.VISIBLE);
			}
			if (ServicesList.services_latitudes.get(id) != null
					&& ServicesList.services_lontitudes.get(id) != null) {
				((Button) findViewById(R.id.button1))
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {

								ServiceInfo.this
										.startActivity(new Intent(
												ServiceInfo.this,
												MapActivity.class)
												.putExtra(
														"coordinates",
														new double[] {
																Double.parseDouble(ServicesList.services_latitudes
																		.get(id)),
																Double.parseDouble(ServicesList.services_lontitudes
																		.get(id)) })
												.putExtra(
														"data",
														new String[] {
																ServicesList.services_names
																		.get(id),
																ServicesList.services_addresses
																		.get(id)
																		.length() > 0 ? ServicesList.services_addresses
																		.get(id)
																		: "" }));
							}
						});
				((Button) findViewById(R.id.button1))
						.setVisibility(View.VISIBLE);
			}
		} else {
			new GetServiceInfo().execute(ServicesList.services_ids.get(id));
		}
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(ServiceInfo.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												ServiceInfo.this, 0),
										ServiceInfo.this).show();
						d = 0;
					}
				});
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						finish();
					}
				});
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							ServiceInfo.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {

		if (d == 0) {
			switch (which) {
			case 0:
				startActivity(new Intent(this, About.class));
				break;
			case 1:
				startActivity(new Intent(this, Settings.class));
			}
		} else if (d == 2) {
			String site = ((TextView) findViewById(R.id.textView6)).getText()
					.toString();
			Analytics.sendSite(site, false);
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(site)));
			site = null;
		}
	}

	@Override
	public void onClick(View arg0) {

		switch (arg0.getId()) {
		case R.id.textView4:
			Analytics.sendCall(((TextView) arg0).getText().toString(), false);
			startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri
					.parse("tel:" + ((TextView) arg0).getText().toString())));
			break;
		case R.id.textView6:
			d = 2;
			new AlertDialog.Builder(this).setTitle("������� �� ����")
					.setMessage(((TextView) arg0).getText() + "?")
					.setPositiveButton("��", this)
					.setNegativeButton("���", null).create().show();
		}
	}

	class GetServiceInfo extends AsyncTask<Integer, Void, JSONObject> {

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

		@Override
		protected JSONObject doInBackground(Integer... params) {
			JSONArray service_array = Updating
					.getJSONFromURL("http://mydmitrov.com/json/android/directory_level2.php?id="
							+ params[0]);
			if (service_array == null)
				return null;
			try {
				return service_array.getJSONObject(0);
			} catch (JSONException e) {
				service_array = null;
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {

			super.onPostExecute(result);
			if (result == null)
				new AlertDialog.Builder(ServiceInfo.this)
						.setMessage("���������� ��������� ������.")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										finish();
									}
								}).setCancelable(false).show();
			else {
				try {
					final String address = result.getString("adr");
					if (address.length() > 0) {
						((TextView) findViewById(R.id.textView5))
								.setText(address);
						((LinearLayout) findViewById(R.id.addressLayout1))
								.setVisibility(View.VISIBLE);
					}
					String site = result.getString("site");
					if (site.length() > 0) {
						((TextView) findViewById(R.id.textView6)).setText(site);
						((TextView) findViewById(R.id.textView6))
								.setOnClickListener(ServiceInfo.this);
						((LinearLayout) findViewById(R.id.siteLayout1))
								.setVisibility(View.VISIBLE);
					}
					String text = result.getString("text");
					if (text.length() > 0) {
						((TextView) findViewById(R.id.textView7)).setText(text);
						((TextView) findViewById(R.id.textView7))
								.setVisibility(View.VISIBLE);
					}
					final Double lat = result.getDouble("lat"), lon = result
							.getDouble("lon");
					if (lat != 0 && lon != 0) {
						((Button) findViewById(R.id.button1))
								.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {

										ServiceInfo.this
												.startActivity(new Intent(
														ServiceInfo.this,
														MapActivity.class)
														.putExtra(
																"coordinates",
																new double[] {
																		lat,
																		lon })
														.putExtra(
																"data",
																new String[] {
																		ServicesList.services_names
																				.get(id),
																		address.length() > 0 ? address
																				: "" }));
									}
								});
						((Button) findViewById(R.id.button1))
								.setVisibility(View.VISIBLE);
					}
					site = null;
					text = null;
				} catch (JSONException e) {
				}
			}
		}
	}
}