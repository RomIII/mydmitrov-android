package com.gilevich.dmitrov.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.utils.ImageUtils;

public class SightActivity extends FragmentActivity implements OnClickListener {
	private LinearLayout phonesLayout;
	private LinearLayout photosLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sight_activity);
		int position = getIntent().getIntExtra("position", -1);
		if (MainActivity.work_type == 0)
			if (position >= 0) {
				setSightInformation(position);
			} else
				finish();
	}

	private void setSightInformation(int position) {
		phonesLayout = (LinearLayout) findViewById(R.id.sight_phones_layout);
		photosLayout = (LinearLayout) findViewById(R.id.sight_photos_layout);
		findViewById(R.id.button_logo).setOnClickListener(this);

		((TextView) findViewById(R.id.sight_name))
				.setText(SightsListActivity.sights_names.get(position));

		((TextView) findViewById(R.id.sight_group_name))
				.setText(SightsListActivity.sights_groups_names
						.get(SightsListActivity.sights_groups_ids
								.indexOf(SightsListActivity.sights_group_ids
										.get(position))));

		((TextView) findViewById(R.id.sight_description))
				.setText(SightsListActivity.sights_descriptions.get(position));

		String address = SightsListActivity.sights_addresses.get(position);
		if (address.length() > 0)
			((TextView) findViewById(R.id.sight_address)).setText(address);
		else
			findViewById(R.id.sight_address).setVisibility(View.GONE);

		String working_hours = SightsListActivity.sights_working_hours
				.get(position);
		if (working_hours.length() > 0) {
			((TextView) findViewById(R.id.sight_working_hours)).setText(String
					.format(getResources().getString(R.string.working_hours),
							working_hours));
			working_hours = null;
		} else
			findViewById(R.id.sight_working_hours).setVisibility(View.GONE);

		setPhones(SightsListActivity.sights_phones.get(position));

		setPhotos(SightsListActivity.sights_photos.get(position));
	}

	private void setPhones(String phones) {
		LayoutInflater lInflater = LayoutInflater.from(this);
		if (phones.length() > 0) {
			String[] phonesArray = phones.split(",");
			for (int i = 0; i < phonesArray.length; i++) {
				View v = lInflater.inflate(R.layout.sight_phone_item,
						phonesLayout, false);
				((TextView) v.findViewById(R.id.sight_phone))
						.setText(phonesArray[i].trim());
				phonesLayout.addView(v);
				v = null;
			}
		} else
			phonesLayout.setVisibility(View.GONE);
		lInflater = null;
	}

	private void setPhotos(String photos) {
		if (photos != null) {
			int padding = ImageUtils.dpToPx(5, this);
			String[] photosArray = photos.split(",");
			for (int i = 0; i < photosArray.length; i++) {
				ImageView v = new ImageView(this);
				v.setImageDrawable(Drawable.createFromPath(photosArray[i]));
				v.setAdjustViewBounds(true);
				if (i > 0)
					v.setPadding(0, padding, 0, 0);
				photosLayout.addView(v);
				v = null;
			}
		} else
			photosLayout.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_logo:
			finish();
			break;
		}
	}
}