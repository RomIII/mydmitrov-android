package com.gilevich.dmitrov.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.adapters.ServicesListAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.Updating;
import com.gilevich.dmitrov.utils.DateUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ServicesList extends Activity implements
		android.content.DialogInterface.OnClickListener {
	public static ArrayList<String> services_names, services_phones,
			services_addresses, services_sites, services_texts, services_logos,
			services_info, services_latitudes, services_lontitudes;
	public static ArrayList<Integer> services_ids, services_group_ids;
	DBHelper dbHelper;
	SQLiteDatabase db;
	int isUpd;
	ProgressDialog mProgressDialog;
	TextView t2;
	final String doenstLoaded = "������ �����������. ��� �������� ������ ������� ������ � ������� ������ ����";
	int id;
	int d = 0;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.services_list);
		id = getIntent().getIntExtra("ID", -1);
		t2 = (TextView) findViewById(R.id.textView2);
		new ServicesLoading().execute();
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						finish();
					}
				});
		setSettingButtonClickListener();
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							ServicesList.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(ServicesList.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												ServicesList.this,
												MainActivity.work_type % 1),
										ServicesList.this).show();
					}
				});
	}

	@Override
	protected void onStart() {
		super.onStart();
		t2.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		setSettingButtonClickListener();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			startActivity(new Intent(this, Settings.class));

		}
	}

	class ServicesLoading extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(ServicesList.this);
			mProgressDialog
					.setMessage("�������� ������ ������ \"����������\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			((ListView) findViewById(R.id.listView1))
					.setAdapter(new ServicesListAdapter(ServicesList.this));
			SharedPreferences sp = PreferenceManager
					.getDefaultSharedPreferences(ServicesList.this);
			if (isUpd == 1) {
				if (MainActivity.work_type == 0) {
					Editor ed = sp.edit();
					DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
							Locale.ENGLISH);
					Date date = new Date();
					ed.putString("LastUpdate", dateFormat.format(date));
					ed.commit();
					t2.setText("������ ��������� "
							+ DateUtils.getDateText(dateFormat.format(date)));
				}
				new AlertDialog.Builder(ServicesList.this)
						.setMessage("�������� ������ ��������� �������.")
						.setPositiveButton("��", null).show();
			} else {
				if (MainActivity.work_type == 0)
					t2.setText("������ ��������� "
							+ DateUtils.getDateText(sp.getString("LastUpdate",
									"")));
			}
			isUpd = 0;
		}

		@Override
		protected Void doInBackground(Void... params) {
			services_ids = new ArrayList<>();
			services_names = new ArrayList<>();
			services_phones = new ArrayList<>();
			services_addresses = new ArrayList<>();
			services_sites = new ArrayList<>();
			services_texts = new ArrayList<>();
			services_logos = new ArrayList<>();
			services_group_ids = new ArrayList<>();
			services_info = new ArrayList<>();
			services_latitudes = new ArrayList<>();
			services_lontitudes = new ArrayList<>();
			dbHelper = new DBHelper(ServicesList.this);
			db = dbHelper.getWritableDatabase();
			if (MainActivity.work_type == 0) {
				Cursor c = db.query("services", null, "group_id = " + id, null,
						null, null, null);
				if (c.moveToFirst()) {
					int nameIndex = c.getColumnIndex("service_name");
					int idIndex = c.getColumnIndex("group_id");
					int phoneIndex = c.getColumnIndex("service_phone");
					int addressIndex = c.getColumnIndex("service_address");
					int siteIndex = c.getColumnIndex("service_site");
					int textIndex = c.getColumnIndex("service_text");
					int logoIndex = c.getColumnIndex("service_logo");
					int infoIndex = c.getColumnIndex("service_info");
					int latIndex = c.getColumnIndex("service_latitude");
					int lonIndex = c.getColumnIndex("service_lontitude");
					do {
						services_names.add(c.getString(nameIndex));
						services_group_ids.add(c.getInt(idIndex));
						services_phones.add(c.getString(phoneIndex));
						services_addresses.add(c.getString(addressIndex));
						services_sites.add(c.getString(siteIndex));
						services_logos.add(c.getString(logoIndex));
						services_texts.add(c.getString(textIndex));
						services_info.add(c.getString(infoIndex));
						services_latitudes.add(c.getString(latIndex));
						services_lontitudes.add(c.getString(lonIndex));
					} while (c.moveToNext());
				}
				c.close();
				db.close();
			} else {
				db.close();
				Updating u = new Updating(ServicesList.this);
				int upd;
				if (MainActivity.work_type == 1) {
					upd = u.updateServices(id);
				} else {
					u.updatePlaces();
					u.updateEvents();
					u.updateTaxi();
					u.updateDelivery();
					u.updateServicesGroups();
					upd = u.updateServices();
					u.updateRestoraunts();
					//u.updateSights();
				}
				if (upd == 0) {
					services_ids = u.services_id;
					services_names = u.services_names;
					services_phones = u.services_phones;
					services_addresses = u.services_addresses;
					services_sites = u.services_sites;
					services_texts = u.services_texts;
					services_logos = u.services_logos;
					services_group_ids = u.services_group_ids;
					services_info = u.services_info;
					services_latitudes = u.services_latitudes;
					services_lontitudes = u.services_lontitudes;
				}
				u = null;
			}
			return null;
		}
	}

}
