package com.gilevich.dmitrov.activities;

import java.util.ArrayList;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.AfishaPlaceEventsAdapter;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.Analytics;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.Updating;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AfishaPlacesInfo extends Activity implements OnClickListener,
		android.content.DialogInterface.OnClickListener, OnItemClickListener {
	ProgressDialog mProgressDialog;
	public int id;
	int d = 0;
	public static ArrayList<String> event_names, event_dates_from,
			event_dates_to, event_times, event_info, event_pic, event_type,
			event_price;
	public static ArrayList<Integer> event_ids;
	View v;
	ListView l1;
	boolean hasHeader = false;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.afisha_place_info);
		l1 = (ListView) findViewById(R.id.listView1);
		v = getLayoutInflater().inflate(R.layout.afisha_place_info_header, l1,
				false);
		id = getIntent().getIntExtra("ID", 0);
		TextView name = (TextView) v.findViewById(R.id.textView1);
		TextView address = (TextView) v.findViewById(R.id.textView2);
		TextView phone = (TextView) v.findViewById(R.id.textView3);
		TextView site = (TextView) v.findViewById(R.id.textView4);
		TextView descr = (TextView) v.findViewById(R.id.textView5);
		name.setText(AfishaPlacesChoose.places_names.get(id));
		if (AfishaPlacesChoose.place_address.get(id).length() > 0) {
			address.setText(AfishaPlacesChoose.place_address.get(id));
		} else {
			((LinearLayout) v.findViewById(R.id.linearLayout2))
					.setVisibility(View.GONE);
		}
		if (AfishaPlacesChoose.place_tel.get(id).length() > 0) {
			phone.setText(AfishaPlacesChoose.place_tel.get(id));
			phone.setOnClickListener(this);
		} else {
			((LinearLayout) v.findViewById(R.id.linearLayout1))
					.setVisibility(View.GONE);
		}
		if (AfishaPlacesChoose.place_site.get(id).length() > 0) {
			site.setText(AfishaPlacesChoose.place_site.get(id));
			site.setOnClickListener(this);
		} else {
			((LinearLayout) v.findViewById(R.id.linearLayout3))
					.setVisibility(View.GONE);
		}
		if (AfishaPlacesChoose.place_description.get(id).length() > 0) {
			descr.setText(AfishaPlacesChoose.place_description.get(id));
		} else {
			descr.setHeight(0);
		}
		FromBase fb = new FromBase();
		fb.execute();
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						new AlertDialog.Builder(AfishaPlacesInfo.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												AfishaPlacesInfo.this, 0),
										AfishaPlacesInfo.this).show();
						d = 0;
					}
				});
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						finish();
					}
				});
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							AfishaPlacesInfo.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		
		if (d == 0) {
			switch (which) {
			case 0:
				startActivity(new Intent(this, About.class));
				break;
			case 1:
				startActivity(new Intent(this, Settings.class));
			}
		} else if (d == 2) {
			Analytics.sendSite(AfishaPlacesChoose.place_site.get(id), false);
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse(AfishaPlacesChoose.place_site.get(id))));
		}
	}

	@Override
	public void onClick(View arg0) {
		
		switch (arg0.getId()) {
		case R.id.textView3:
			Analytics.sendCall(AfishaPlacesChoose.place_tel.get(id), false);
			startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri
					.parse("tel:" + AfishaPlacesChoose.place_tel.get(id))));
			break;
		case R.id.textView4:
			d = 2;
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			adb = new AlertDialog.Builder(this);
			adb.setTitle("������� �� ����");
			adb.setMessage(AfishaPlacesChoose.place_site.get(id) + "?");
			adb.setPositiveButton("��", this);
			adb.setNegativeButton("���", null);
			adb.create();
			adb.show();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		if (arg2 > 0) {
			Intent intent = new Intent(this, AfishaEventInfo.class);
			intent.putExtra("ID", arg2 - 1);
			intent.putExtra("isPlace", 1);
			intent.putExtra("Name", AfishaPlacesChoose.places_names.get(id));
			startActivity(intent);
		}
	}

	class FromBase extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(AfishaPlacesInfo.this);
			mProgressDialog.setMessage("���������� ���������...");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			
			super.onPostExecute(result);
			AfishaPlaceEventsAdapter apea = new AfishaPlaceEventsAdapter(
					AfishaPlacesInfo.this);
			if (!hasHeader) {
				l1.addHeaderView(v);
				hasHeader = true;
			}
			l1.setAdapter(apea);
			l1.setOnItemClickListener(AfishaPlacesInfo.this);
			mProgressDialog.dismiss();
		}

		@Override
		protected Void doInBackground(Void... params) {
			
			event_ids = new ArrayList<>();
			event_names = new ArrayList<>();
			event_times = new ArrayList<>();
			event_info = new ArrayList<>();
			event_pic = new ArrayList<>();
			event_type = new ArrayList<>();
			event_price = new ArrayList<>();
			event_dates_from = new ArrayList<>();
			event_dates_to = new ArrayList<>();
			DBHelper dbHelper = new DBHelper(AfishaPlacesInfo.this);
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			if (MainActivity.work_type == 0) {
				Cursor c = db.query("afisha_events", null, "place_id = "
						+ String.valueOf(AfishaPlacesChoose.places_id.get(id)),
						null, null, null, null);
				if (c.moveToFirst()) {
					int event_nameIndex = c.getColumnIndex("event_name");
					int date_fromIndex = c.getColumnIndex("date_from");
					int date_toIndex = c.getColumnIndex("date_to");
					int timeIndex = c.getColumnIndex("event_time");
					int infoIndex = c.getColumnIndex("event_info");
					int picIndex = c.getColumnIndex("event_pic");
					int typeIndex = c.getColumnIndex("event_type");
					int priceIndex = c.getColumnIndex("event_price");
					do {
						event_names.add(c.getString(event_nameIndex));
						event_dates_from.add(c.getString(date_fromIndex));
						event_dates_to.add(c.getString(date_toIndex));
						event_times.add(c.getString(timeIndex));
						event_info.add(c.getString(infoIndex));
						event_pic.add(c.getString(picIndex));
						event_type.add(c.getString(typeIndex));
						event_price.add(c.getString(priceIndex));
					} while (c.moveToNext());
					c.close();
					if (dbHelper != null) {
						dbHelper.close();
					}
					if (db != null) {
						db.close();
					}
				}
			} else {
				Updating u = new Updating(AfishaPlacesInfo.this);
				int upd = u.updateEvents(id);
				if (upd == 0) {
					event_ids = u.event_id;
					event_names = u.event_names;
					event_dates_from = u.event_dates_from;
					event_dates_to = u.event_dates_to;
					event_times = u.event_times;
					event_info = u.event_info;
					event_pic = u.event_pic;
					event_type = u.event_type;
					event_price = u.event_price;
				}
				u = null;
			}
			return null;
		}
	}
}
