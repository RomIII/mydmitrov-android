package com.gilevich.dmitrov.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.adapters.TaxiAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.GetRestorauntsList;
import com.gilevich.dmitrov.background.Updating;
import com.gilevich.dmitrov.utils.DateUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class TaxiMain extends Activity implements OnClickListener,
		android.content.DialogInterface.OnClickListener {
	public static ListView lv1;
	public static ArrayList<String> name, phone, phones, main_numbers, logos;
	public static ArrayList<Integer> id, isMain, operator;
	DBHelper dbHelper;
	SQLiteDatabase db;
	ContentValues cv;
	ProgressDialog mProgressDialog;
	Context context;
	ImageButton b1;
	TaxiAdapter adapter;
	TextView t2;
	final String doenstLoaded = "������ �����������. ��� �������� ������ ������� ������ � ������� ������ ����";
	int isUpd;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.taxi_main);
		name = new ArrayList<String>();
		phone = new ArrayList<String>();
		logos = new ArrayList<String>();
		phones = new ArrayList<String>();
		id = new ArrayList<Integer>();
		isMain = new ArrayList<Integer>();
		operator = new ArrayList<Integer>();
		b1 = (ImageButton) findViewById(R.id.button_refresh);
		b1.setOnClickListener(this);
		t2 = (TextView) findViewById(R.id.textView2);
		if (MainActivity.work_type == 1)
			t2.setVisibility(View.GONE);
		String lu = PreferenceManager.getDefaultSharedPreferences(this)
				.getString("LastUpdate", MainActivity.doenstLoaded);
		if (lu.equals(MainActivity.doenstLoaded))
			t2.setText(lu);
		else
			t2.setText("������ ��������� " + DateUtils.getDateText(lu));
		if (MainActivity.work_type == -1)
			MainActivity.work_type = PreferenceManager
					.getDefaultSharedPreferences(this).getInt("work_type", 0);
		new LoadAndParse().execute(false);
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						finish();
					}
				});
		setSettingButtonClickListener();
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							TaxiMain.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						new AlertDialog.Builder(TaxiMain.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(TaxiMain.this,
												MainActivity.work_type == 0 ? 1
														: 0), TaxiMain.this)
								.show();
					}
				});
	}

	@Override
	protected void onStart() {
		super.onStart();
		t2.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE
						: View.INVISIBLE);
		setSettingButtonClickListener();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 1), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		switch (arg1) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			if (MainActivity.work_type == 1)
				startActivity(new Intent(this, Settings.class));
			else {
				new LoadAndParse().execute(true);
			}
			break;
		case 2:
			startActivity(new Intent(this, Settings.class));
		}
	}

	class LoadAndParse extends AsyncTask<Boolean, Integer, Boolean> {
		ArrayList<String> n, p;
		private String[] modules_names;

		public LoadAndParse() {
			modules_names = getResources().getStringArray(R.array.modules);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			mProgressDialog
					.setMessage("�������� ������ ������ \""
							+ modules_names[values[0]]
							+ "\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(TaxiMain.this);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			publishProgress(0);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			if (result) {
				((ListView) findViewById(R.id.listView1))
						.setAdapter(new TaxiAdapter(TaxiMain.this));
				SharedPreferences sp = PreferenceManager
						.getDefaultSharedPreferences(TaxiMain.this);
				if (isUpd == 1) {
					if (MainActivity.work_type == 0) {
						Editor ed = sp.edit();
						DateFormat dateFormat = new SimpleDateFormat(
								"dd.MM.yyyy", Locale.ENGLISH);
						Date date = new Date();
						ed.putString("LastUpdate", dateFormat.format(date));
						ed.commit();
					}
				}
				if (MainActivity.work_type == 0)
					t2.setText("������ ��������� "
							+ DateUtils.getDateText(sp.getString("LastUpdate",
									"")));
				isUpd = 0;
			} else {
				new AlertDialog.Builder(TaxiMain.this)
						.setTitle("���������� ��������� ������.")
						.setMessage("����������� ���������� � ����������.")
						.setPositiveButton("��", null).show();
			}
			modules_names = null;
		}

		@SuppressLint("NewApi")
		@Override
		protected Boolean doInBackground(Boolean... params) {
			dbHelper = new DBHelper(context);
			db = dbHelper.getWritableDatabase();
			Cursor c = db
					.query("taxi_name", null, null, null, null, null, null);
			if (MainActivity.work_type == 0 && c.moveToFirst() && !params[0]) {
				int nameIndex = c.getColumnIndex("name");
				int telIndex = c.getColumnIndex("tel");
				int logoIndex = c.getColumnIndex("logo");
				do {
					name.add(c.getString(nameIndex));
					phone.add(c.getString(telIndex));
					logos.add(c.getString(logoIndex));
				} while (c.moveToNext());
				c.close();
				c = db.query("taxi_tel", null, null, null, null, null, null);
				c.moveToFirst();
				int nomerIndex = c.getColumnIndex("nomer_tel");
				int taxiIndex = c.getColumnIndex("taxi_id");
				int mainIndex = c.getColumnIndex("main");
				int operatorIndex = c.getColumnIndex("operator");
				do {
					phones.add(c.getString(nomerIndex));
					id.add(c.getInt(taxiIndex));
					isMain.add(c.getInt(mainIndex));
					operator.add(c.getInt(operatorIndex));
				} while (c.moveToNext());
				c.close();
				if (dbHelper != null) {
					dbHelper.close();
				}
				if (db != null) {
					db.close();
				}
			} else {
				isUpd = 1;
				c.close();
				if (dbHelper != null) {
					dbHelper.close();
				}
				if (db != null) {
					db.close();
				}
				Updating u = new Updating(TaxiMain.this);
				int upd;
				if (MainActivity.work_type == 1) {
					publishProgress(1);
					upd = u.updateTaxi();
				} else {
					publishProgress(0);
					u.updatePlaces();
					u.updateEvents();
					publishProgress(1);
					upd = u.updateTaxi();
					publishProgress(2);
					u.updateDelivery();
					publishProgress(3);
					u.updateServicesGroups();
					u.updateServices();
					publishProgress(4);
					u.updateRestoraunts();
					publishProgress(5);
					//u.updateSights();
					try {
						GetRestorauntsList get = new GetRestorauntsList(
								TaxiMain.this, 2);
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
							get.executeOnExecutor(
									AsyncTask.THREAD_POOL_EXECUTOR, false)
									.get();
						else
							get.execute(false).get();
						get = null;
					} catch (InterruptedException | ExecutionException e) {
					}
				}
				if (upd == 0) {
					name = u.taxi_names;
					phone = u.taxi_phone;
					logos = u.taxi_logos;
					phones = u.taxi_phones;
					id = u.taxi_ids;
					isMain = u.taxi_isMain;
					operator = u.taxi_operators;
					u = null;
				} else if (upd == 2) {
					u = null;
					return false;
				}
			}
			return true;
		}
	}

	@Override
	public void onClick(View arg0) {
		new LoadAndParse().execute(true);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
		if (dbHelper != null) {
			dbHelper.close();
		}
		if (db != null) {
			db.close();
		}
	}
}
