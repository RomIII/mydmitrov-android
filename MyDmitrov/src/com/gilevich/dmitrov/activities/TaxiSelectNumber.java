package com.gilevich.dmitrov.activities;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.adapters.TaxiAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.DBHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class TaxiSelectNumber extends Activity implements OnClickListener,
		OnCheckedChangeListener,
		android.content.DialogInterface.OnClickListener {
	RadioGroup r1;
	TextView t1;
	Button b1;
	DBHelper dbHelper;
	SQLiteDatabase db;
	ContentValues cv;
	String name, cur_phone, new_phone;
	int taxi_id, checked;
	RadioButton[] rbs;
	ImageView i1;
	ProgressDialog mProgressDialog;
	Context context;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.taxi_selectnumber);
		r1 = (RadioGroup) findViewById(R.id.radioGroup1);
		t1 = (TextView) findViewById(R.id.textView1);
		b1 = (Button) findViewById(R.id.button1);
		i1 = (ImageView) findViewById(R.id.imageView1);
		b1.setOnClickListener(this);
		name = TaxiAdapter.t1.getText().toString();
		cur_phone = TaxiAdapter.t2.getText().toString();
		t1.setText(name);
		taxi_id = TaxiMain.id.get(TaxiMain.phones.indexOf(TaxiMain.phone
				.get(TaxiMain.name.indexOf(name))));
		int size = 0;
		switch (getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			size = 35;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			size = 50;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			size = 70;
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			size = 100;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			size = 140;
		}
		LayoutParams lp = new LayoutParams(size, LayoutParams.WRAP_CONTENT);
		lp.setMargins(5, 5, 5, 5);
		String image_path = TaxiMain.logos.get(taxi_id);
		if (image_path != null && image_path.length() > 0) {
			if (image_path.lastIndexOf("g") == image_path.length() - 1) {
				i1.setLayoutParams(lp);
			} else {
				image_path = image_path.substring(0,
						image_path.lastIndexOf("g") + 1);
			}
			i1.setImageDrawable(Drawable.createFromPath(image_path));
		} else
			i1.setVisibility(View.GONE);
		image_path = null;
		int a = -1;
		rbs = new RadioButton[TaxiMain.id.lastIndexOf(taxi_id)
				- TaxiMain.id.indexOf(taxi_id) + 1];
		for (int i = TaxiMain.id.indexOf(taxi_id); i <= TaxiMain.id
				.lastIndexOf(taxi_id); i++) {
			a++;
			rbs[a] = new RadioButton(this);
			rbs[a].setText(TaxiMain.phones.get(i));
			rbs[a].setTextColor(Color.parseColor("#007EE7"));
			switch (TaxiMain.operator.get(i)) {
			case 1:
				rbs[a].setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.landline, 0);
				break;
			case 2:
				rbs[a].setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.mts, 0);
				break;
			case 3:
				rbs[a].setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.beeline, 0);
				break;
			case 4:
				rbs[a].setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.megaphone, 0);
				break;
			case 5:
				rbs[a].setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.free, 0);
				break;
			}
			r1.addView(rbs[a], new RadioGroup.LayoutParams(-1, -2, 0));
			if (TaxiMain.phones.get(i).contains(cur_phone)) {
				r1.check(rbs[a].getId());
				checked = rbs[a].getId();
			}
		}
		r1.setOnCheckedChangeListener(this);
		context = this;
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						new AlertDialog.Builder(TaxiSelectNumber.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												TaxiSelectNumber.this, 0),
										TaxiSelectNumber.this).show();
					}
				});
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						finish();
					}
				});
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							TaxiSelectNumber.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			startActivity(new Intent(this, Settings.class));
		}
	}

	@Override
	public void onClick(View arg0) {
		new_phone = ((RadioButton) findViewById(checked)).getText().toString();
		Saving s = new Saving();
		s.execute();
	}

	void set() {
		TaxiAdapter.t2.setText(new_phone);
		mProgressDialog.dismiss();
		finish();
	}

	@Override
	public void onCheckedChanged(RadioGroup arg0, int arg1) {
		checked = arg1;
	}

	class Saving extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage("���������� ���������...");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			set();
		}

		@Override
		protected Void doInBackground(Void... params) {
			if (!TaxiAdapter.t2.toString().contains(new_phone)) {
				dbHelper = new DBHelper(context);
				db = dbHelper.getWritableDatabase();
				ContentValues cv = new ContentValues();
				cv.put("name", name);
				cv.put("tel", new_phone);
				TaxiMain.phone.remove(taxi_id);
				TaxiMain.phone.add(taxi_id, new_phone);
				int f = TaxiMain.phones.indexOf(cur_phone);
				int f1 = TaxiMain.phones.indexOf(new_phone);
				TaxiMain.isMain.remove(f1);
				TaxiMain.isMain.add(f1, 1);
				TaxiMain.isMain.remove(f);
				TaxiMain.isMain.add(f, 0);
				db.update("taxi_name", cv, "tel = " + cur_phone, null);
				cv = new ContentValues();
				cv.put("nomer_tel", new_phone);
				cv.put("taxi_id", taxi_id);
				cv.put("main", 1);
				cv.put("operator", TaxiMain.operator.get(f1));
				db.update("taxi_tel", cv, "nomer_tel = " + new_phone, null);
				cv = new ContentValues();
				cv.put("nomer_tel", cur_phone);
				cv.put("taxi_id", taxi_id);
				cv.put("main", 0);
				cv.put("operator", TaxiMain.operator.get(f));
				db.update("taxi_tel", cv, "nomer_tel = " + cur_phone, null);
				db.close();
				dbHelper.close();
			}
			return null;
		}

	}
}
