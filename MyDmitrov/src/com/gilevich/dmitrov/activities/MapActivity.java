package com.gilevich.dmitrov.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.gilevich.dmitrov.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
	double[] coordinates;
	String[] data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.map_activity);
		coordinates = getIntent().getDoubleArrayExtra("coordinates");
		data = getIntent().getStringArrayExtra("data");
		if (data != null && coordinates != null) {
			SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map);
			mapFragment.getMapAsync(this);
		}
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						
						finish();
					}
				});
	}

	@Override
	public void onMapReady(GoogleMap map) {
		LatLng building = new LatLng(coordinates[0], coordinates[1]);

		map.setMyLocationEnabled(false);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(building, 16));

		map.addMarker(new MarkerOptions().title(data[0]).snippet(data[1])
				.position(building));
	}

}
