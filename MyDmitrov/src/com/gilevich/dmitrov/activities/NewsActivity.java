package com.gilevich.dmitrov.activities;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.NewsAdapter;
import com.gilevich.dmitrov.background.NewsGetter;
import com.gilevich.dmitrov.beans.NewsBean;

public class NewsActivity extends Activity implements OnClickListener,
		OnItemClickListener {
	private static ListView newsList;
	private static NewsAdapter adapter;
	public static List<NewsBean> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_activity);
		newsList = (ListView) findViewById(R.id.listView1);
		newsList.setOnItemClickListener(this);
		new NewsGetter(this).execute();
		((ImageButton) findViewById(R.id.button_logo)).setOnClickListener(this);
	}

	public static void setAdapter(Context context) {
		if (adapter == null) {
			adapter = new NewsAdapter(context);
			newsList.setAdapter(adapter);
		} else {
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		adapter = null;
		list = null;
		newsList = null;
	}

	@Override
	public void onClick(View v) {
		finish();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Uri webpage = Uri.parse(list.get(position).getUrl());
		Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
		startActivity(intent);
	}
}