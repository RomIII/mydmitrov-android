package com.gilevich.dmitrov.activities;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainAdapter;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.AutoBaseUpdateService;
import com.gilevich.dmitrov.background.GetRestorauntsList;
import com.gilevich.dmitrov.background.NewsAvailability;
import com.gilevich.dmitrov.background.Updating;
import com.gilevich.dmitrov.utils.DateUtils;

public class MainActivity extends Activity implements OnItemClickListener,
		OnClickListener, android.content.DialogInterface.OnClickListener {
	ProgressDialog mProgressDialog;
	TextView t2;

	final static String doenstLoaded = "������ �� ���������. ��� �������� ������ ������� ������ � ������� ������ ����";
	final int VERSION = 36;
	/**
	 * Indicates the work type of app. Value 0 indicates about off-line work
	 * type. Value 1 indicates about on-line work type.
	 * */
	public static int work_type = -1;
	boolean isBaseUpd;
	SharedPreferences sp;
	String lu;
	BroadcastReceiver br;
	public static Advertisment ads;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_activity);
		t2 = (TextView) findViewById(R.id.textView2);
		sp = PreferenceManager.getDefaultSharedPreferences(this);
		if (sp.getInt("version", 0) < VERSION) {
			sp.edit().remove("work_type").remove("LastUpdate").commit();
			work_type = 0;
			lu = doenstLoaded;
			LayoutInflater factory = LayoutInflater.from(this);
			View content = factory.inflate(R.layout.welcome_alert, null, false);

			final ListView lv = (ListView) content.findViewById(R.id.list);
			lv.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_single_choice,
					getResources().getStringArray(R.array.work_types)));
			lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			final AlertDialog ad = new AlertDialog.Builder(this)
					.setTitle(R.string.choose_work_type)
					.setView(content)
					.setCancelable(false)
					.setPositiveButton(R.string.choose,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									int message_version = sp.getInt(
											"message_id", 0);
									work_type = lv.getCheckedItemPosition() == 0 ? 1
											: 0;
									sp.edit()
											.clear()
											.putInt("version", VERSION)
											.putInt("message_id",
													message_version)
											.putInt("work_type", work_type)
											.commit();
									try {
										FileUtils.deleteDirectory(new File(
												Updating.folder_path));
									} catch (IOException e) {
									}
									if (work_type == 0) {
										if (lu.equals(doenstLoaded))
											makeAlert(true);
										else
											new GetRestorauntsList(
													MainActivity.this, 3)
													.execute(false);
									} else {
										setAdapter();
									}
									t2.setText(doenstLoaded);
								}
							}).show();
			ad.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
			lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					ad.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
				}
			});
		} else {
			work_type = sp.getInt("work_type", 0);
			lu = sp.getString("LastUpdate", doenstLoaded);
			if (lu.equals(doenstLoaded)) {
				if (work_type == 0)
					makeAlert(true);
				t2.setText(doenstLoaded);
			} else {
				if (work_type == 0)
					new GetRestorauntsList(MainActivity.this, 3).execute(false);
				t2.setText("������ ��������� " + DateUtils.getDateText(lu));
			}
		}
		setAdapter();
		((ListView) findViewById(R.id.listView1)).setOnItemClickListener(this);
		((ImageButton) findViewById(R.id.button_refresh))
				.setOnClickListener(this);
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(this);
		((ImageView) findViewById(R.id.ads_image)).setOnClickListener(this);
		isBaseUpd = sp.getBoolean("AutoUpdate", false);
		ads = new Advertisment(getApplicationContext());
		registerReciever();
	}

	private void setAdapter() {
		MainAdapter ma = null;
		if (work_type == 0) {
			String newsUpdate = sp.getString("news_update",
					"0000-00-00 00:00:00");
			ma = new MainAdapter(this,
					!newsUpdate.equals("0000-00-00 00:00:00"));
			newsUpdate = null;
		} else {
			boolean showNews = false;
			try {
				showNews = new NewsAvailability().execute().get(2,
						TimeUnit.SECONDS);
			} catch (Exception e) {
			}
			ma = new MainAdapter(this, showNews);
		}
		((ListView) findViewById(R.id.listView1)).setAdapter(ma);
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							MainActivity.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(MainActivity.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												MainActivity.this,
												MainActivity.work_type == 0 ? 1
														: 0), MainActivity.this)
								.show();
					}
				});
	}

	@Override
	protected void onStart() {
		super.onStart();
		work_type = sp.getInt("work_type", -1);
		lu = sp.getString("LastUpdate", doenstLoaded);
		setSettingButtonClickListener();
		if (work_type == 0)
			isMyServiceRunning();
		t2.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		if (work_type == 0) {
			if (!lu.equals(doenstLoaded)) {
				DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
						Locale.ENGLISH);
				Date date = new Date();
				lu = dateFormat.format(date);
				t2.setText("������ ��������� " + DateUtils.getDateText(lu));
			} else
				t2.setText(doenstLoaded);
		}
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE
						: View.INVISIBLE);
		setAdapter();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 1), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	void makeAlert(boolean showTitle) {
		AlertDialog.Builder alert = new AlertDialog.Builder(this).setMessage(
				"��� ������ � ���������� ���������� ��������� ������!")
				.setPositiveButton("��������� ������!",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								new FullUpdate().execute();
							}
						});
		if (showTitle)
			alert.setTitle("����� ����������!");
		alert.show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (work_type == 0 && lu.equals(doenstLoaded)) {
			makeAlert(false);
		} else {
			switch (arg2) {
			case 0:
				startActivity(new Intent(this, AfishaPlacesChoose.class));
				break;
			case 1:
				startActivity(new Intent(this, TaxiMain.class));
				break;
			case 2:
				startActivity(new Intent(this, DeliveryMain.class));
				break;
			case 3:
				startActivity(new Intent(this, ServicesGroups.class));
				break;
			case 4:
				startActivity(new Intent(this, RestorauntsList.class));
				break;
			case 5:
				if (work_type == 0) {
					new AlertDialog.Builder(this)
							.setMessage(
									"������ ������ ������� ����������� � ���������. ���������� ��������?")
							.setPositiveButton("��",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											startActivity(new Intent(
													MainActivity.this,
													NewsActivity.class));
										}
									}).setNegativeButton("���", null).show();
				} else {
					startActivity(new Intent(MainActivity.this,
							NewsActivity.class));
				}
				break;
			}
		}
	}

	class FullUpdate extends AsyncTask<Void, Integer, Boolean> {
		private String[] modules_names;

		public FullUpdate() {
			modules_names = getResources().getStringArray(R.array.modules);
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog = new ProgressDialog(MainActivity.this);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			publishProgress(0);
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			Updating u = new Updating(MainActivity.this);
			publishProgress(0);
			int places = u.updatePlaces();
			int events = u.updateEvents();
			publishProgress(1);
			int taxi = u.updateTaxi();
			publishProgress(2);
			int delivery = u.updateDelivery();
			publishProgress(3);
			int services_groups = u.updateServicesGroups();
			int services = u.updateServices();
			publishProgress(4);
			int restoraunts = u.updateRestoraunts();
			publishProgress(5);
			// int sights = u.updateSights();
			if (places < 2 && events < 2 && taxi < 2 && delivery < 2
					&& services_groups < 2 && services < 2 && restoraunts < 2) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		protected void onProgressUpdate(Integer... values) {

			super.onProgressUpdate(values);
			mProgressDialog
					.setMessage("�������� ������ ������ \""
							+ modules_names[values[0]]
							+ "\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			if (result) {
				if (MainActivity.work_type == 0) {
					SharedPreferences sp = PreferenceManager
							.getDefaultSharedPreferences(MainActivity.this);
					Editor ed = sp.edit();
					DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
							Locale.ENGLISH);
					Date date = new Date();
					ed.putString("LastUpdate", dateFormat.format(date));
					ed.commit();
					lu = dateFormat.format(date);
					t2.setText("������ ��������� " + DateUtils.getDateText(lu));
				}
				new AlertDialog.Builder(MainActivity.this)
						.setMessage("�������� ������ ��������� �������.")
						.setPositiveButton("��", null).show();
			} else {
				new AlertDialog.Builder(MainActivity.this)
						.setTitle("���������� ��������� ������.")
						.setMessage("����������� ���������� � ����������.")
						.setPositiveButton("��", null).show();
			}
			new GetRestorauntsList(MainActivity.this, 2).execute(false);
			modules_names = null;
		}
	}

	@Override
	public void onClick(View arg0) {

		switch (arg0.getId()) {
		case R.id.button_refresh:
			new FullUpdate().execute();
			break;
		case R.id.button_setting:
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 1), this).show();
		}
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {

		switch (arg1) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			if (MainActivity.work_type == 1)
				startActivity(new Intent(this, Settings.class));
			else {
				FullUpdate fu = new FullUpdate();
				fu.execute();
			}
			break;
		case 2:
			startActivity(new Intent(this, Settings.class));
		}
	}

	void isMyServiceRunning() {
		boolean isBaseUpdOn = false;
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (isBaseUpd
					&& AutoBaseUpdateService.class.getName().equals(
							service.service.getClassName())) {
				isBaseUpdOn = true;
			}
		}
		if (isBaseUpd && !isBaseUpdOn) {
			startService(new Intent(this, AutoBaseUpdateService.class));
		}
	}

}
