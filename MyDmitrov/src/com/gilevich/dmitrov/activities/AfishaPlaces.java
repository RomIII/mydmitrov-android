package com.gilevich.dmitrov.activities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.AfishaPlacesAdapter;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.Updating;
import com.gilevich.dmitrov.utils.DateUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class AfishaPlaces extends Activity implements OnClickListener,
		OnChildClickListener, android.content.DialogInterface.OnClickListener {
	ProgressDialog mProgressDialog;
	DBHelper dbHelper;
	SQLiteDatabase db;
	ContentValues cv;
	public static ArrayList<String> event_names, places_names,
			event_dates_from, event_dates_to, event_times, event_info,
			event_pic, event_type, event_price, place_description,
			place_address, place_tel, place_site;
	public static ArrayList<Integer> event_ids, places_id, event_places_id;
	ExpandableListView l1;
	public static Map<String, List<Integer>> collection;
	TextView t2;
	final String doenstLoaded = "������ �����������. ��� �������� ������ ������� ������ � ������� ������ ����";
	int isUpd;
	String date;
	boolean isAll = true;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.afisha_places);
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		t2 = (TextView) findViewById(R.id.textView2);
		String lu = sp.getString("LastUpdate", doenstLoaded);
		if (lu.equals(doenstLoaded))
			t2.setText(lu);
		else
			t2.setText("������ ��������� " + DateUtils.getDateText(lu));
		if (!getIntent().getBooleanExtra("isAll", false)) {
			date = getIntent().getStringExtra("Date");
			((TextView) findViewById(R.id.textView3)).setText("����� �� "
					+ date);
			isAll = false;
		} else {
			((TextView) findViewById(R.id.textView3)).setHeight(0);
		}
		new LoadAndParse().execute(isAll);
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						finish();
					}
				});
		setSettingButtonClickListener();
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							AfishaPlaces.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(AfishaPlaces.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												AfishaPlaces.this, 0),
										AfishaPlaces.this).show();
					}
				});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 1), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {

		switch (which) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			startActivity(new Intent(this, Settings.class));
			break;
		}
	}

	public int GetPixelFromDips(float pixels) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}

	class LoadAndParse extends AsyncTask<Boolean, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(AfishaPlaces.this);
			mProgressDialog.setMessage("�������� ������...");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

		void getCurrentDateEvents() {
			int i = 0;
			while (i < event_names.size()) {
				Date f = null, t = null, c = null;
				try {
					f = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
							.parse(event_dates_from.get(i));
					t = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
							.parse(event_dates_to.get(i));
					c = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
							.parse(date);
				} catch (ParseException e) {

					e.printStackTrace();
				}
				if (c.compareTo(f) < 0 || c.compareTo(t) > 0) {
					event_names.remove(i);
					event_dates_from.remove(i);
					event_places_id.remove(i);
					event_dates_to.remove(i);
					event_times.remove(i);
					if (MainActivity.work_type == 0)
						event_info.remove(i);
					event_pic.remove(i);
					event_type.remove(i);
					event_price.remove(i);
					i = -1;
				}
				i++;
			}
		}

		@Override
		protected void onPostExecute(Void result) {

			super.onPostExecute(result);
			mProgressDialog.dismiss();
			l1 = (ExpandableListView) findViewById(R.id.listView1);
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			int width = metrics.widthPixels;
			l1.setIndicatorBounds(width - GetPixelFromDips(50), width
					- GetPixelFromDips(10));
			AfishaPlacesAdapter aa = new AfishaPlacesAdapter(
					(Activity) AfishaPlaces.this, places_names, collection);
			l1.setAdapter(aa);
			l1.setOnChildClickListener(AfishaPlaces.this);
			for (int j = 0; j < places_id.size(); j++) {
				l1.expandGroup(j);
			}
			SharedPreferences sp = PreferenceManager
					.getDefaultSharedPreferences(AfishaPlaces.this);
			if (isUpd == 1) {
				if (MainActivity.work_type == 0) {
					Editor ed = sp.edit();
					DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
							Locale.ENGLISH);
					Date date = new Date();
					ed.putString("LastUpdate", dateFormat.format(date));
					ed.commit();
					t2.setText("������ ��������� "
							+ DateUtils.getDateText(dateFormat.format(date)));
				}
			} else {
				if (MainActivity.work_type == 0)
					t2.setText("������ ��������� "
							+ DateUtils.getDateText(sp.getString("LastUpdate",
									"")));
			}
			isUpd = 0;
		}

		@Override
		protected Void doInBackground(Boolean... params) {

			event_names = new ArrayList<String>();
			event_times = new ArrayList<String>();
			event_info = new ArrayList<String>();
			event_pic = new ArrayList<String>();
			event_type = new ArrayList<String>();
			event_dates_from = new ArrayList<String>();
			event_dates_to = new ArrayList<String>();
			event_places_id = new ArrayList<Integer>();
			event_price = new ArrayList<String>();

			places_id = new ArrayList<Integer>();
			places_id.addAll(AfishaPlacesChoose.places_id);
			places_names = new ArrayList<String>();
			places_names.addAll(AfishaPlacesChoose.places_names);
			place_description = new ArrayList<String>();
			place_description.addAll(AfishaPlacesChoose.place_description);
			place_address = new ArrayList<String>();
			place_address.addAll(AfishaPlacesChoose.place_address);
			place_tel = new ArrayList<String>();
			place_tel.addAll(AfishaPlacesChoose.place_tel);
			place_site = new ArrayList<String>();
			place_site.addAll(AfishaPlacesChoose.place_site);
			dbHelper = new DBHelper(AfishaPlaces.this);
			db = dbHelper.getWritableDatabase();
			if (MainActivity.work_type == 0) {
				Cursor c = db.query("afisha_events", null, null, null, null,
						null, null);
				c.moveToFirst();
				int event_nameIndex = c.getColumnIndex("event_name");
				int place_idIndex = c.getColumnIndex("place_id");
				int date_fromIndex = c.getColumnIndex("date_from");
				int date_toIndex = c.getColumnIndex("date_to");
				int timeIndex = c.getColumnIndex("event_time");
				int infoIndex = c.getColumnIndex("event_info");
				int picIndex = c.getColumnIndex("event_pic");
				int typeIndex = c.getColumnIndex("event_type");
				int priceIndex = c.getColumnIndex("event_price");
				do {
					event_names.add(c.getString(event_nameIndex));
					event_places_id.add(c.getInt(place_idIndex));
					event_dates_from.add(c.getString(date_fromIndex));
					event_dates_to.add(c.getString(date_toIndex));
					event_times.add(c.getString(timeIndex));
					event_info.add(c.getString(infoIndex));
					event_pic.add(c.getString(picIndex));
					event_type.add(c.getString(typeIndex));
					event_price.add(c.getString(priceIndex));
				} while (c.moveToNext());
				c.close();
				dbHelper.close();
				db.close();
			} else {
				Updating u = new Updating(AfishaPlaces.this);
				int upd = u.updateEvents();
				if (upd == 0) {
					event_ids = u.event_id;
					event_names = u.event_names;
					event_times = u.event_times;
					event_info = u.event_info;
					event_pic = u.event_pic;
					event_type = u.event_type;
					event_dates_from = u.event_dates_from;
					event_dates_to = u.event_dates_to;
					event_places_id = u.event_places_id;
					event_price = u.event_price;
				}
				u = null;
			}
			if (!params[0])
				getCurrentDateEvents();
			int i = 0;
			while (i < places_names.size()) {
				if (event_places_id.indexOf(places_id.get(i)) == -1) {
					place_address.remove(i);
					place_description.remove(i);
					place_site.remove(i);
					place_tel.remove(i);
					places_id.remove(i);
					places_names.remove(i);
					i = -1;
				}
				i++;
			}
			collection = new LinkedHashMap<String, List<Integer>>();
			for (i = 0; i < places_id.size(); i++) {
				ArrayList<Integer> en = new ArrayList<Integer>();
				for (int j = 0; j < event_names.size(); j++) {
					if (event_places_id.get(j) == places_id.get(i)) {
						en.add(j);
					}
				}
				collection.put(places_names.get(i), en);
			}
			return null;
		}
	}

	@Override
	protected void onStart() {

		super.onStart();
		t2.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		if (MainActivity.work_type == 0) {
			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
					Locale.ENGLISH);
			Date date = new Date();
			t2.setText("������ ��������� "
					+ DateUtils.getDateText(dateFormat.format(date)));
		}
	}

	@Override
	public void onClick(View arg0) {

		new LoadAndParse().execute(isAll);
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
		if (dbHelper != null) {
			dbHelper.close();
		}
		if (db != null) {
			db.close();
		}
		unregisterReceiver(br);
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {

		startActivity(new Intent(this, AfishaEventInfo.class).putExtra(
				"ID",
				collection.get(places_names.get(groupPosition)).get(
						childPosition)));
		return true;
	}
}
