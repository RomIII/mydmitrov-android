package com.gilevich.dmitrov.activities;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.AutoBaseUpdateService;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;

public class Settings extends Activity implements OnClickListener,
		OnCheckedChangeListener,
		android.content.DialogInterface.OnClickListener, OnTimeSetListener,
		android.widget.RadioGroup.OnCheckedChangeListener {
	CheckBox c1, c2;
	Button b1;
	boolean auto_update;
	final int[] workType = { R.id.radio1, R.id.radio0 };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings);
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		((RadioGroup) findViewById(R.id.radioGroup1)).check(workType[sp.getInt(
				"work_type", 0)]);
		((RadioGroup) findViewById(R.id.radioGroup1))
				.setOnCheckedChangeListener(this);
		auto_update = sp.getBoolean("AutoUpdate", false);
		c1 = (CheckBox) findViewById(R.id.checkBox1);
		c1.setChecked(auto_update);
		c1.setOnCheckedChangeListener(this);
		b1 = (Button) findViewById(R.id.button1);
		b1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				new TimePickerDialog(Settings.this, Settings.this, Integer
						.valueOf(b1.getText().toString().substring(0, 2)),
						Integer.valueOf(b1.getText().toString().substring(3)),
						true).show();
			}
		});
		if (c1.isChecked()) {
			b1.setText(String.valueOf(sp.getInt("Hours", 0)) + ":"
					+ String.valueOf(sp.getInt("Minutes", 0)));
			if (b1.getText().toString().indexOf(":") != 2) {
				b1.setText("0" + b1.getText());
			}
			if (b1.getText().length() != 5) {
				b1.setText(b1.getText().toString().substring(0, 3) + "0"
						+ b1.getText().toString().substring(3));
			}
		} else {
			b1.setEnabled(false);
		}
		c2 = (CheckBox) findViewById(R.id.checkBox2);
		c2.setChecked(sp.getBoolean("LoadImages", true));
		((Button) findViewById(R.id.button2)).setOnClickListener(this);
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						new AlertDialog.Builder(Settings.this)
								.setTitle("Настройки")
								.setAdapter(
										new MainSettingsAdapter(Settings.this,
												-1), Settings.this).show();
					}
				});
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						finish();
					}
				});
		onCheckedChanged(((RadioGroup) findViewById(R.id.radioGroup1)),
				workType[sp.getInt("work_type", 0)]);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("Настройки")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		
		switch (arg1) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			startActivity(new Intent(this, Settings.class));
		}
	}

	@Override
	public void onClick(View arg0) {
		
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor ed = sp.edit();
		int checked_id = ((RadioGroup) findViewById(R.id.radioGroup1))
				.getCheckedRadioButtonId(), type = checked_id == R.id.radio0 ? 1
				: 0;
		ed.putInt("work_type", type);
		MainActivity.work_type = type;
		if (type == 0 && c1.isChecked() && c1.isEnabled()) {
			ed.putInt("Hours",
					Integer.valueOf(b1.getText().toString().substring(0, 2)));
			ed.putInt("Minutes",
					Integer.valueOf(b1.getText().toString().substring(3)));
			ed.putBoolean("AutoUpdate", true);
			startService(new Intent(this, AutoBaseUpdateService.class));
		} else {
			ed.putBoolean("AutoUpdate", false);
			ed.putInt("Hours", 0);
			ed.putInt("Minutes", 0);
			stopService(new Intent(this, AutoBaseUpdateService.class));
		}
		ed.putBoolean("LoadImages", (c2.isChecked()));
		if (type == 1)
			ed.remove("LastUpdate");
		ed.commit();
		finish();
	}

	@Override
	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		
		b1.setEnabled(arg1);
		auto_update = arg1;
	}

	@Override
	public void onTimeSet(TimePicker arg0, int arg1, int arg2) {
		
		String m, h;
		m = String.valueOf(arg1);
		if (m.length() == 1) {
			m = "0" + m;
		}
		h = String.valueOf(arg2);
		if (h.length() == 1) {
			h = "0" + h;
		}
		b1.setText(m + ":" + h);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		
		// c1.setChecked(auto_update && checkedId != R.id.radio1);
		if (checkedId == R.id.radio1)
			c1.setChecked(auto_update);
		c1.setEnabled(checkedId == R.id.radio1);
	}
}
