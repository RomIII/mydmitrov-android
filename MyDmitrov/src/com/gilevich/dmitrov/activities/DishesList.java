package com.gilevich.dmitrov.activities;

import java.util.ArrayList;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.DishesListAdapter;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.Cart;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.Updating;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class DishesList extends Activity implements OnItemClickListener,
		OnClickListener, android.content.DialogInterface.OnClickListener {
	public static ArrayList<String> names, descriptions, portions, pictures;
	public static ArrayList<Integer> ids, prices;
	public static int restorauntID, catalogID;
	public static Button cartButton;
	DishesListAdapter adapter = null;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.restoraunts_list);
		((TextView) findViewById(R.id.textView2))
				.setText(RestorauntSectionsList.names
						.get(RestorauntSectionsList.ids.indexOf(getIntent()
								.getIntExtra("CatalogID", 0))));
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(View.GONE);
		restorauntID = getIntent().getIntExtra("RestorauntID", 0);
		catalogID = getIntent().getIntExtra("CatalogID", 0);
		ids = new ArrayList<>();
		names = new ArrayList<>();
		descriptions = new ArrayList<>();
		prices = new ArrayList<>();
		portions = new ArrayList<>();
		pictures = new ArrayList<>();
		((TextView) findViewById(R.id.textView3)).setOnClickListener(this);
		((TextView) findViewById(R.id.textView3))
				.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.logo_cutted, 0, 0, 0);
		String logo_path = RestorauntsList.logo.get(RestorauntsList.id
				.indexOf(restorauntID));
		if (logo_path != null) {
			((ImageView) findViewById(R.id.imageView1))
					.setImageDrawable(Drawable.createFromPath(logo_path));
			((ImageView) findViewById(R.id.imageView1))
					.setOnClickListener(this);
		}
		((TextView) findViewById(R.id.textView4)).setVisibility(View.VISIBLE);
		((TextView) findViewById(R.id.textView4)).setText(RestorauntsList.names
				.get(RestorauntsList.id.indexOf(restorauntID)));
		((ListView) findViewById(R.id.listView1))
				.setOnItemClickListener(DishesList.this);
		cartButton = (Button) findViewById(R.id.button1);
		cartButton.setOnClickListener(this);
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						new AlertDialog.Builder(DishesList.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												DishesList.this, 0),
										DishesList.this).show();
					}
				});
		new GetDishesList().execute(catalogID, restorauntID);
		((ListView) findViewById(R.id.listView1))
				.setOnItemClickListener(DishesList.this);
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							DishesList.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	@Override
	protected void onStart() {
		
		super.onStart();
		if (Cart.cartCreated)
			setCartButtonText();
		if (adapter != null)
			adapter.notifyDataSetChanged();
	}

	public static void setCartButtonText() {
		int cartAmount = Cart.getCartAmount();
		if (cartAmount > 0) {
			cartButton.setText("������� " + cartAmount + " ���.");
		} else {
			cartButton.setText("�������");
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {
		
		switch (arg1) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			startActivity(new Intent(this, Settings.class));
		}
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.button1:
			startActivity(new Intent(this, CartActivity.class));
			break;
		case R.id.textView3:
		case R.id.imageView1:
			finish();
			break;
		}
	}

	class GetDishesList extends AsyncTask<Integer, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Integer... params) {
			
			DBHelper dbh = new DBHelper(DishesList.this);
			SQLiteDatabase db = dbh.getReadableDatabase();
			Cursor c = db
					.query("dishes", null, "catalog_id=" + params[0]
							+ " AND restoraunt_id=" + params[1], null, null,
							null, null);

			if (MainActivity.work_type == 0 && c.moveToFirst()) {
				clearArrayLists();
				int idIndex = c.getColumnIndex("id"), nameIndex = c
						.getColumnIndex("name"), descriptionIndex = c
						.getColumnIndex("description"), pricesIndex = c
						.getColumnIndex("price"), portionsId = c
						.getColumnIndex("portion"), picturesId = c
						.getColumnIndex("picture");
				do {
					ids.add(c.getInt(idIndex));
					names.add(c.getString(nameIndex));
					descriptions.add(c.getString(descriptionIndex));
					prices.add(c.getInt(pricesIndex));
					portions.add(c.getString(portionsId));
					pictures.add(c.getString(picturesId));
				} while (c.moveToNext());
				c = null;
				db.close();
				dbh.close();
			} else {
				if (MainActivity.work_type == 1) {
					clearArrayLists();
					Updating u = new Updating(DishesList.this);
					ArrayList<ArrayList<String>> upd = u.getRestorauntDishes(
							restorauntID, catalogID);
					if (upd == null) {
						u = null;
						return false;
					}
					ArrayList<String> ids = new ArrayList<>();
					ids.addAll(upd.get(0));
					ArrayList<String> prices = new ArrayList<>();
					prices.addAll(upd.get(2));
					for (int i = 0; i < ids.size(); i++) {
						DishesList.ids.add(Integer.parseInt(ids.get(i)));
						DishesList.prices.add(Integer.parseInt(prices.get(i)));
					}
					names.addAll(upd.get(1));
					descriptions.addAll(upd.get(3));
					pictures.addAll(upd.get(4));
					portions.addAll(upd.get(5));
					prices = null;
					ids = null;
					u = null;
				} else
					return false;
			}
			return true;
		}

		void clearArrayLists() {
			ids.clear();
			names.clear();
			descriptions.clear();
			prices.clear();
			portions.clear();
			pictures.clear();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			
			super.onPostExecute(result);
			if (result) {
				if (adapter == null) {
					adapter = new DishesListAdapter(DishesList.this);
					((ListView) findViewById(R.id.listView1))
							.setAdapter(adapter);
					// int p = RestorauntsList.id.indexOf(restorauntID);
					// if (RestorauntsList.updated.get(p).equals(
					// RestorauntsList.user_update.get(p))) {
					// Button b = (Button) ((LayoutInflater)
					// getSystemService(LAYOUT_INFLATER_SERVICE))
					// .inflate(R.layout.restoraunt_item, null)
					// .findViewById(R.id.button1);
					// b.setText("�������� ����");
					// b.setId(R.id.button2);
					// b.setOnClickListener(DishesList.this);
					// ((ListView) findViewById(R.id.listView1))
					// .addHeaderView(b);
					// }
				} else
					adapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		int tag = (int) arg1.getTag();
		((TextView) arg1.findViewById(R.id.textView1))
				.setMaxLines(99 * tag + 1);
		((TextView) arg1.findViewById(R.id.textView2))
				.setMaxLines(99 * tag + 1);
		if (tag == 1) {
			tag = 0;
		} else {
			tag = 1;
		}
		arg1.setTag(tag);
	}
}
