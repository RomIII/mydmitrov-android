package com.gilevich.dmitrov.activities;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.adapters.RestorauntSectionsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.Cart;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.GetRestorauntMenu;
import com.gilevich.dmitrov.background.Updating;

public class RestorauntSectionsList extends Activity implements
		OnItemClickListener, OnClickListener,
		android.content.DialogInterface.OnClickListener {
	public static ArrayList<String> names;
	public static ArrayList<Integer> ids;
	int restorauntID;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.restoraunts_list);
		restorauntID = getIntent().getIntExtra("RestorauntID", 0);
		names = new ArrayList<>();
		ids = new ArrayList<>();
		((TextView) findViewById(R.id.textView3)).setOnClickListener(this);
		((TextView) findViewById(R.id.textView3))
				.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.logo_cutted, 0, 0, 0);
		String logo_path = RestorauntsList.logo.get(RestorauntsList.id
				.indexOf(restorauntID));
		if (logo_path != null) {
			((ImageView) findViewById(R.id.imageView1))
					.setImageDrawable(Drawable.createFromPath(logo_path));
			((ImageView) findViewById(R.id.imageView1))
					.setOnClickListener(this);
		}
		((TextView) findViewById(R.id.textView4)).setVisibility(View.VISIBLE);
		((TextView) findViewById(R.id.textView4)).setText(RestorauntsList.names
				.get(RestorauntsList.id.indexOf(restorauntID)));
		((TextView) findViewById(R.id.textView2)).setVisibility(View.GONE);
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(View.GONE);
		((Button) findViewById(R.id.button1)).setOnClickListener(this);
		((ListView) findViewById(R.id.listView1))
				.setOnItemClickListener(RestorauntSectionsList.this);
		new GetRestorauntSections().execute(restorauntID);
		setSettingButtonClickListener();
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							RestorauntSectionsList.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(RestorauntSectionsList.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												RestorauntSectionsList.this, 0),
										RestorauntSectionsList.this).show();
					}
				});
	}

	@Override
	protected void onStart() {

		super.onStart();
		if (Cart.cartCreated)
			setCartButtonText();
	}

	void setCartButtonText() {
		int cartAmount = Cart.getCartAmount();
		if (cartAmount > 0) {
			((Button) findViewById(R.id.button1)).setText("������� "
					+ cartAmount + " ���.");
		} else {
			((Button) findViewById(R.id.button1)).setText("�������");
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {

		switch (which) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			startActivity(new Intent(this, Settings.class));
		}
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.button1:
			startActivity(new Intent(this, CartActivity.class));
			break;
		case R.id.button2:
			try {
				new GetRestorauntMenu(this).execute(restorauntID).get();
				((ListView) findViewById(R.id.listView1))
						.removeHeaderView(arg0);
			} catch (InterruptedException | ExecutionException e) {

				e.printStackTrace();
			}
			break;
		case R.id.textView3:
		case R.id.imageView1:
			finish();
			break;
		}
	}

	class GetRestorauntSections extends AsyncTask<Integer, Void, Boolean> {
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			((ListView) findViewById(R.id.listView1)).removeAllViewsInLayout();
			if (result) {
				// int p = RestorauntsList.id.indexOf(restorauntID);
				// if (RestorauntsList.updated.get(p).equals(
				// RestorauntsList.user_update.get(p))) {
				// Button b = (Button) ((LayoutInflater)
				// getSystemService(LAYOUT_INFLATER_SERVICE))
				// .inflate(R.layout.restoraunt_item, null)
				// .findViewById(R.id.button1);
				// b.setText("�������� ����");
				// b.setId(R.id.button2);
				// b.setOnClickListener(RestorauntSectionsList.this);
				// ((ListView) findViewById(R.id.listView1)).addHeaderView(b);
				// }
				((ListView) findViewById(R.id.listView1))
						.setAdapter(new RestorauntSectionsAdapter(
								RestorauntSectionsList.this));
			}
		}

		@Override
		protected Boolean doInBackground(Integer... params) {
			DBHelper dbh = new DBHelper(RestorauntSectionsList.this);
			SQLiteDatabase db = dbh.getReadableDatabase();
			Cursor c = db.query("sections", null, "restoraunt_id=" + params[0],
					null, null, null, null);
			if (MainActivity.work_type == 0 && c.moveToFirst()) {
				ids.clear();
				names.clear();
				int nameIndex = c.getColumnIndex("name"), idIndex = c
						.getColumnIndex("id");
				do {
					ids.add(c.getInt(idIndex));
					names.add(c.getString(nameIndex));
				} while (c.moveToNext());
			} else {
				if (MainActivity.work_type == 1) {
					Updating u = new Updating(RestorauntSectionsList.this);
					ids.clear();
					names.clear();
					ArrayList<ArrayList<String>> upd = u
							.getRestorauntCatalog(restorauntID);
					if (upd == null) {
						u = null;
						return false;
					}
					ArrayList<String> ids = new ArrayList<>();
					ids.addAll(upd.get(0));
					for (int i = 0; i < ids.size(); i++)
						RestorauntSectionsList.ids.add(Integer.parseInt(ids
								.get(i)));
					ids = null;
					names.addAll(upd.get(1));
					u = null;
				} else
					return false;
			}
			return true;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		startActivity(new Intent(this, DishesList.class).putExtra("CatalogID",
				ids.get(arg2)).putExtra("RestorauntID", restorauntID));
	}
}
