package com.gilevich.dmitrov.activities;

import java.util.ArrayList;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.CartAdapter;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.Cart;
import com.gilevich.dmitrov.background.DBHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class CartActivity extends Activity implements OnItemClickListener,
		OnClickListener, android.content.DialogInterface.OnClickListener {
	public static ArrayList<String> names, descriptions, portions, pictures;
	public static ArrayList<Integer> ids, prices;
	public static TextView restorauntName, cartAmount;
	public static ListView l1;
	public static View v;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.restoraunts_list);
		l1 = (ListView) findViewById(R.id.listView1);
		((TextView) findViewById(R.id.textView1)).setText("�������");
		((Button) findViewById(R.id.button1)).setVisibility(View.GONE);
		((TextView) findViewById(R.id.textView2)).setVisibility(View.GONE);
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(View.GONE);
		((Button) findViewById(R.id.button2)).setVisibility(View.VISIBLE);
		((Button) findViewById(R.id.button2)).setOnClickListener(this);
		((TextView) findViewById(R.id.textView3)).setOnClickListener(this);
		ids = new ArrayList<>();
		names = new ArrayList<>();
		descriptions = new ArrayList<>();
		prices = new ArrayList<>();
		portions = new ArrayList<>();
		pictures = new ArrayList<>();
		new GetDishesList().execute(Cart.currentRestoraunt);
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							CartActivity.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
		setSettingButtonClickListener();
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(CartActivity.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												CartActivity.this, 0),
										CartActivity.this).show();
					}
				});
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {

		switch (which) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			startActivity(new Intent(this, Settings.class));
			break;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	public static void changeCartHeader(boolean cartIsEmpty) {
		if (v != null && !cartIsEmpty) {
			v.setVisibility(View.VISIBLE);
			if (Cart.restorauntPhone.length() > 0)
				restorauntName.setText(Cart.restorauntName + ". "
						+ Cart.restorauntPhone);
			else
				restorauntName.setText(Cart.restorauntName);
			cartAmount.setText("��������� ������ " + Cart.getCartAmount()
					+ " ���.");
		} else {
			if (v != null)
				v.setVisibility(View.GONE);
		}
	}

	void clearArrayLists() {
		ids.clear();
		names.clear();
		descriptions.clear();
		prices.clear();
		portions.clear();
		pictures.clear();
	}

	class GetDishesList extends AsyncTask<Integer, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Integer... params) {

			DBHelper dbh = new DBHelper(CartActivity.this);
			SQLiteDatabase db = dbh.getReadableDatabase();
			Cursor c = db.query("dishes", null, "restoraunt_id=" + params[0],
					null, null, null, null);
			if (c.moveToFirst()) {
				clearArrayLists();
				int idIndex = c.getColumnIndex("id"), nameIndex = c
						.getColumnIndex("name"), descriptionIndex = c
						.getColumnIndex("description"), pricesIndex = c
						.getColumnIndex("price"), portionsId = c
						.getColumnIndex("portion"), picturesId = c
						.getColumnIndex("picture");
				do {
					if (Cart.dishes.get(c.getInt(idIndex), -1) >= 0) {
						ids.add(c.getInt(idIndex));
						names.add(c.getString(nameIndex));
						descriptions.add(c.getString(descriptionIndex));
						prices.add(c.getInt(pricesIndex));
						portions.add(c.getString(portionsId));
						pictures.add(c.getString(picturesId));
					}
				} while (c.moveToNext());
				c = null;
				db.close();
				dbh.close();
				return true;
			} else {
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {

			super.onPostExecute(result);
			if (result) {
				CartActivity.v = getLayoutInflater().inflate(
						R.layout.cart_header, l1, false);
				restorauntName = (TextView) v.findViewById(R.id.textView1);
				cartAmount = (TextView) v.findViewById(R.id.textView2);
				CartActivity.changeCartHeader(Cart.dishes.size() == 0);
				l1.addHeaderView(v);
				l1.setAdapter(new CartAdapter(CartActivity.this));
				l1.setOnItemClickListener(CartActivity.this);
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		if (arg2 > 0) {

			int tag = (int) arg1.getTag();
			((TextView) arg1.findViewById(R.id.textView1))
					.setMaxLines(99 * tag + 1);
			((TextView) arg1.findViewById(R.id.textView2))
					.setMaxLines(99 * tag + 1);
			if (tag == 1) {
				tag = 0;
			} else {
				tag = 1;
			}
			arg1.setTag(tag);
		}
	}

	@Override
	public void onClick(View arg0) {

		switch (arg0.getId()) {
		case R.id.textView3:
			finish();
			break;
		case R.id.button2:
			new AlertDialog.Builder(this)
					.setMessage("�� ������������� ������ �������� �������?")
					.setPositiveButton("��",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {

									Cart.clearCart();
									clearArrayLists();
									changeCartHeader(true);
								}
							}).setNegativeButton("���", null).show();
			break;
		}
	}
}
