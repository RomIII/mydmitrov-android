package com.gilevich.dmitrov.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.adapters.ServicesGroupsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.GetRestorauntsList;
import com.gilevich.dmitrov.background.Updating;
import com.gilevich.dmitrov.utils.DateUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ServicesGroups extends Activity implements OnClickListener,
		OnItemClickListener, android.content.DialogInterface.OnClickListener {
	DBHelper dbHelper;
	SQLiteDatabase db;
	public static ArrayList<String> services_groups_names,
			services_groups_text;
	public static ArrayList<Integer> services_groups_id;
	int isUpd;
	ProgressDialog mProgressDialog;
	TextView t2;
	BroadcastReceiver br;
	final String doenstLoaded = "������ �����������. ��� �������� ������ ������� ������ � ������� ������ ����";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.services_groups);
		t2 = (TextView) findViewById(R.id.textView2);
		t2 = (TextView) findViewById(R.id.textView2);
		String lu = PreferenceManager.getDefaultSharedPreferences(this)
				.getString("LastUpdate", MainActivity.doenstLoaded);
		if (lu.equals(MainActivity.doenstLoaded))
			t2.setText(lu);
		else
			t2.setText("������ ��������� " + DateUtils.getDateText(lu));
		((ImageButton) findViewById(R.id.button_refresh))
				.setOnClickListener(this);
		if (MainActivity.work_type == -1)
			MainActivity.work_type = PreferenceManager
					.getDefaultSharedPreferences(this).getInt("work_type", 0);
		new GroupsLoading().execute(false);
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						finish();
					}
				});
		setSettingButtonClickListener();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							ServicesGroups.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(ServicesGroups.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												ServicesGroups.this,
												MainActivity.work_type == 0 ? 1
														: 0),
										ServicesGroups.this).show();
					}
				});
		registerReciever();
	}

	@Override
	protected void onStart() {

		super.onStart();
		t2.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE
						: View.INVISIBLE);
		setSettingButtonClickListener();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 1), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {

		switch (arg1) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			if (MainActivity.work_type == 1)
				startActivity(new Intent(this, Settings.class));
			else {
				new GroupsLoading().execute(true);
			}
			break;
		case 2:
			startActivity(new Intent(this, Settings.class));
		}
	}

	class GroupsLoading extends AsyncTask<Boolean, Integer, Boolean> {
		private String[] modules_names;

		public GroupsLoading() {
			modules_names = getResources().getStringArray(R.array.modules);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			mProgressDialog
					.setMessage("�������� ������ ������ \""
							+ modules_names[values[0]]
							+ "\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(ServicesGroups.this);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			publishProgress(0);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			if (result) {
				((ListView) findViewById(R.id.listView1))
						.setAdapter(new ServicesGroupsAdapter(
								ServicesGroups.this));
				((ListView) findViewById(R.id.listView1))
						.setOnItemClickListener(ServicesGroups.this);
				SharedPreferences sp = PreferenceManager
						.getDefaultSharedPreferences(ServicesGroups.this);
				if (isUpd == 1) {
					if (MainActivity.work_type == 0) {
						Editor ed = sp.edit();
						DateFormat dateFormat = new SimpleDateFormat(
								"dd.MM.yyyy", Locale.ENGLISH);
						Date date = new Date();
						ed.putString("LastUpdate", dateFormat.format(date));
						ed.commit();
						t2.setText("������ ��������� "
								+ DateUtils.getDateText(dateFormat.format(date)));
					}
					new AlertDialog.Builder(ServicesGroups.this)
							.setMessage("�������� ������ ��������� �������.")
							.setPositiveButton("��", null).show();
				} else {
					if (MainActivity.work_type == 0)
						t2.setText("������ ��������� "
								+ DateUtils.getDateText(sp.getString(
										"LastUpdate", "")));
				}
				isUpd = 0;
			} else {
				new AlertDialog.Builder(ServicesGroups.this)
						.setTitle("���������� ��������� ������.")
						.setMessage("����������� ���������� � ����������.")
						.setPositiveButton("��", null).show();

			}
			modules_names = null;
		}

		@SuppressLint("NewApi")
		@Override
		protected Boolean doInBackground(Boolean... params) {

			dbHelper = new DBHelper(ServicesGroups.this);
			db = dbHelper.getWritableDatabase();
			Cursor c = db.query("services_groups", null, null, null, null,
					null, null);
			if (MainActivity.work_type == 0 && c.moveToFirst() && !params[0]) {
				services_groups_names = new ArrayList<String>();
				services_groups_text = new ArrayList<String>();
				services_groups_id = new ArrayList<Integer>();
				int nameIndex = c.getColumnIndex("group_name");
				int idIndex = c.getColumnIndex("group_id");
				int textIndex = c.getColumnIndex("group_description");
				do {
					services_groups_names.add(c.getString(nameIndex));
					services_groups_id.add(c.getInt(idIndex));
					services_groups_text.add(c.getString(textIndex));
				} while (c.moveToNext());
				c.close();
				if (dbHelper != null) {
					dbHelper.close();
				}
				if (db != null) {
					db.close();
				}
			} else {
				if (params[0])
					isUpd = 1;
				c.close();
				if (dbHelper != null) {
					dbHelper.close();
				}
				if (db != null) {
					db.close();
				}
				int upd;
				Updating u = new Updating(ServicesGroups.this);
				if (MainActivity.work_type == 1) {
					publishProgress(3);
					upd = u.updateServicesGroups();
				} else {
					publishProgress(0);
					u.updatePlaces();
					u.updateEvents();
					publishProgress(1);
					u.updateTaxi();
					publishProgress(2);
					u.updateDelivery();
					publishProgress(3);
					upd = u.updateServicesGroups();
					u.updateServices();
					publishProgress(4);
					u.updateRestoraunts();
					publishProgress(5);
					//u.updateSights();
					try {
						GetRestorauntsList get = new GetRestorauntsList(
								ServicesGroups.this, 2);
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
							get.executeOnExecutor(
									AsyncTask.THREAD_POOL_EXECUTOR, false)
									.get();
						else
							get.execute(false).get();
						get = null;
					} catch (InterruptedException | ExecutionException e) {
					}
				}
				if (upd == 0) {
					services_groups_names = u.services_groups_names;
					services_groups_id = u.services_groups_id;
					services_groups_text = u.services_groups_text;
				} else if (upd == 2) {
					return false;
				}
				u = null;
			}
			return true;

		}

	}

	@Override
	public void onClick(View arg0) {

		new GroupsLoading().execute(true);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		startActivity(new Intent(this, ServicesList.class).putExtra("ID",
				services_groups_id.get(arg2)));
	}
}
