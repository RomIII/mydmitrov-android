package com.gilevich.dmitrov.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.AfishaPlacesChooseAdapter;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.GetRestorauntsList;
import com.gilevich.dmitrov.background.Updating;
import com.gilevich.dmitrov.utils.DateUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class AfishaPlacesChoose extends Activity implements
		OnItemClickListener, android.content.DialogInterface.OnClickListener,
		OnDateSetListener, OnClickListener {
	ProgressDialog mProgressDialog;
	public static ArrayList<String> places_names, place_description,
			place_address, place_tel, place_site;
	public static ArrayList<Integer> places_id;
	int day, month, year;
	String date = "";
	ListView l1;
	AfishaPlacesChooseAdapter adapter = null;
	TextView t2;
	ImageButton b1;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.afisha_places_choose);
		place_description = new ArrayList<>();
		place_address = new ArrayList<>();
		place_tel = new ArrayList<>();
		place_site = new ArrayList<>();
		places_names = new ArrayList<>();
		places_id = new ArrayList<>();
		if (MainActivity.work_type == -1)
			MainActivity.work_type = PreferenceManager
					.getDefaultSharedPreferences(this).getInt("work_type", 0);
		PlacesParsing pp = new PlacesParsing();
		pp.execute(false);
		l1 = (ListView) findViewById(R.id.listView1);
		l1.setOnItemClickListener(AfishaPlacesChoose.this);
		t2 = (TextView) findViewById(R.id.textView2);
		b1 = (ImageButton) findViewById(R.id.button_refresh);
		b1.setOnClickListener(this);
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						finish();
					}
				});
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
				Locale.ENGLISH);
		String[] curDate = dateFormat.format(new Date()).split("\\.");
		day = Integer.valueOf(curDate[0]);
		month = Integer.valueOf(curDate[1]);
		year = Integer.valueOf(curDate[2]);
		((Button) findViewById(R.id.button1))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						new DatePickerDialog(AfishaPlacesChoose.this,
								AfishaPlacesChoose.this, year, month - 1, day)
								.show();
					}
				});
		date = day + "." + month + "." + year;
		((Button) findViewById(R.id.button2))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						Intent intent = new Intent(AfishaPlacesChoose.this,
								AfishaPlaces.class);
						intent.putExtra("isAll", true);
						startActivity(intent);
					}
				});
		setSettingButtonClickListener();
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							AfishaPlacesChoose.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(AfishaPlacesChoose.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												AfishaPlacesChoose.this,
												MainActivity.work_type == 0 ? 1
														: 0),
										AfishaPlacesChoose.this).show();
					}
				});
	}

	@Override
	protected void onStart() {

		super.onStart();
		t2.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		String lu = PreferenceManager.getDefaultSharedPreferences(this)
				.getString("LastUpdate", MainActivity.doenstLoaded);
		if (lu.equals(MainActivity.doenstLoaded))
			t2.setText(lu);
		else
			t2.setText("������ ��������� " + DateUtils.getDateText(lu));
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE
						: View.INVISIBLE);
		setSettingButtonClickListener();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {

		switch (which) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			if (MainActivity.work_type == 1)
				startActivity(new Intent(this, Settings.class));
			else {
				new PlacesParsing().execute(true);
			}
			break;
		case 2:
			startActivity(new Intent(this, Settings.class));
		}
	}

	class PlacesParsing extends AsyncTask<Boolean, Integer, Boolean> {
		boolean isUpdating = false;
		private String[] modules_names;

		public PlacesParsing() {
			modules_names = getResources().getStringArray(R.array.modules);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			mProgressDialog
					.setMessage("�������� ������ ������ \""
							+ modules_names[values[0]]
							+ "\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(AfishaPlacesChoose.this);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			publishProgress(0);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			if (result) {
				if (adapter == null) {
					adapter = new AfishaPlacesChooseAdapter(
							AfishaPlacesChoose.this);
					((ListView) findViewById(R.id.listView1))
							.setAdapter(adapter);
				} else
					adapter.notifyDataSetChanged();
				SharedPreferences sp = PreferenceManager
						.getDefaultSharedPreferences(AfishaPlacesChoose.this);
				if (isUpdating) {
					if (MainActivity.work_type == 0) {
						Editor ed = sp.edit();
						DateFormat dateFormat = new SimpleDateFormat(
								"dd.MM.yyyy", Locale.ENGLISH);
						Date date = new Date();
						ed.putString("LastUpdate", dateFormat.format(date));
						ed.commit();
						t2.setText("������ ��������� "
								+ DateUtils.getDateText(dateFormat.format(date)));
					}
					new AlertDialog.Builder(AfishaPlacesChoose.this)
							.setMessage("�������� ������ ��������� �������.")
							.setPositiveButton("��", null).show();
				} else {
					if (MainActivity.work_type == 0)
						t2.setText("������ ��������� "
								+ DateUtils.getDateText((new SimpleDateFormat(
										"dd.MM.yyyy", Locale.ENGLISH)
										.format(new Date()))));
				}
			} else {
				new AlertDialog.Builder(AfishaPlacesChoose.this)
						.setTitle("���������� ��������� ������.")
						.setMessage("����������� ���������� � ����������.")
						.setPositiveButton("��", null).show();

			}
			modules_names = null;
		}

		@SuppressLint("NewApi")
		@Override
		protected Boolean doInBackground(Boolean... params) {
			isUpdating = params[0];
			DBHelper dbHelper = new DBHelper(AfishaPlacesChoose.this);
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			Cursor c = db.query("afisha_places", null, null, null, null, null,
					null);
			if (MainActivity.work_type == 0 && c.moveToFirst() && !isUpdating) {
				place_description.clear();
				place_address.clear();
				place_tel.clear();
				place_site.clear();
				places_names.clear();
				places_id.clear();
				int nameIndex = c.getColumnIndex("place_name");
				int idIndex = c.getColumnIndex("place_id");
				int descrIndex = c.getColumnIndex("place_description");
				int addrIndex = c.getColumnIndex("place_address");
				int telIndex = c.getColumnIndex("place_tel");
				int siteIndex = c.getColumnIndex("place_site");
				do {
					places_names.add(c.getString(nameIndex));
					places_id.add(c.getInt(idIndex));
					place_description.add(c.getString(descrIndex));
					place_address.add(c.getString(addrIndex));
					place_tel.add(c.getString(telIndex));
					place_site.add(c.getString(siteIndex));
				} while (c.moveToNext());
				c.close();
			} else {
				c.close();
				db.close();
				Updating u = new Updating(AfishaPlacesChoose.this);
				int upd = u.updatePlaces();
				if (MainActivity.work_type == 0) {
					u.updateEvents();
					publishProgress(1);
					u.updateTaxi();
					publishProgress(2);
					u.updateDelivery();
					publishProgress(3);
					u.updateServicesGroups();
					u.updateServices();
					publishProgress(4);
					u.updateRestoraunts();
					publishProgress(5);
					//u.updateSights();
					try {
						GetRestorauntsList get = new GetRestorauntsList(
								AfishaPlacesChoose.this, 2);
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
							get.executeOnExecutor(
									AsyncTask.THREAD_POOL_EXECUTOR, false)
									.get();
						else
							get.execute(false).get();
						get = null;
					} catch (InterruptedException | ExecutionException e) {
					}
				}
				if (upd == 0) {
					places_id = u.places_id;
					places_names = u.places_names;
					place_description = u.place_description;
					place_tel = u.place_tel;
					place_site = u.place_site;
					place_address = u.place_address;
				} else if (upd == 2) {
					return false;
				}
				u = null;
			}
			if (dbHelper != null) {
				dbHelper.close();
			}
			if (db != null) {
				db.close();
			}
			return true;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

		Intent intent = new Intent(this, AfishaPlacesInfo.class);
		intent.putExtra("ID", arg2);
		startActivity(intent);
	}

	@Override
	public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

		year = arg1;
		month = arg2 + 1;
		day = arg3;
		date = day + "." + month + "." + year;
		Intent intent = new Intent(this, AfishaPlaces.class);
		intent.putExtra("isAll", false);
		intent.putExtra("Date", date);
		startActivity(intent);
	}

	@Override
	public void onClick(View arg0) {

		new PlacesParsing().execute(true);
	}
}
