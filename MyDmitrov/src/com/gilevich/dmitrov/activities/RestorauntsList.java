package com.gilevich.dmitrov.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.Cart;
import com.gilevich.dmitrov.background.GetRestorauntsList;
import com.gilevich.dmitrov.utils.DateUtils;

public class RestorauntsList extends Activity implements OnItemClickListener,
		OnClickListener, android.content.DialogInterface.OnClickListener {
	public static ArrayList<String> names, phones, updated, user_update, logo,
			delivery_time;
	public static ArrayList<Integer> id, minimal_amount;
	public static TextView t2;
	final String doenstLoaded = "������ �����������. ��� �������� ������ ������� ������ � ������� ������ ����";
	public static boolean isRestorauntUpdating = false;
	public static Button cartButton;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.restoraunts_list);
		((TextView) findViewById(R.id.textView1)).setVisibility(View.VISIBLE);
		((TextView) findViewById(R.id.textView1)).setText("������ ����������");
		((ListView) findViewById(R.id.listView1)).setOnItemClickListener(this);
		initializeArrayLists();
		cartButton = (Button) findViewById(R.id.button1);
		cartButton.setOnClickListener(this);
		((ImageButton) findViewById(R.id.button_refresh))
				.setOnClickListener(this);
		((TextView) findViewById(R.id.textView3)).setOnClickListener(this);
		t2 = (TextView) findViewById(R.id.textView2);
		if (MainActivity.work_type == -1)
			MainActivity.work_type = PreferenceManager
					.getDefaultSharedPreferences(this).getInt("work_type", 0);
		new GetRestorauntsList(RestorauntsList.this, 1).execute(false);
		setSettingButtonClickListener();
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							RestorauntsList.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(RestorauntsList.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												RestorauntsList.this,
												MainActivity.work_type == 0 ? 1
														: 0),
										RestorauntsList.this).show();
					}
				});
	}

	public static void initializeArrayLists() {
		RestorauntsList.id = new ArrayList<Integer>();
		RestorauntsList.names = new ArrayList<String>();
		RestorauntsList.phones = new ArrayList<String>();
		RestorauntsList.updated = new ArrayList<String>();
		RestorauntsList.logo = new ArrayList<String>();
		RestorauntsList.minimal_amount = new ArrayList<Integer>();
		RestorauntsList.delivery_time = new ArrayList<String>();
		RestorauntsList.user_update = new ArrayList<String>();
	}

	@Override
	protected void onStart() {

		super.onStart();
		if (Cart.cartCreated)
			setCartButtonText();
		t2.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		String lu = PreferenceManager.getDefaultSharedPreferences(this)
				.getString("LastUpdate", MainActivity.doenstLoaded);
		if (MainActivity.work_type == 0 && lu.equals(MainActivity.doenstLoaded)) {
			t2.setText(lu);
			((ListView) findViewById(R.id.listView1)).setAdapter(null);
		} else
			t2.setText("������ ��������� " + DateUtils.getDateText(lu));
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE
						: View.INVISIBLE);
		setSettingButtonClickListener();
	}

	public static void setCartButtonText() {
		int cartAmount = Cart.getCartAmount();
		if (cartAmount > 0) {
			cartButton.setText("������� " + cartAmount + " ���.");
		} else {
			cartButton.setText("�������");
		}
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {

		switch (arg1) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			if (MainActivity.work_type == 1)
				startActivity(new Intent(this, Settings.class));
			else {
				new GetRestorauntsList(RestorauntsList.this, 1).execute(true);
			}
			break;
		case 2:
			startActivity(new Intent(this, Settings.class));
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.button1:
			startActivity(new Intent(this, CartActivity.class));
			break;
		case R.id.button_refresh:
			new GetRestorauntsList(RestorauntsList.this, 1).execute(true);
			break;
		case R.id.textView3:
			finish();
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if ((int) arg1.getTag() == 0) {
			startActivity(new Intent(this, RestorauntSectionsList.class)
					.putExtra("RestorauntID", id.get(arg2)));
		}
	}
}
