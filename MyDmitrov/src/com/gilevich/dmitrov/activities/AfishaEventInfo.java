package com.gilevich.dmitrov.activities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.Updating;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class AfishaEventInfo extends Activity implements
		android.content.DialogInterface.OnClickListener {
	int id;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.afisha_event_info);
		id = getIntent().getIntExtra("ID", 0);
		TextView name = (TextView) findViewById(R.id.textView1);
		TextView where = (TextView) findViewById(R.id.textView2);
		TextView when = (TextView) findViewById(R.id.textView3);
		TextView time = (TextView) findViewById(R.id.textView4);
		TextView descr = (TextView) findViewById(R.id.textView5);
		TextView type = (TextView) findViewById(R.id.textView6);
		TextView price = (TextView) findViewById(R.id.textView7);
		ImageView logo = (ImageView) findViewById(R.id.imageView1);
		int size = 0;
		switch (getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			size = 35;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			size = 50;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			size = 70;
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			size = 100;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			size = 140;
		}
		LayoutParams lp = new LayoutParams(size, LayoutParams.WRAP_CONTENT);
		lp.setMargins(5, 5, 5, 5);
		if (getIntent().getIntExtra("isPlace", 0) == 0) {
			name.setText(AfishaPlaces.event_names.get(id));
			where.setText(AfishaPlaces.places_names.get(AfishaPlaces.places_id
					.indexOf(AfishaPlaces.event_places_id.get(id))));
			if (!AfishaPlaces.event_dates_to.get(id).contains(
					AfishaPlaces.event_dates_from.get(id))) {
				when.setText("c " + AfishaPlaces.event_dates_from.get(id)
						+ " �� " + AfishaPlaces.event_dates_to.get(id));
			} else {
				when.setText(AfishaPlaces.event_dates_from.get(id));
			}
			if (AfishaPlaces.event_times.get(id).length() > 0) {
				time.setText(AfishaPlaces.event_times.get(id));
			} else {
				time.setVisibility(View.GONE);
			}
			if (MainActivity.work_type == 0)
				if (AfishaPlaces.event_info.get(id).length() > 0) {
					descr.setText(AfishaPlaces.event_info.get(id));
				} else {
					descr.setVisibility(View.GONE);
				}
			else
				new GetEventInfo().execute(AfishaPlaces.event_ids.get(id));
			String pic = AfishaPlaces.event_pic.get(id);
			if (pic != null && pic.length() > 0) {
				if (pic.lastIndexOf("g") == pic.length() - 1) {
					logo.setLayoutParams(lp);
				} else {
					pic = pic.substring(0, pic.lastIndexOf("g") + 1);
				}
				logo.setImageDrawable(Drawable.createFromPath(pic));
				pic = null;
			} else {
				logo.setVisibility(View.GONE);
			}
			if (AfishaPlaces.event_type.get(id).length() > 0) {
				type.setText(AfishaPlaces.event_type.get(id));
			} else {
				type.setVisibility(View.GONE);
			}
			if (AfishaPlaces.event_price.get(id).length() > 0) {
				price.setText(AfishaPlaces.event_price.get(id));
			} else {
				price.setVisibility(View.GONE);
			}
		} else {
			name.setText(AfishaPlacesInfo.event_names.get(id));
			where.setText(getIntent().getStringExtra("Name"));
			if (!AfishaPlacesInfo.event_dates_to.get(id).contains(
					AfishaPlacesInfo.event_dates_from.get(id))) {
				when.setText("c " + AfishaPlacesInfo.event_dates_from.get(id)
						+ " �� " + AfishaPlacesInfo.event_dates_to.get(id));
			} else {
				when.setText(AfishaPlacesInfo.event_dates_from.get(id));
			}
			if (AfishaPlacesInfo.event_times.get(id).length() > 0) {
				time.setText(AfishaPlacesInfo.event_times.get(id));
			} else {
				time.setVisibility(View.GONE);
			}
			if (MainActivity.work_type == 0)
				if (AfishaPlacesInfo.event_info.get(id).length() > 0) {
					descr.setText(AfishaPlacesInfo.event_info.get(id));
				} else {
					descr.setVisibility(View.GONE);
				}
			else
				new GetEventInfo().execute(AfishaPlacesInfo.event_ids.get(id));
			if (AfishaPlacesInfo.event_pic.get(id) != null) {
				String t = AfishaPlacesInfo.event_pic.get(id);
				if (t.lastIndexOf("g") == t.length() - 1) {
					logo.setLayoutParams(lp);
				} else {
					t = t.substring(0, t.lastIndexOf("g") + 1);
				}
				logo.setImageDrawable(Drawable.createFromPath(t));
			}
			if (AfishaPlacesInfo.event_type.get(id).length() > 0) {
				type.setText(AfishaPlacesInfo.event_type.get(id));
			} else {
				type.setVisibility(View.GONE);
			}
			if (AfishaPlacesInfo.event_price.get(id).length() > 0) {
				price.setText(AfishaPlacesInfo.event_price.get(id));
			} else {
				price.setVisibility(View.GONE);
			}
		}
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						new AlertDialog.Builder(AfishaEventInfo.this)
								.setTitle(R.string.main_setting_header)//���������
								.setAdapter(
										new MainSettingsAdapter(
												AfishaEventInfo.this, 0),
										AfishaEventInfo.this).show();
					}
				});
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						finish();
					}
				});
		registerReciever();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							AfishaEventInfo.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle(R.string.main_setting_header)
					.setAdapter(new MainSettingsAdapter(this, 0), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		
		switch (which) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			startActivity(new Intent(this, Settings.class));
		}
	}

	class GetEventInfo extends AsyncTask<Integer, Void, JSONObject> {

		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
		}

		@Override
		protected JSONObject doInBackground(Integer... params) {
			
			JSONArray service_array = Updating
					.getJSONFromURL("http://mydmitrov.com/json/android/afisha_event.php?event_id="
							+ params[0]);
			if (service_array == null)
				return null;
			try {
				return service_array.getJSONObject(0);
			} catch (JSONException e) {
				service_array = null;
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			
			super.onPostExecute(result);
			if (result == null)
				new AlertDialog.Builder(AfishaEventInfo.this)
						.setMessage("���������� ��������� ������.")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										
										finish();
									}
								}).setCancelable(false).show();
			else {
				try {
					String info = result.getString("a_text");
					if (info.length() > 0) {
						((TextView) findViewById(R.id.textView5)).setText(info);
					} else {
						((TextView) findViewById(R.id.textView5))
								.setVisibility(View.GONE);
					}
					info = null;
				} catch (JSONException e) {
					((TextView) findViewById(R.id.textView5))
							.setVisibility(View.GONE);
				}
			}
		}
	}
}
