package com.gilevich.dmitrov.activities;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class About extends Activity implements OnClickListener {
	BroadcastReceiver br;
	public static int work_type = -1;
	SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.about);
		((ImageButton) findViewById(R.id.button_logo)).setOnClickListener(this);
		((TextView) findViewById(R.id.textView9)).setOnClickListener(this);
		registerReciever();
		
		// настройка ActionBar ==================
		sp = PreferenceManager.getDefaultSharedPreferences(this);
		work_type = sp.getInt("work_type",-1);
		((ImageButton) findViewById(R.id.button_refresh)).setVisibility(About.work_type == 0 ? View.VISIBLE : View.INVISIBLE);
		((ImageButton) findViewById(R.id.button_setting)).setOnClickListener(this);
		
		//========================================
	}
	
	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(About.this)
								.setTitle("Настройки")
								//.setAdapter(
									//	new MainSettingsAdapter(
										//		About.this,
											//	About.work_type == 0 ? 1
												//		: 0), About.this)
								.show();
					}
				});
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							About.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
	}

	@Override
	public void onClick(View arg0) {		
		switch (arg0.getId()) {
		case R.id.button_logo:
			finish();
			break;
		case R.id.textView9:
			Intent callIntent = new Intent(Intent.ACTION_DIAL);
			callIntent.setData(Uri.parse("tel:89255816898"));
			startActivity(callIntent);
			break;
		}
	}
}
