package com.gilevich.dmitrov.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.adapters.DeliveryAdapter;
import com.gilevich.dmitrov.adapters.MainSettingsAdapter;
import com.gilevich.dmitrov.background.Advertisment;
import com.gilevich.dmitrov.background.AdvertismentClickListener;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.background.GetRestorauntsList;
import com.gilevich.dmitrov.background.Updating;
import com.gilevich.dmitrov.utils.DateUtils;

public class DeliveryMain extends Activity implements OnClickListener,
		android.content.DialogInterface.OnClickListener {
	DBHelper dbHelper;
	SQLiteDatabase db;
	public static ArrayList<String> names, short_descriptions,
			long_descriptions, addresses, phones, logos, sites,
			restoraunts_names;
	public static ArrayList<Integer> restoraunt_ids, delivery_ids;
	public static ArrayList<Boolean> restoraunt_updated;
	ProgressDialog mProgressDialog;
	TextView t2;
	int isUpd;
	public static DeliveryAdapter adapter = null;
	ImageButton b1;
	BroadcastReceiver br;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.delivery_main);
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(this);
		t2 = (TextView) findViewById(R.id.textView2);
		String lu = sp.getString("LastUpdate", MainActivity.doenstLoaded);
		if (lu.equals(MainActivity.doenstLoaded))
			t2.setText(lu);
		else
			t2.setText("������ ��������� " + DateUtils.getDateText(lu));
		if (MainActivity.work_type == -1) {
			MainActivity.work_type = PreferenceManager
					.getDefaultSharedPreferences(this).getInt("work_type", 0);
			getRestorauntsList(true);
		}
		initializeLists();
		new DeliveryParsing().execute(false);
		b1 = (ImageButton) findViewById(R.id.button_refresh);
		b1.setOnClickListener(this);
		((ImageButton) findViewById(R.id.button_logo))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						finish();
					}
				});
		setSettingButtonClickListener();
		registerReciever();
	}

	private void initializeLists() {
		delivery_ids = new ArrayList<>();
		names = new ArrayList<String>();
		short_descriptions = new ArrayList<String>();
		long_descriptions = new ArrayList<String>();
		addresses = new ArrayList<String>();
		phones = new ArrayList<String>();
		sites = new ArrayList<>();
		logos = new ArrayList<String>();
		restoraunt_ids = new ArrayList<>();
	}

	private void clearLists() {
		delivery_ids.clear();
		names.clear();
		short_descriptions.clear();
		long_descriptions.clear();
		addresses.clear();
		phones.clear();
		sites.clear();
		logos.clear();
		restoraunt_ids.clear();
	}

	void registerReciever() {
		br = new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				int id = arg1.getIntExtra("AdvertismentItemNumber", -1);
				if (id >= 0 && Advertisment.images.size() > 0) {
					ImageView ads = (ImageView) findViewById(R.id.ads_image);
					ads.setVisibility(View.VISIBLE);
					ads.setImageBitmap(Advertisment.images.get(id));
					ads.setTag(id);
					ads.setOnClickListener(new AdvertismentClickListener(
							DeliveryMain.this));
				}
			}

		};
		registerReceiver(br, new IntentFilter("NewAdvertismentImage"));
		Advertisment.sendBroadcast(this);
	}

	void setSettingButtonClickListener() {
		((ImageButton) findViewById(R.id.button_setting))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						new AlertDialog.Builder(DeliveryMain.this)
								.setTitle("���������")
								.setAdapter(
										new MainSettingsAdapter(
												DeliveryMain.this,
												MainActivity.work_type == 0 ? 1
														: 0), DeliveryMain.this)
								.show();
					}
				});
	}

	@Override
	protected void onStart() {
		super.onStart();
		t2.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE : View.GONE);
		if (MainActivity.work_type == 0) {
			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy",
					Locale.ENGLISH);
			Date date = new Date();
			t2.setText("������ ��������� "
					+ DateUtils.getDateText(dateFormat.format(date)));
		}
		((ImageButton) findViewById(R.id.button_refresh))
				.setVisibility(MainActivity.work_type == 0 ? View.VISIBLE
						: View.INVISIBLE);
		setSettingButtonClickListener();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {
			new AlertDialog.Builder(this).setTitle("���������")
					.setAdapter(new MainSettingsAdapter(this, 1), this).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1) {

		switch (arg1) {
		case 0:
			startActivity(new Intent(this, About.class));
			break;
		case 1:
			if (MainActivity.work_type == 1)
				startActivity(new Intent(this, Settings.class));
			else {
				new DeliveryParsing().execute(true);
			}
			break;
		case 2:
			startActivity(new Intent(this, Settings.class));
		}
	}

	public static void updateRestorauntsLists(boolean fromMainClass) {
		restoraunts_names = new ArrayList<>();
		restoraunt_updated = new ArrayList<>();
		while (RestorauntsList.id == null) {
		}
		for (int i = 0; i < restoraunt_ids.size(); i++) {
			int id = restoraunt_ids.get(i);
			int position = RestorauntsList.id.indexOf(id);
			if (position >= 0) {
				restoraunts_names.add(RestorauntsList.names.get(position));
				restoraunt_updated.add(RestorauntsList.updated.get(position)
						.equals(RestorauntsList.user_update.get(position)));
			} else
				restoraunt_updated.add(null);
		}
		if (!fromMainClass) {
			adapter.notifyDataSetChanged();
		}
	}

	class DeliveryParsing extends AsyncTask<Boolean, Integer, Boolean> {
		private String[] modules_names;

		public DeliveryParsing() {
			modules_names = getResources().getStringArray(R.array.modules);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			mProgressDialog
					.setMessage("�������� ������ ������ \""
							+ modules_names[values[0]]
							+ "\". ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(DeliveryMain.this);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			publishProgress(0);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			if (result) {
				if (adapter == null) {
					adapter = new DeliveryAdapter(DeliveryMain.this);
					((ListView) findViewById(R.id.listView1))
							.setAdapter(adapter);
				} else
					adapter.notifyDataSetChanged();
				SharedPreferences sp = PreferenceManager
						.getDefaultSharedPreferences(DeliveryMain.this);
				if (isUpd == 1) {
					if (MainActivity.work_type == 0) {
						Editor ed = sp.edit();
						DateFormat dateFormat = new SimpleDateFormat(
								"dd.MM.yyyy", Locale.ENGLISH);
						Date date = new Date();
						ed.putString("LastUpdate", dateFormat.format(date));
						ed.commit();
						t2.setText("������ ��������� "
								+ DateUtils.getDateText(dateFormat.format(date)));
					}
				} else {
					t2.setText("������ ��������� "
							+ DateUtils.getDateText(sp.getString("LastUpdate",
									"")));
				}
				isUpd = 0;
			} else {
				new AlertDialog.Builder(DeliveryMain.this)
						.setTitle("���������� ��������� ������.")
						.setMessage("����������� ���������� � ����������.")
						.setPositiveButton("��", null).show();

			}
			modules_names = null;
		}

		@Override
		protected Boolean doInBackground(Boolean... params) {
			dbHelper = new DBHelper(DeliveryMain.this);
			db = dbHelper.getWritableDatabase();
			Cursor c = db.query("delivery_firms", null, null, null, null, null,
					null);
			if (MainActivity.work_type == 0 && c.moveToFirst() && !params[0]) {
				clearLists();
				publishProgress(2);
				int idIndex = c.getColumnIndex("id"), nameIndex = c
						.getColumnIndex("delivery_name"), shortDescriptionIndex = c
						.getColumnIndex("delivery_short_description"), longDescriptionIndex = c
						.getColumnIndex("delivery_long_description"), addressIndex = c
						.getColumnIndex("delivery_address"), phoneIndex = c
						.getColumnIndex("delivery_phone"), siteIndex = c
						.getColumnIndex("delivery_site"), logoIndex = c
						.getColumnIndex("delivery_logo"), restorauntIdIndex = c
						.getColumnIndex("restoraunt_id");
				do {
					delivery_ids.add(c.getInt(idIndex));
					names.add(c.getString(nameIndex));
					short_descriptions.add(c.getString(shortDescriptionIndex));
					long_descriptions.add(c.getString(longDescriptionIndex));
					addresses.add(c.getString(addressIndex));
					phones.add(c.getString(phoneIndex));
					sites.add(c.getString(siteIndex));
					logos.add(c.getString(logoIndex));
					restoraunt_ids.add(c.getInt(restorauntIdIndex));
				} while (c.moveToNext());
				c.close();
			} else {
				isUpd = 1;
				c.close();
				db.close();
				Updating u = new Updating(DeliveryMain.this);
				int upd;
				if (MainActivity.work_type == 1) {
					getRestorauntsList(false);
					publishProgress(2);
					upd = u.updateDelivery();
				} else {
					publishProgress(0);
					u.updatePlaces();
					u.updateEvents();
					publishProgress(1);
					u.updateTaxi();
					publishProgress(2);
					upd = u.updateDelivery();
					publishProgress(3);
					u.updateServicesGroups();
					u.updateServices();
					publishProgress(4);
					u.updateRestoraunts();
					getRestorauntsList(false);
					publishProgress(5);
					// u.updateSights();
				}
				if (upd == 0) {
					clearLists();
					delivery_ids.addAll(u.delivery_ids);
					names.addAll(u.delivery_names);
					short_descriptions.addAll(u.delivery_short_descriptions);
					long_descriptions.addAll(u.delivery_long_descriptions);
					addresses.addAll(u.delivery_addresses);
					phones.addAll(u.delivery_phones);
					sites.addAll(u.delivery_sites);
					logos.addAll(u.delivery_logos);
					restoraunt_ids.addAll(u.delivery_restoraunt_ids);
				} else if (upd == 2) {
					return false;
				}
				u = null;
			}
			updateRestorauntsLists(true);
			return true;
		}
	}

	@SuppressLint("NewApi")
	void getRestorauntsList(boolean fromOnCreate) {
		try {
			GetRestorauntsList get = new GetRestorauntsList(DeliveryMain.this,
					2);
			if (!fromOnCreate
					&& Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
				get.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, false)
						.get();
			else
				get.execute(false).get();
			get = null;
		} catch (InterruptedException | ExecutionException e) {
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(br);
		if (dbHelper != null) {
			dbHelper.close();
		}
		if (db != null) {
			db.close();
		}
		if (adapter != null)
			adapter = null;
	}

	@Override
	public void onClick(View arg0) {

		new DeliveryParsing().execute(true);
	}
}
