package com.gilevich.dmitrov.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.NewsActivity;
import com.gilevich.dmitrov.beans.NewsBean;

public class NewsAdapter extends BaseAdapter {
	LayoutInflater layoutInflater;

	static class ViewHolder {
		ImageView newsImage;
		TextView newsDate;
		TextView newsTitle;
		TextView newsText;
	}

	public NewsAdapter(Context context) {
		this.layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return NewsActivity.list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		NewsBean newsBean = NewsActivity.list.get(position);
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.news_item, parent,
					false);
			holder = new ViewHolder();
			holder.newsImage = (ImageView) convertView
					.findViewById(R.id.news_image);
			holder.newsDate = (TextView) convertView
					.findViewById(R.id.news_date);
			holder.newsTitle = (TextView) convertView
					.findViewById(R.id.news_title);
			holder.newsText = (TextView) convertView
					.findViewById(R.id.news_text);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		String image = newsBean.getImage();
		if (image != null) {
			holder.newsImage.setVisibility(View.VISIBLE);
			holder.newsImage.setImageDrawable(Drawable.createFromPath(image));
			image = null;
		}
		holder.newsTitle.setText(newsBean.getTitle());
		holder.newsDate.setText(newsBean.getDate());
		holder.newsText.setText(newsBean.getText());
		return convertView;
	}
}