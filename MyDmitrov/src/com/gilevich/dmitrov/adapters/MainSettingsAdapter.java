package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MainSettingsAdapter extends BaseAdapter {
	LayoutInflater lInflater;
	Context context;
	String[] items = { "� ���������", "��������� ������", "���������" };
	int fromMainActivity;

	@Override
	public int getCount() {
		
		return 2 + fromMainActivity;
	}

	public MainSettingsAdapter(Context context, int isFromMainActivity) {
		this.context = context;
		fromMainActivity = isFromMainActivity;
	}

	@Override
	public Object getItem(int arg0) {
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		
		lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arg1 = lInflater.inflate(R.layout.main_settings_item, arg2, false);
		switch (arg0) {
		case 0:
			((TextView) arg1.findViewById(R.id.textView1)).setText(items[arg0]);
			break;
		case 1:
			if (fromMainActivity == 1) {
				((TextView) arg1.findViewById(R.id.textView1))
						.setText(items[arg0]);
			} else {
				((TextView) arg1.findViewById(R.id.textView1))
						.setText(items[2]);
			}
			break;
		case 2:
			((TextView) arg1.findViewById(R.id.textView1)).setText(items[arg0]);
		}
		return arg1;
	}

}
