package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.RestorauntSectionsList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RestorauntSectionsAdapter extends BaseAdapter {
	Context context;

	public RestorauntSectionsAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		
		return RestorauntSectionsList.ids.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arg1 = lInflater.inflate(R.layout.restoraunt_item, arg2, false);
		((TextView) arg1.findViewById(R.id.textView1))
				.setText(RestorauntSectionsList.names.get(arg0));
		((TextView) arg1.findViewById(R.id.textView2)).setVisibility(View.GONE);
		((TextView) arg1.findViewById(R.id.textView3)).setVisibility(View.GONE);
		return arg1;
	}

}
