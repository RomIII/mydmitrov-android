package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.ServicesGroups;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ServicesGroupsAdapter extends BaseAdapter {
	Context context;
	String[] names = { "���������� ������", "���", "������", "��������",
			"����", "������������", "������", "�����������",
			"������� � ��������", "��������", "��������" };
	int[] icons = { R.drawable.sp_01, R.drawable.sp_02, R.drawable.sp_03,
			R.drawable.sp_04, R.drawable.sp_05, R.drawable.sp_06,
			R.drawable.sp_07, R.drawable.sp_08, R.drawable.sp_09,
			R.drawable.sp_10, R.drawable.sp_11 };

	public ServicesGroupsAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		
		return ServicesGroups.services_groups_id.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return ServicesGroups.services_groups_id.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		
		LayoutInflater lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arg1 = lInflater.inflate(R.layout.services_groups_item, arg2, false);
		int cur_img = 0;
		String name = ServicesGroups.services_groups_names.get(arg0);
		for (int i = 0; i < names.length; i++) {
			if (names[i].equals(name)) {
				cur_img = icons[i];
				break;
			}
		}
		if (cur_img > 0)
			((ImageView) arg1.findViewById(R.id.imageView11))
					.setImageResource(cur_img);
		((TextView) arg1.findViewById(R.id.textView1)).setText(name);
		name = null;
		((TextView) arg1.findViewById(R.id.textView2))
				.setText(ServicesGroups.services_groups_text.get(arg0));
		return arg1;
	}

}
