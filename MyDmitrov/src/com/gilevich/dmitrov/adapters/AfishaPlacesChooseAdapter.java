package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.AfishaPlacesChoose;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AfishaPlacesChooseAdapter extends BaseAdapter {
	Context context;

	public AfishaPlacesChooseAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		
		return AfishaPlacesChoose.places_names.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return AfishaPlacesChoose.places_names.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.afisha_group, null);
		}
		TextView name = (TextView) arg1.findViewById(R.id.textView1);
		name.setText(AfishaPlacesChoose.places_names.get(arg0));
		return arg1;
	}

}
