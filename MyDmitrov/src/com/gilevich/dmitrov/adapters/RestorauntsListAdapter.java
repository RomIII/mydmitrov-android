package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.MainActivity;
import com.gilevich.dmitrov.activities.RestorauntsList;
import com.gilevich.dmitrov.background.GetRestorauntMenu;
import com.gilevich.dmitrov.utils.ImageUtils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class RestorauntsListAdapter extends BaseAdapter implements
		OnClickListener {
	Context context;
	LayoutParams lp;

	public RestorauntsListAdapter(Context context) {
		this.context = context;
		lp = new LayoutParams(ImageUtils.getSize(context),
				LayoutParams.WRAP_CONTENT);
		lp.setMargins(0, 0, ImageUtils.dpToPx(10, context), 0);
	}

	@Override
	public int getCount() {
		
		return RestorauntsList.id.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arg1 = lInflater.inflate(R.layout.restoraunt_item, arg2, false);
		String logo_path = RestorauntsList.logo.get(arg0);
		if (logo_path != null) {
			Drawable logo = Drawable.createFromPath(logo_path);
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setImageDrawable(logo);
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setLayoutParams(lp);
			logo = null;
			logo_path = null;
		} else {
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setVisibility(View.GONE);
		}
		((TextView) arg1.findViewById(R.id.textView1))
				.setText(RestorauntsList.names.get(arg0));
		((TextView) arg1.findViewById(R.id.textView2))
				.setText("����� ������ �� "
						+ RestorauntsList.minimal_amount.get(arg0) + " �.");
		((TextView) arg1.findViewById(R.id.textView3))
				.setText("����� �������� "
						+ RestorauntsList.delivery_time.get(arg0));
		if (RestorauntsList.user_update.get(arg0) != null
				&& !RestorauntsList.user_update.get(arg0).equals(
						RestorauntsList.updated.get(arg0))
				&& MainActivity.work_type == 0) {
			((Button) arg1.findViewById(R.id.button1))
					.setText("��������\n����");
			((Button) arg1.findViewById(R.id.button1))
					.setTag(RestorauntsList.id.get(arg0));
			((Button) arg1.findViewById(R.id.button1)).setOnClickListener(this);
			((Button) arg1.findViewById(R.id.button1))
					.setVisibility(View.VISIBLE);
			arg1.setTag(1);
		} else {
			arg1.setTag(0);
		}
		return arg1;
	}

	@Override
	public void onClick(View arg0) {
		new GetRestorauntMenu(context).execute((int) arg0.getTag());
	}

}
