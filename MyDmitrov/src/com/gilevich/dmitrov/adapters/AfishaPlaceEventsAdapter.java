package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.AfishaPlacesInfo;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class AfishaPlaceEventsAdapter extends BaseAdapter {
	Context context;
	LayoutParams lp;
	int size = 0;

	public AfishaPlaceEventsAdapter(Context context) {
		this.context = context;
		switch (context.getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			size = 35;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			size = 50;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			size = 70;
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			size = 100;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			size = 140;
		}
		lp = new LayoutParams(size, LayoutParams.WRAP_CONTENT);
		lp.setMargins(5, 5, 5, 5);
	}

	@Override
	public int getCount() {
		
		return AfishaPlacesInfo.event_names.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		arg1 = inflater.inflate(R.layout.afisha_event_item, arg2, false);
		TextView name = (TextView) arg1.findViewById(R.id.textView1);
		TextView time = (TextView) arg1.findViewById(R.id.textView2);
		TextView type = (TextView) arg1.findViewById(R.id.textView3);
		TextView when = (TextView) arg1.findViewById(R.id.textView4);
		TextView price = (TextView) arg1.findViewById(R.id.textView5);
		name.setText(AfishaPlacesInfo.event_names.get(arg0));
		String t = AfishaPlacesInfo.event_times.get(arg0);
		if (t.length() > 0) {
			time.setText(t);
		} else {
			time.setHeight(0);
		}
		t = AfishaPlacesInfo.event_type.get(arg0);
		if (t.length() > 0) {
			type.setText(t);
		} else {
			type.setHeight(0);
		}
		ImageView i1 = (ImageView) arg1.findViewById(R.id.imageView1);
		t = AfishaPlacesInfo.event_pic.get(arg0);
		if (t != null && t.length() > 0) {
			if (t.lastIndexOf("g") == t.length() - 1) {
				i1.setLayoutParams(lp);
			} else {
				t = t.substring(0, t.lastIndexOf("g") + 1);
			}
			i1.setImageDrawable(Drawable.createFromPath(t));
		} else {
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) i1
					.getLayoutParams();
			params.setMargins(0, 0, 0, 0);
			i1.setLayoutParams(params);
		}
		if (AfishaPlacesInfo.event_price.get(arg0).length() > 0) {
			price.setText(AfishaPlacesInfo.event_price.get(arg0));
		} else {
			price.setHeight(0);
		}
		if (!AfishaPlacesInfo.event_dates_to.get(arg0).contains(
				AfishaPlacesInfo.event_dates_from.get(arg0))) {
			when.setText("c " + AfishaPlacesInfo.event_dates_from.get(arg0)
					+ " �� " + AfishaPlacesInfo.event_dates_to.get(arg0));
		} else {
			when.setText(AfishaPlacesInfo.event_dates_from.get(arg0));
		}
		t = null;
		return arg1;
	}

}
