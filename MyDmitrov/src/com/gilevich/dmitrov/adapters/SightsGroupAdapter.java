package com.gilevich.dmitrov.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.SightsListActivity;

public class SightsGroupAdapter extends BaseExpandableListAdapter {
	Context context;
	LayoutInflater lInflater;

	public SightsGroupAdapter(Context context) {
		this.context = context;
		lInflater = LayoutInflater.from(context);
	}

	@Override
	public int getGroupCount() {
		return SightsListActivity.sights_groups_ids.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return SightsListActivity.sights_groups_ends.get(groupPosition)
				- SightsListActivity.sights_groups_starts.get(groupPosition);
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		TextView v = (TextView) lInflater.inflate(R.layout.sight_group_item,
				parent, false);
		v.setText(SightsListActivity.sights_groups_names.get(groupPosition));
		return v;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		View v = lInflater.inflate(R.layout.sight_item, parent, false);
		int position = getChildPosition(groupPosition, childPosition);
		String image = SightsListActivity.sights_main_photos.get(position);
		if (image != null) {
			((ImageView) v.findViewById(R.id.sight_main_image))
					.setImageDrawable(Drawable.createFromPath(image));
			image = null;
		} else
			v.findViewById(R.id.sight_main_image).setVisibility(View.GONE);
		((TextView) v.findViewById(R.id.sight_item_name))
				.setText(SightsListActivity.sights_names.get(position));
		v.setTag(position);
		return v;
	}

	private int getChildPosition(int groupPosition, int childPosition) {
		return SightsListActivity.sights_groups_starts.get(groupPosition)
				+ childPosition;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
