package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class MainAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater lInflater;
	private TextView t1, t2;
	private String[] order = { "�����", "�����", "�������� �� ���",
			"����������", "����", "�������" };
	private String[] dop_info = { "���������, ����, ��������",
			"������ ����� ������", "�����, ����, ����� � ��.",
			"������ � �������� ����������� �����������",
			"�������� ������������ ��� ������", "����������� �����������" };
	private int[] images = { R.drawable.main_afisha, R.drawable.main_taxi,
			R.drawable.main_dostavka, R.drawable.main_uslygi,
			R.drawable.main_menu, R.drawable.main_tv_smt };
	private LayoutParams lp;
	private int size;
	private boolean showNews;

	public MainAdapter(Context context, boolean showNews) {
		this.context = context;
		this.showNews = showNews;
		switch (context.getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			size = 35;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			size = 50;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			size = 70;
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			size = 100;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			size = 140;
		}
		lp = new LayoutParams(size, -2);
		lp.setMargins(0, 0, 0, 0);
		lp.gravity = Gravity.CENTER_VERTICAL;
	}

	@Override
	public int getCount() {
		return order.length - (showNews ? 0 : 1);
	}

	@Override
	public String getItem(int position) {
		return order[position];
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = convertView;
		if (v == null) {
			v = lInflater.inflate(R.layout.main_items, parent, false);
		}
		t1 = (TextView) v.findViewById(R.id.textView1);
		t1.setText(order[position]);
		t2 = (TextView) v.findViewById(R.id.textView2);
		t2.setText(dop_info[position]);
		ImageView i1 = (ImageView) v.findViewById(R.id.photo);
		i1.setLayoutParams(lp);
		i1.setImageResource(images[position]);
		return v;
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

}