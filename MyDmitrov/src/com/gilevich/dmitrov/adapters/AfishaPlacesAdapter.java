package com.gilevich.dmitrov.adapters;

import java.util.List;
import java.util.Map;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.AfishaPlaces;
import com.gilevich.dmitrov.utils.ImageUtils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class AfishaPlacesAdapter extends BaseExpandableListAdapter {
	Map<String, List<Integer>> eventCollections;
	List<String> places;
	Activity context;
	int s_id, size = 0;
	LayoutParams lp;

	@Override
	public Integer getChild(int groupPosition, int childPosition) {
		
		return eventCollections.get(places.get(groupPosition)).get(
				childPosition);
	}

	public AfishaPlacesAdapter(Activity context, List<String> places,
			Map<String, List<Integer>> collection) {
		this.context = context;
		this.eventCollections = collection;
		this.places = places;
		lp = new LayoutParams(ImageUtils.getSize(context),
				LayoutParams.WRAP_CONTENT);
		lp.setMargins(5, 5, 5, 5);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		convertView = inflater.inflate(R.layout.afisha_event_item, parent,
				false);
		TextView name = (TextView) convertView.findViewById(R.id.textView1);
		TextView time = (TextView) convertView.findViewById(R.id.textView2);
		TextView type = (TextView) convertView.findViewById(R.id.textView3);
		TextView when = (TextView) convertView.findViewById(R.id.textView4);
		TextView price = (TextView) convertView.findViewById(R.id.textView5);
		name.setText(AfishaPlaces.event_names.get(eventCollections.get(
				places.get(groupPosition)).get(childPosition)));
		String t = AfishaPlaces.event_times.get(eventCollections.get(
				places.get(groupPosition)).get(childPosition));
		if (t.length() > 0) {
			time.setText(t);
		} else {
			time.setHeight(0);
		}
		t = "";
		t = AfishaPlaces.event_type.get(eventCollections.get(
				places.get(groupPosition)).get(childPosition));
		if (t.length() > 0) {
			type.setText(t);
		} else {
			type.setHeight(0);
		}
		t = null;
		t = AfishaPlaces.event_pic.get(eventCollections.get(
				places.get(groupPosition)).get(childPosition));
		ImageView i1 = (ImageView) convertView.findViewById(R.id.imageView1);
		if (t != null && t.length() > 0) {
			if (t.lastIndexOf("g") == t.length() - 1) {
				i1.setLayoutParams(lp);
			} else {
				t = t.substring(0, t.lastIndexOf("g") + 1);
			}
			i1.setImageDrawable(null);
			i1.setImageDrawable(Drawable.createFromPath(t));
		} else {
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) i1
					.getLayoutParams();
			params.setMargins(0, 0, 0, 0);
			i1.setLayoutParams(params);
		}
		t = AfishaPlaces.event_price.get(eventCollections.get(
				places.get(groupPosition)).get(childPosition));
		if (t.length() > 0) {
			price.setText(t);
		} else {
			price.setHeight(0);
		}
		if (!AfishaPlaces.event_dates_to.get(
				eventCollections.get(places.get(groupPosition)).get(
						childPosition)).contains(
				AfishaPlaces.event_dates_from.get(eventCollections.get(
						places.get(groupPosition)).get(childPosition)))) {
			when.setText("c "
					+ AfishaPlaces.event_dates_from.get(eventCollections.get(
							places.get(groupPosition)).get(childPosition))
					+ " �� "
					+ AfishaPlaces.event_dates_to.get(eventCollections.get(
							places.get(groupPosition)).get(childPosition)));
		} else {
			when.setText(AfishaPlaces.event_dates_from.get(eventCollections
					.get(places.get(groupPosition)).get(childPosition)));
		}
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		
		return eventCollections.get(places.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		
		return AfishaPlaces.places_id.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		
		return eventCollections.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		
		return ((Integer) groupPosition).longValue();
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.afisha_group, parent,
					false);
		}
		TextView item = (TextView) convertView.findViewById(R.id.textView1);
		item.setText(places.get(groupPosition));
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		
		return true;
	}
}