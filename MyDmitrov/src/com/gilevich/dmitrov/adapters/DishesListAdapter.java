package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.DishesList;
import com.gilevich.dmitrov.background.Cart;
import com.gilevich.dmitrov.background.DBHelper;
import com.gilevich.dmitrov.utils.ImageUtils;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class DishesListAdapter extends BaseAdapter implements OnClickListener {
	Context context;
	LayoutParams lp;

	public DishesListAdapter(Context context) {
		this.context = context;
		lp = new LayoutParams(ImageUtils.getSize(context),
				LayoutParams.WRAP_CONTENT);
		lp.setMargins(0, 0, ImageUtils.dpToPx(10, context), 0);
	}

	@Override
	public int getCount() {
		return DishesList.ids.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		
		LayoutInflater lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arg1 = lInflater.inflate(R.layout.dish_item, arg2, false);
		String logo_path = DishesList.pictures.get(arg0);
		if (logo_path != null) {
			Drawable logo = Drawable.createFromPath(logo_path);
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setImageDrawable(logo);
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setLayoutParams(lp);
			logo = null;
		} else {
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setVisibility(View.GONE);
		}
		logo_path = null;
		((TextView) arg1.findViewById(R.id.textView1)).setText(DishesList.names
				.get(arg0));
		((TextView) arg1.findViewById(R.id.textView2))
				.setText(DishesList.descriptions.get(arg0));
		((TextView) arg1.findViewById(R.id.textView3))
				.setText(DishesList.portions.get(arg0));
		((TextView) arg1.findViewById(R.id.textView4)).setText(" "
				+ DishesList.prices.get(arg0) + " ���.");
		int dish_id = DishesList.ids.get(arg0);
		((TextView) arg1.findViewById(R.id.textView5)).setTag(R.id.DISH_ID,
				dish_id);
		((TextView) arg1.findViewById(R.id.textView7)).setTag(R.id.DISH_ID,
				dish_id);
		((TextView) arg1.findViewById(R.id.textView5)).setTag(R.id.ACTION, 1);
		((TextView) arg1.findViewById(R.id.textView7)).setTag(R.id.ACTION, -1);
		((TextView) arg1.findViewById(R.id.textView5)).setOnClickListener(this);
		((TextView) arg1.findViewById(R.id.textView7)).setOnClickListener(this);
		int count = Cart.dishes.get(dish_id, 0);
		Log.d("i1", "count = " + count);
		if (count > 0) {
			((TextView) arg1.findViewById(R.id.textView6))
					.setVisibility(View.VISIBLE);
			((TextView) arg1.findViewById(R.id.textView6)).setText(count + "");
			((TextView) arg1.findViewById(R.id.textView7))
					.setVisibility(View.VISIBLE);
		}
		arg1.setTag(1);
		return arg1;
	}

	@Override
	public void onClick(View arg0) {
		
		int dish_id = (int) arg0.getTag(R.id.DISH_ID);
		SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
		if (!db.query("dishes", new String[] { "id" }, "id=" + dish_id, null,
				null, null, null).moveToFirst()) {
			ContentValues cv = new ContentValues();
			int position = DishesList.ids.indexOf(dish_id);
			cv.put("id", dish_id);
			cv.put("name", DishesList.names.get(position));
			cv.put("description", DishesList.descriptions.get(position));
			cv.put("price", DishesList.prices.get(position));
			cv.put("picture	", DishesList.pictures.get(position));
			cv.put("portion", DishesList.portions.get(position));
			cv.put("catalog_id", DishesList.catalogID);
			cv.put("restoraunt_id", DishesList.restorauntID);
			db.insert("dishes", null, cv);
		}
		db.close();
		int result = Cart.modifyDish(dish_id, (int) arg0.getTag(R.id.ACTION),
				DishesList.restorauntID);
		if (result >= 0) {
			LinearLayout ll = (LinearLayout) arg0.getParent();
			int visibility_code = View.VISIBLE;
			if ((int) arg0.getTag(R.id.ACTION) == -1 && result == 0) {
				visibility_code = View.GONE;
			}
			((TextView) ll.findViewById(R.id.textView6))
					.setVisibility(visibility_code);
			((TextView) ll.findViewById(R.id.textView7))
					.setVisibility(visibility_code);
			((TextView) ll.findViewById(R.id.textView6)).setText(result + "");
			ll = null;
		} else if (result == -1) {
			new AlertDialog.Builder(context)
					.setMessage(
							"���������� ������� ����� �� ������ ���������� ������������. �������� �������?")
					.setPositiveButton("��",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									
									Cart.dishes.clear();
									Cart.currentRestoraunt = -1;
									DishesList.setCartButtonText();
								}
							}).setNegativeButton("���", null)
					.setCancelable(false).show();
		}
		DishesList.setCartButtonText();
	}
}
