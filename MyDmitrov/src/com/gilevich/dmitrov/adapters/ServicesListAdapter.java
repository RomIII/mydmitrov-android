package com.gilevich.dmitrov.adapters;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.ServiceInfo;
import com.gilevich.dmitrov.activities.ServicesList;
import com.gilevich.dmitrov.background.Analytics;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class ServicesListAdapter extends BaseAdapter implements OnClickListener {
	Context context;
	int s_id;
	LayoutParams lp;
	int size = 0;

	public ServicesListAdapter(Context context) {
		this.context = context;
		switch (context.getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			size = 35;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			size = 50;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			size = 70;
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			size = 100;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			size = 140;
		}
		lp = new LayoutParams(size, LayoutParams.WRAP_CONTENT);

	}

	@Override
	public int getCount() {
		
		return ServicesList.services_group_ids.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return ServicesList.services_group_ids.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		
		LayoutInflater lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arg1 = lInflater.inflate(R.layout.services_list_item, arg2, false);
		String logo = ServicesList.services_logos.get(arg0);
		if (logo != null && logo.length() > 0) {
			String t = ServicesList.services_logos.get(arg0);
			if (t.lastIndexOf("g") == t.length() - 1) {
				((ImageView) arg1.findViewById(R.id.imageView1))
						.setLayoutParams(lp);
			} else {
				t = t.substring(0, t.lastIndexOf("g") + 1);
			}
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setImageDrawable(Drawable.createFromPath(t));
			logo = null;
		} else {
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setVisibility(View.GONE);
		}
		((TextView) arg1.findViewById(R.id.textView1))
				.setText(ServicesList.services_names.get(arg0));
		((TextView) arg1.findViewById(R.id.textView2))
				.setText(ServicesList.services_info.get(arg0));
		((ImageView) arg1.findViewById(R.id.imageView2))
				.setOnClickListener(this);
		if (ServicesList.services_phones.get(arg0).length() > 0) {
			((TextView) arg1.findViewById(R.id.textView1))
					.setOnClickListener(this);
			((TextView) arg1.findViewById(R.id.textView2))
					.setOnClickListener(this);
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setOnClickListener(this);
		}
		return arg1;
	}

	@Override
	public void onClick(View arg0) {
		
		switch (arg0.getId()) {
		case R.id.textView1:
			s_id = ServicesList.services_names.indexOf(((TextView) arg0)
					.getText());
			break;
		case R.id.textView2:
			s_id = ServicesList.services_info.indexOf(((TextView) arg0)
					.getText());
			break;
		case R.id.imageView1:
			RelativeLayout rl = (RelativeLayout) arg0.getParent();
			s_id = ServicesList.services_names.indexOf(((TextView) rl
					.findViewById(R.id.textView1)).getText());
			break;
		case R.id.imageView2:
			RelativeLayout rl1 = (RelativeLayout) arg0.getParent();
			context.startActivity(new Intent(context, ServiceInfo.class)
					.putExtra("ID", ServicesList.services_names
							.indexOf(((TextView) rl1
									.findViewById(R.id.textView1)).getText())));
		}
		if (arg0.getId() != R.id.imageView2) {
			String phone = ServicesList.services_phones.get(s_id);
			if (phone.indexOf(",") == 0) {
				Analytics.sendCall(phone, false);
				context.startActivity(new Intent(Intent.ACTION_DIAL)
						.setData(Uri.parse("tel:" + phone)));
			} else {
				final String[] phones = phone.split(",");
				new AlertDialog.Builder(context)
						.setTitle("�������� ����� ��� ������")
						.setNegativeButton("������", null)
						.setItems(phones,
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Analytics
												.sendCall(phones[which], false);
										context.startActivity(new Intent(
												Intent.ACTION_DIAL).setData(Uri
												.parse("tel:" + phones[which])));
									}
								}).show();
			}
		}
	}
}
