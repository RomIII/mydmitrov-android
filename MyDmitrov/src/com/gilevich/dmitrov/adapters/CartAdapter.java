package com.gilevich.dmitrov.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.CartActivity;
import com.gilevich.dmitrov.background.Cart;

public class CartAdapter extends BaseAdapter implements OnClickListener {
	Context context;

	public CartAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		return Cart.dishes.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arg1 = lInflater.inflate(R.layout.dish_item, arg2, false);
		String logo_path = CartActivity.pictures.get(arg0);
		if (logo_path != null) {
			Drawable logo = Drawable.createFromPath(logo_path);
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setImageDrawable(logo);
			logo = null;
		} else {
			((ImageView) arg1.findViewById(R.id.imageView1))
					.setVisibility(View.GONE);
		}
		logo_path = null;
		((TextView) arg1.findViewById(R.id.textView1))
				.setText(CartActivity.names.get(arg0));
		((TextView) arg1.findViewById(R.id.textView2))
				.setText(CartActivity.descriptions.get(arg0));
		((TextView) arg1.findViewById(R.id.textView3))
				.setText(CartActivity.portions.get(arg0));
		((TextView) arg1.findViewById(R.id.textView4)).setText(" "
				+ CartActivity.prices.get(arg0) + " ���.");
		int dish_id = CartActivity.ids.get(arg0);
		((TextView) arg1.findViewById(R.id.textView5)).setTag(R.id.DISH_ID,
				dish_id);
		((TextView) arg1.findViewById(R.id.textView7)).setTag(R.id.DISH_ID,
				dish_id);
		((TextView) arg1.findViewById(R.id.textView5)).setTag(R.id.ACTION, 1);
		((TextView) arg1.findViewById(R.id.textView7)).setTag(R.id.ACTION, -1);
		((TextView) arg1.findViewById(R.id.textView5)).setOnClickListener(this);
		((TextView) arg1.findViewById(R.id.textView7)).setOnClickListener(this);
		int count = Cart.dishes.get(dish_id, 0);
		if (count > 0) {
			((TextView) arg1.findViewById(R.id.textView6))
					.setVisibility(View.VISIBLE);
			((TextView) arg1.findViewById(R.id.textView6)).setText(count + "");
			((TextView) arg1.findViewById(R.id.textView7))
					.setVisibility(View.VISIBLE);
		}
		arg1.setTag(1);
		return arg1;
	}

	@Override
	public void onClick(View arg0) {
		
		int result = Cart.modifyDish((int) arg0.getTag(R.id.DISH_ID),
				(int) arg0.getTag(R.id.ACTION), Cart.currentRestoraunt);
		if (result == 0) {
			int position = CartActivity.ids.indexOf(arg0.getTag(R.id.DISH_ID));
			CartActivity.ids.remove(position);
			CartActivity.names.remove(position);
			CartActivity.descriptions.remove(position);
			CartActivity.prices.remove(position);
			CartActivity.portions.remove(position);
			CartActivity.pictures.remove(position);
			notifyDataSetChanged();
		} else if (result > 0) {
			LinearLayout ll = (LinearLayout) arg0.getParent();
			((TextView) ll.findViewById(R.id.textView6)).setText(result + "");
			ll = null;
		} else if (result == -1) {
			new AlertDialog.Builder(context)
					.setMessage(
							"���������� ������� ����� �� ������ ���������� ������������. �������� �������?")
					.setPositiveButton("��",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									
									Cart.dishes.clear();
									Cart.currentRestoraunt = -1;
									Cart.restorauntName = "";
								}
							}).setNegativeButton("���", null)
					.setCancelable(false).show();
		}
		CartActivity.changeCartHeader(Cart.dishes.size() == 0);
	}
}
