package com.gilevich.dmitrov.adapters;

import java.util.ArrayList;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.MainActivity;
import com.gilevich.dmitrov.activities.TaxiMain;
import com.gilevich.dmitrov.activities.TaxiSelectNumber;
import com.gilevich.dmitrov.background.Analytics;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class TaxiAdapter extends BaseAdapter implements OnClickListener {
	Context context;
	LayoutInflater lInflater;
	public static TextView t2, t1;
	LayoutParams lp;
	int size = 0;

	public TaxiAdapter(Context context) {
		this.context = context;
		switch (context.getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			size = 35;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			size = 50;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			size = 70;
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			size = 100;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			size = 140;
		}
		lp = new LayoutParams(size, LayoutParams.WRAP_CONTENT);
		lp.setMargins(0, 0, 0, 0);
	}

	@Override
	public int getCount() {
		
		return TaxiMain.name.size();
	}

	@Override
	public String getItem(int position) {
		
		return TaxiMain.phone.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		lInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = lInflater.inflate(R.layout.taxi_item, parent, false);
		TextView name = (TextView) convertView.findViewById(R.id.textView1);
		ImageView i1 = (ImageView) convertView.findViewById(R.id.imageView1);
		String logo = TaxiMain.logos.get(position);
		if (logo != null && logo.length() > 0) {
			String t = TaxiMain.logos.get(position);
			if (t.lastIndexOf("g") == t.length() - 1) {
			} else {
				t = t.substring(0, t.lastIndexOf("g") + 1);
			}
			i1.setImageDrawable(Drawable.createFromPath(t));
		}
		i1.setLayoutParams(lp);
		i1.setOnClickListener(this);
		logo = null;
		ImageView i2 = (ImageView) convertView.findViewById(R.id.imageView2);
		name.setText(TaxiMain.name.get(position));
		TextView phone = (TextView) convertView.findViewById(R.id.textView2);
		if (MainActivity.work_type == 0) {
			i2.setOnClickListener(this);
			phone.setText(TaxiMain.phone.get(position));
			phone.setOnClickListener(this);
		} else {
			phone.setVisibility(View.GONE);
			i2.setVisibility(View.INVISIBLE);
		}
		name.setOnClickListener(this);
		name.setTag(TaxiMain.id.get(TaxiMain.phones.indexOf(TaxiMain.phone
				.get(position))));
		return convertView;
	}

	@Override
	public void onClick(View arg0) {
		
		if (arg0.getId() == R.id.textView1 || arg0.getId() == R.id.textView2) {
			LinearLayout ll = (LinearLayout) arg0.getParent();
			RelativeLayout rl = (RelativeLayout) ll.getParent();
			t2 = (TextView) rl.findViewById(R.id.textView2);
			t1 = (TextView) rl.findViewById(R.id.textView1);
		} else {
			RelativeLayout ll = (RelativeLayout) arg0.getParent();
			t2 = (TextView) ll.findViewById(R.id.textView2);
			t1 = (TextView) ll.findViewById(R.id.textView1);
		}
		if (arg0.getId() != R.id.imageView2) {
			final Intent callIntent = new Intent(Intent.ACTION_DIAL);
			if (MainActivity.work_type == 0) {
				Analytics.sendCall(t2.getText().toString(), false);
				callIntent.setData(Uri.parse("tel:" + t2.getText().toString()));
				context.startActivity(callIntent);
			} else {
				final ArrayList<String> phones = new ArrayList<>();
				int start = TaxiMain.id.indexOf((int) t1.getTag()), end = TaxiMain.id
						.lastIndexOf((int) t1.getTag());
				for (int i = start; i <= end; i++) {
					phones.add(TaxiMain.phones.get(i));
				}
				new AlertDialog.Builder(context)
						.setTitle(R.string.choose_phone_number)
						.setItems(phones.toArray(new String[phones.size()]),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										
										Analytics.sendCall(phones.get(which),
												false);
										callIntent.setData(Uri.parse("tel:"
												+ phones.get(which)));
										context.startActivity(callIntent);
									}
								}).show();
			}
		} else {
			Intent intent = new Intent(context, TaxiSelectNumber.class);
			context.startActivity(intent);
		}
	}

	@Override
	public long getItemId(int arg0) {
		
		return arg0;
	}
}