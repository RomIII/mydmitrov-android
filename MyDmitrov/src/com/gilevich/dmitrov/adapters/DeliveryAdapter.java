package com.gilevich.dmitrov.adapters;

import java.util.concurrent.ExecutionException;

import com.gilevich.dmitrov.R;
import com.gilevich.dmitrov.activities.DeliveryMain;
import com.gilevich.dmitrov.activities.MainActivity;
import com.gilevich.dmitrov.activities.RestorauntSectionsList;
import com.gilevich.dmitrov.background.Analytics;
import com.gilevich.dmitrov.background.GetRestorauntsList;
import com.gilevich.dmitrov.background.Updating;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class DeliveryAdapter extends BaseAdapter implements OnClickListener {
	Context context;
	int cur = -1, size = 0;
	LayoutParams lp;

	public DeliveryAdapter(Context context) {

		this.context = context;
		switch (context.getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			size = 35;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			size = 50;
			break;
		case DisplayMetrics.DENSITY_HIGH:
			size = 70;
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			size = 100;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			size = 140;
		}
		lp = new LayoutParams(size, LayoutParams.WRAP_CONTENT);
		lp.setMargins(0, 0, 0, 0);
	}

	@Override
	public int getCount() {

		int count = DeliveryMain.names.size();
		return count;
	}

	@Override
	public String getItem(int arg0) {

		return DeliveryMain.names.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {

		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		arg1 = ((LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.delivery_item, arg2, false);
		TextView name = (TextView) arg1.findViewById(R.id.textView1);
		TextView descr = (TextView) arg1.findViewById(R.id.textView2);
		TextView phone = (TextView) arg1.findViewById(R.id.textView3);
		ImageView i1 = (ImageView) arg1.findViewById(R.id.imageView1);
		Button menu = (Button) arg1.findViewById(R.id.button1);
		arg1.setTag(arg0);
		Boolean updated = DeliveryMain.restoraunt_updated.get(arg0);
		menu.setVisibility(View.VISIBLE);
		if (updated == null) {
			menu.setVisibility(View.GONE);
		} else if (MainActivity.work_type == 1 || updated) {
			menu.setText("����");
			menu.setTag(0);
		} else {
			menu.setText("��������\n����");
			menu.setTag(1);
		}
		if (updated != null)
			menu.setOnClickListener(this);
		((ImageView) arg1.findViewById(R.id.imageView2))
				.setOnClickListener(this);
		i1.setImageDrawable(null);
		String image = DeliveryMain.logos.get(arg0);
		if (image != null && image.length() > 0) {
			String t = DeliveryMain.logos.get(arg0);
			if (t.lastIndexOf("g") == t.length() - 1) {
				i1.setLayoutParams(lp);
			} else {
				t = t.substring(0, t.lastIndexOf("g") + 1);
			}
			i1.setImageDrawable(Drawable.createFromPath(t));
			i1.setOnClickListener(this);
			image = null;
		} else {
			i1.setVisibility(View.GONE);
		}
		if (DeliveryMain.phones.get(arg0).length() > 0) {
			phone.setText(DeliveryMain.phones.get(arg0));
			phone.setOnClickListener(this);
		} else {
			phone.setHeight(0);
		}
		name.setText(DeliveryMain.names.get(arg0));
		descr.setText(DeliveryMain.short_descriptions.get(arg0));
		name.setOnClickListener(this);
		descr.setOnClickListener(this);
		return arg1;
	}

	void infoDialog() {
		String text = "";
		String descr = DeliveryMain.long_descriptions.get(cur), address = DeliveryMain.addresses
				.get(cur), phone = DeliveryMain.phones.get(cur), site = DeliveryMain.sites
				.get(cur);
		if (descr.length() > 0) {
			text += descr + "<br/><br/>";
		}
		if (site.length() > 0) {
			text += "����: <a href=\"" + site + "\">" + site + "</a><br/><br/>";
		}
		if (address.length() > 0) {
			text += "�����: " + address + "<br/><br/>";
		}
		if (phone.length() > 0) {
			text += "�������: " + phone;
		}
		((TextView) new AlertDialog.Builder(context)
				.setPositiveButton("�������", null)
				.setMessage(Html.fromHtml(text)).show()
				.findViewById(android.R.id.message))
				.setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == R.id.imageView1) {
			RelativeLayout ll = (RelativeLayout) arg0.getParent();
			cur = (int) ((View) arg0.getParent()).getTag();
			Analytics.sendCall(DeliveryMain.phones.get(cur), false);
			Intent callIntent = new Intent(Intent.ACTION_DIAL);
			callIntent
					.setData(Uri.parse("tel:" + DeliveryMain.phones.get(cur)));
			context.startActivity(callIntent);
		} else if (arg0.getId() == R.id.imageView2
				|| arg0.getId() == R.id.button1) {
			cur = (int) ((View) arg0.getParent().getParent()).getTag();
			if (arg0.getId() == R.id.imageView2) {
				infoDialog();
			} else {
				switch ((int) arg0.getTag()) {
				case 0:
					context.startActivity(new Intent(context,
							RestorauntSectionsList.class).putExtra(
							"RestorauntID",
							DeliveryMain.restoraunt_ids.get(cur)));
					break;
				case 1:
					new GetRestorauntMenu().execute(DeliveryMain.restoraunt_ids
							.get(cur));
					break;
				}
			}
		} else {
			LinearLayout ll = (LinearLayout) arg0.getParent();
			cur = (int) ((View) arg0.getParent().getParent()).getTag();
			Analytics.sendCall(DeliveryMain.phones.get(cur), false);
			Intent callIntent = new Intent(Intent.ACTION_DIAL);
			callIntent
					.setData(Uri.parse("tel:" + DeliveryMain.phones.get(cur)));
			context.startActivity(callIntent);
		}
	}

	class GetRestorauntMenu extends AsyncTask<Integer, Void, Boolean> {
		ProgressDialog mProgressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog
					.setMessage("�������� ������. ����� �������� ����� �� �������� ����������� � ��������� � ������ ����������.");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

		@Override
		protected Boolean doInBackground(Integer... params) {
			Updating u = new Updating(context);
			boolean result = u.updateRestoraunt(params[0]) == 0;
			u = null;
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			if (result) {
				new AlertDialog.Builder(context)
						.setMessage("�������� ���������!")
						.setPositiveButton("OK", null).show();
				try {
					new GetRestorauntsList(context, 2).execute(false).get();
				} catch (InterruptedException | ExecutionException e) {
				}
				DeliveryMain.updateRestorauntsLists(false);
			} else {
				new AlertDialog.Builder(context)
						.setMessage(
								"���������� ��������� ������.�������� ���������� � ����������.")
						.show();
			}
		}
	}
}
